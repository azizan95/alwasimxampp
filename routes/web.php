<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::get('/index', function () {
    return view('index');
});

Route::get('/index/logout', function () {
    return view('logout');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/userManagement', [App\Http\Controllers\UserManagementController::class, 'index'])->name('userManagement');
Route::get('/userProfile/{id}', [App\Http\Controllers\UserManagementController::class, 'userProfile'])->name('userProfile');
Route::get('/adminRegister', [App\Http\Controllers\UserManagementController::class, 'index'])->name('userManagement');
Route::post('/editUser', [App\Http\Controllers\UserManagementController::class, 'editUser'])->name('editUser');
Route::post('/deleteUser', [App\Http\Controllers\UserManagementController::class, 'deleteUser'])->name('deleteUser');
Route::post('/adminRegister', [App\Http\Controllers\UserManagementController::class, 'adminRegister'])->name('adminRegister');
Route::post('/approveUsers', [App\Http\Controllers\UserManagementController::class, 'approveUsers'])->name('approveUsers');
Route::post('/rejectUsers', [App\Http\Controllers\UserManagementController::class, 'rejectUsers'])->name('rejectUsers');
Route::post('/changePassword', [App\Http\Controllers\UserManagementController::class, 'changePassword'])->name('changePassword');

Route::post('/createClass', [App\Http\Controllers\ClassManagementController::class, 'createClass'])->name('createClass');
Route::post('/editClass', [App\Http\Controllers\ClassManagementController::class, 'editClass'])->name('editClass');
Route::post('/deleteClass', [App\Http\Controllers\ClassManagementController::class, 'deleteClass'])->name('deleteClass');
Route::post('/enrollClass', [App\Http\Controllers\ClassManagementController::class, 'enrollClass'])->name('enrollClass');
Route::post('/leaveClass', [App\Http\Controllers\ClassManagementController::class, 'leaveClass'])->name('leaveClass');
Route::get('ajax-autocomplete-search', [App\Http\Controllers\ClassManagementController::class,'selectSearch']);
Route::get('/createClass', [App\Http\Controllers\ClassManagementController::class, 'index'])->name('classManagement');
Route::any('/classManagement', [App\Http\Controllers\ClassManagementController::class, 'index'])->name('classManagement');
Route::get('/classDetails/{id}', [App\Http\Controllers\ClassManagementController::class, 'classDetails'])->name('classDetails');

Route::get('/ajaxcheckValidDatetime2', [App\Http\Controllers\SessionManagementController::class,'checkValidDatetime2'])->name('checkValidDatetime2');
Route::get('/ajaxcheckValidDatetime', [App\Http\Controllers\SessionManagementController::class,'checkValidDatetime'])->name('checkValidDatetime');
Route::get('classDetails/{classID}/sessionDetails/{sessionID}', [App\Http\Controllers\SessionManagementController::class, 'sessionDetails'])->name('sessionDetails');
Route::get('/checkValidDatetimeStudent', [App\Http\Controllers\SessionManagementController::class,'checkValidDatetimeStudent'])->name('checkValidDatetimeStudent');
Route::post('/bookSession', [App\Http\Controllers\SessionManagementController::class, 'bookSession'])->name('bookSession');
Route::post('/cancelBooking', [App\Http\Controllers\SessionManagementController::class, 'cancelBooking'])->name('cancelBooking');
Route::post('/addSessionLink', [App\Http\Controllers\SessionManagementController::class, 'addSessionLink'])->name('addSessionLink');
Route::post('/cancelSessionBooking', [App\Http\Controllers\SessionManagementController::class, 'cancelSessionBooking'])->name('cancelSessionBooking');
Route::post('/confirmSessionBooking', [App\Http\Controllers\SessionManagementController::class, 'confirmSessionBooking'])->name('confirmSessionBooking');
Route::post('/checkValidDatetimeStudent', [App\Http\Controllers\SessionManagementController::class,'checkValidDatetimeStudent'])->name('checkValidDatetimeStudent');
Route::post('/createSession', [App\Http\Controllers\SessionManagementController::class, 'createSession'])->name('createSession');
Route::post('/ajaxcheckValidDatetime', [App\Http\Controllers\SessionManagementController::class,'checkValidDatetime'])->name('checkValidDatetime');
Route::post('/ajaxcheckValidDatetime2', [App\Http\Controllers\SessionManagementController::class,'checkValidDatetime2'])->name('checkValidDatetime2');
Route::post('/editSession', [App\Http\Controllers\SessionManagementController::class, 'editSession'])->name('editSession');
Route::post('/deleteSession', [App\Http\Controllers\SessionManagementController::class, 'deleteSession'])->name('deleteSession');
Route::post('/submitAttendance', [App\Http\Controllers\SessionManagementController::class,'submitAttendance'])->name('submitAttendance');
Route::post('/submitSessionProgress', [App\Http\Controllers\SessionManagementController::class,'submitSessionProgress'])->name('submitSessionProgress');
Route::any('classDetails/{classID}/sessionHistory', [App\Http\Controllers\SessionManagementController::class, 'sessionHistoryView'])->name('sessionHistoryView');
Route::any('getSessionHistory', [App\Http\Controllers\SessionManagementController::class, 'sessionHistory'])->name('sessionHistory');
Route::any('classDetails/{classID}/sessionHistory/sessionDetails/{sessionID}', [App\Http\Controllers\SessionManagementController::class, 'sessionDetails'])->name('sessionHistory');

Route::any('userProfile/{userID}/creditDetails', [App\Http\Controllers\CreditManagementController::class, 'creditDetails'])->name('userProfile/creditDetails');
Route::any('/topupCredit', [App\Http\Controllers\CreditManagementController::class, 'topupCredit'])->name('topupCredit');
Route::any('getCreditBalance', [App\Http\Controllers\CreditManagementController::class, 'getCreditBalance'])->name('getCreditBalance');
Route::any('/getCreditDetails', [App\Http\Controllers\CreditManagementController::class, 'getCreditDetails'])->name('getCreditDetails');
Route::any('/payTeacherCredit', [App\Http\Controllers\CreditManagementController::class, 'payTeacherCredit'])->name('payTeacherCredit');
Route::any('creditDetails/{userID}', [App\Http\Controllers\CreditManagementController::class, 'creditDetails'])->name('creditDetails');
Route::any('creditManagement', [App\Http\Controllers\CreditManagementController::class, 'creditManagement'])->name('creditManagement');
Route::any('topupCreditReceipt', [App\Http\Controllers\CreditManagementController::class, 'topupCreditReceipt'])->name('topupCreditReceipt');

Route::get('send-sms-notification', [App\Http\Controllers\NotificationController::class, 'sendSmsNotificaition']);


// Route::post('/order', 'OrderController@store')->name('order')->middleware('auth');







// Route::get('/index/logout', function () {
//     return view('logout');
// });



// Route::post('/ajaxcheckValidDatetime','App\Http\Controllers\SessionManagementController@checkValidDatetime');
// Route::get('/userManagement', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

