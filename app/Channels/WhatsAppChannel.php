<?php
namespace App\Channels;

use Illuminate\Notifications\Notification;
use App\Notifications\NotifySession;
use Twilio\Rest\Client;

class WhatsAppChannel
{
    public function send($notifiable, NotifySession $notification)
    {
        $message = $notification->toWhatsApp($notifiable);


        $to = "+6".$notifiable->routeNotificationFor('WhatsApp');
        $from = "+14155238886";

        # Your Account SID from twilio.com/console
        $account_sid = "AC6c706137228816bc95e0098884b0fbf3";
        # Your Auth Token from twilio.com/console
        $auth_token  = "e56c751a13fcb4cfdf0c247afbbe9225";


        $twilio = new Client($account_sid, $auth_token);

        return $twilio->messages->create('whatsapp:' . $to, [
            "from" => 'whatsapp:' . $from,
            "body" => $message->content
        ]);
    }
}