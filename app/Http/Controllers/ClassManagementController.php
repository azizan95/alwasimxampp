<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Class_;
use App\Models\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules\Unique;
use Carbon\Carbon;
use App\Models\Credit;
use DateTime;


class ClassManagementController extends Controller
{
    protected function index()
    {
        $classes = $this->getClasses();
        $teachers = $this->getTeachers();
        return view('/classmanagement', compact('classes', 'teachers'));
    }

    public function selectSearch(Request $request)
    {
    	$teachers = [];

        if($request->has('q')){
            $search = $request->q;
            $teachers =User::select("id", "name", "userPhone")
            		->where('name', 'LIKE', "%$search%")
                    ->where('userRole', '=', 'Teacher')
                    ->where('userStatus', '=', 1)
            		->get();
        }
        return response()->json($teachers);
    }

    protected function classDetails(Request $request)
    {
        $id = $request['id'];
        $class_ = Class_::find($id);
        $teachers = $this->getTeachers();
        $sessions = $this->getSessions($request['id']);
        return view('/classdetails', compact('class_', 'teachers', 'sessions'));
    }

    protected function editClass(Request $request)
    {
        $id = $request['classID'];
        $class_ = Class_::find($id);
        if(strtoupper($class_['className']) != strtoupper($request['className'])){
            $this->validatorEdit($request->all())->validate();
        }
        $class_['className'] = $request['className'];
        $class_['classDesc'] = $request['classDesc'];
        if($class_->classType == 'Special'){
            $class_->classCost = $request['classCost'];
            $class_->classDateStart = $request['classDateStart'];
            $class_->classDateEnd = $request['classDateEnd'];
        }

        $class_->user()->associate($request['teacherID']);
        $class_->save();

        return redirect('classDetails/'.$class_['id']);
    }

    protected function deleteClass(Request $request){
        $id = $request['classID'];

        $class_ = Class_::find($id);
        // $class_->delete();

        $deletedClass = $class_;
        $deleteSuccess = 1;
        $classes = $this->getClasses();
        $teachers = $this->getTeachers();
        return view('/classmanagement', compact('deletedClass', 'deleteSuccess', 'classes', 'teachers'));
    }

    protected function getClasses(){
        $classes = Class_::with('user')->get();
        return $classes;
    }

    protected function getTeachers(){
        $teachers = User::all()->where('userRole', 'Teacher')->toArray();
        return $teachers;
    }

    protected function getSessions(int $request){
        $sessions = Session::all()->where('class_id', $request)->toArray();
        return $sessions;
    }

    public function createClass(Request $request){
        $this->validator($request->all())->validate();
        $this->create($request->all());

        $classes = $this->getClasses();
        $teachers = $this->getTeachers();
        $adminAdd = 1;
        $addedClass = $request['className'];
        return redirect('/classManagement')->with(['classes' => $classes, 'teachers' => $teachers, 'adminAdd' => $adminAdd, 'addedClass' => $addedClass]);;

    }

    public function enrollClass(Request $request){
        $classID = $request['classID'];
        $studentID = $request['studentID'];

        $class_ = Class_::find($classID);
        if($class_->classType == "Special"){
            $currentDateTime = Carbon::now();
            $currentDate = date('Y-m-d', strtotime($currentDateTime));
            $credit = Credit::create([
                'creditTransaction' => 'Special Class',
                'creditDate' => $currentDate,
                // 'creditDebit' => $session->class_->classCost,
                'creditDebit' => $class_->classCost
            ]);
            $credit->user()->associate($studentID);
            $credit->class_()->associate($classID);
            $credit->save();
        }
        $class_->enrolledBy()->attach($studentID);
        $classEnrollLeave = "Successfully enroll to the class!";
        return redirect('classDetails/'.$class_['id'])->with(['classEnrollLeave' => $classEnrollLeave]);;
    }

    public function leaveClass(Request $request){
        
        $studentID = $request['studentID'];
        $classID = $request['classID'];
        $class_ = Class_::find($classID);
        $dateTime = Carbon::now();
        $date = date('Y-m-d',  strtotime($dateTime));

        foreach($class_->session->where('sessionDate', '>=', $date) as $session){
            $sessionTimeDate = Date($session['sessionDate'] . $session['sessionTime']);
            $sessionTimeDate = Date('d-m-Y H:i', strtotime($sessionTimeDate));
            $currentTimeDate = Date('d-m-Y H:i');
            $sessiondate = new DateTime(Date('d-m-Y H:i', strtotime($sessionTimeDate)));
            $currentdate = new DateTime(Date('d-m-Y H:i', strtotime($currentTimeDate)));
            if($sessiondate > $currentdate){
                $session->bookedBy()->detach($studentID);
                info($studentID);
            }
        }

        if($class_->classType == "Special"){
            // $currentDateTime = Carbon::now();
            // $currentDate = date('Y-m-d', strtotime($currentDateTime));
            $credit = Credit::where('user_id', $studentID)->where('class__id', $classID);
            $credit->delete();
        }

        $class_->enrolledBy()->detach($studentID);
        $classEnrollLeave = "Successfully leave the class!";
        
        return redirect('classDetails/'.$class_['id'])->with(['classEnrollLeave' => $classEnrollLeave]);;
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'className' => ['required', 'string', 'max:100', 'unique:classes'],
            'classDesc' => ['required', 'string', 'max:255'],
        ]);
    }
    
    protected function validatorEdit(array $data)
    {
        return Validator::make($data, [
            'className' => ['required', 'string', 'max:100', 'unique:classes'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $class_ = Class_::create([
            'className' => $data['className'],
            'classDesc' => $data['classDesc'],
            'classType' => $data['classType'],
        ]);
        if($class_->classType == 'Special'){
            $class_->classCost = $data['classCost'];
            $class_->classDateStart = $data['classDateStart'];
            $class_->classDateEnd = $data['classDateEnd'];
        }
        $class_->user()->associate($data['teacherID']);
        $class_->save();
        return $class_;
    }
}
