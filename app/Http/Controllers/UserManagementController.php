<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;


class UserManagementController extends Controller
{
    protected function index()
    {
        $users = $this->getApprovedUsers();
        $usersNoApproved = $this->getUnapprovedUsers();

        return view('/usermanagement', compact('users', 'usersNoApproved'));
    }

    protected function userProfile(Request $request)
    {
        $id = $request['id'];
            // if(Auth::user()->is_admin == 1){
            // }
            // $minFee = Feechanges::orderby('id', 'DESC')->get()->first();
            // $flag = 0;
            // $total = 0;
            // $year = date('Y');
            // echo $year;
            // echo $id;
            info("asdasd");
        $user = User::find($id);
        if($user!=null){
            $firstName = explode(" ", $user['name']);
        }else{
            return redirect('userProfile/'.Auth::user()->id);
        }
        // $feesTotal = Fee::where('year', $year)->where('status', 1)->where('userID',$user['id'])->get()->toarray();
        // $fees = Fee::where('status', null)->where('userID', $user['id'])->orderby('id', 'DESC')->get()->toarray();
        // $feesHistory = Fee::where('status', 1)->where('userID', $user['id'])->orderby('id', 'DESC')->get()->toarray();
        return view('/userprofile', compact('user', 'firstName'));
    }

    protected function editUser(Request $request)
    {
        $id = $request['userID'];
        $user = User::find($id);

        if($user['email'] != $request['email']){
            $this->validatorEdit($request->all())->validate();
        }
        
        $user['name'] = $request['name'];
        $user['email'] = $request['email'];
        $user['userPhone'] = $request['userPhone'];
        $user['userGender'] = $request['userGender'];
        $user['userRole'] = $request['userRole'];

        $document = $request->file('fileToUpload');
        if($document != null){
            $document->getRealPath();
            $document->getClientOriginalName();
            $document->getClientOriginalExtension();
            $document->getSize();
            $document->getMimeType();
            $fileName = $id.".jpg";
            $destinationPath = "images/profilepictures";
            
            $document->move($destinationPath, $fileName);
        }
        $user->save();
        
        echo($user['email'] );

        $editProfileSuccess = true;

        return redirect('userProfile/'.$user['id'])->with(['editProfileSuccess' => $editProfileSuccess]);
    }

    protected function deleteUser(Request $request){
        $id = $request['userID'];
        $user = User::find($id);
        $deletedUser = $user;
        // $user->delete();

        return redirect('/userManagement')->with(['deletedUser'=> $deletedUser]);
    }

    public function adminRegister(Request $request){
        $this->validator($request->all())->validate();
        $this->create($request->all());

        $lastUser = User::orderby('id', 'DESC')->get()->first();
        $addedUser = $lastUser['name'];

        return redirect('/userManagement')->with(['addedUser'=> $addedUser]);

    }

    protected function approveUsers(Request $request){
        $ids = $request->input('id');
        $approvedUsersCounter = 0;
		foreach($ids as $userID){
            $user = User::find($userID);
            $user['userStatus'] = 1;
			$user->save();
            $approvedUsersCounter++;
        }
        return redirect('/userManagement')->with(['approvedUsersCounter'=> $approvedUsersCounter]);
    }

    protected function rejectUsers(Request $request){
        $ids = $request->input('id');
        $rejectedUsersCounter = 0;

		foreach($ids as $userID){
            $user = User::find($userID);
			// $user->delete();
            $rejectedUsersCounter++;
        }
        return redirect('/userManagement')->with(['rejectedUsersCounter'=> $rejectedUsersCounter]);
    }

    protected function changePassword(Request $request){
        $this->validatorCurrentPassword($request->all());
        $this->validatorChangePassword($request->all())->validate();

        User::find(auth()->user()->id)->update(['password'=> Hash::make($request->newPassword)]);
        $changePasswordSuccess = true;

        return redirect('userProfile/'.auth()->user()->id)->with(['changePasswordSuccess'=> $changePasswordSuccess]);
    }

    /************************************************************************************************/

    protected function getApprovedUsers(){
        $users = User::all()->where('userStatus', 1)->toArray();
        return $users;
    }

    protected function getUnapprovedUsers(){
        $usersNoApproved = User::all()->where('userStatus', null)->toArray();
        return $usersNoApproved;
    }

    protected function validator(array $data){
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    protected function validatorEdit(array $data){
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);
    }

    protected function validatorCurrentPassword(array $data){
        if(!Hash::check($data['currentPassword'], auth()->user()->password)){
            throw ValidationException::withMessages(['currentPassword' => 'Incorrect current password']);
        }
    }

    protected function validatorChangePassword(array $data){
        return Validator::make($data, [
            'currentPassword' => ['required', 'string', 'min:8', ''],
            'newPassword' => ['required', 'string', 'min:8'],
            'confirm_newPassword' => ['required', 'same:newPassword'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'userGender' => $data['userGender'],
            'userPhone' => $data['userPhone'],
            'userStatus' => 1,
            'userRole' => $data['userRole'],
            'password' => Hash::make($data['password']),
        ]);
    }
    /************************************************************************************************/

}