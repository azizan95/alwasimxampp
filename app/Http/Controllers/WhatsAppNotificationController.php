<?php

namespace App\Http\Controllers;
use App\Models\Session;
use Carbon\Carbon;
use App\Notifications\NotifySession;

use Illuminate\Http\Request;

class WhatsAppNotificationController extends Controller
{
    public function __invoke(){
        $currentDateTime = Carbon::now();
        $currentDate = date('Y-m-d', strtotime($currentDateTime));
        $currentTimePlus10Mins = date('H:i', strtotime($currentDateTime->addMinutes(10)));
        $sessions = Session::where('sessionDate', '=', $currentDate)->where('sessionTime', '=', $currentTimePlus10Mins)->get();

        foreach($sessions as $session){
            foreach($session->bookedBy as $student){
                $student->notify(new NotifySession($session)); //idk
            }
        }
    }
}
