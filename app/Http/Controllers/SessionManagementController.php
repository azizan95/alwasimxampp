<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Class_;
use App\Models\Session;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Collection;
use App\Models\Credit;


class SessionManagementController extends Controller
{
    protected function sessionDetails(Request $request)
    {
        $id = $request['sessionID'];
        $session = Session::find($id);
        return view('/sessionDetails', compact('session'));
    }

    public function createSession(Request $request)
    {
        $this->create($request->all());
        $sessionAdded = true;
        return redirect('classDetails/'.$request['classID'])->with(['sessionAdded'=> $sessionAdded]);
    }

    protected function create(array $data)
    {
        $time = Carbon::parse($data['sessionTime']);
        $sessionTimeEnd = $time->addMinutes($data['sessionDuration']);
        $sessionTimeEnd = Date('H:i', strtotime($sessionTimeEnd));
        $session = Session::create([
            'sessionTime' => $data['sessionTime'],
            'sessionDate' => $data['sessionDate'],
            'sessionCapacity' => $data['sessionCapacity'],
            'sessionDuration' => $data['sessionDuration'],
            'sessionCost' => $data['sessionCost'],
            'sessionTimeEnd' => $sessionTimeEnd,
        ]);
        $session->class_()->associate($data['classID']);
        $session->save();
        echo($session);

        return $session;
    }
    
    public function checkValidDatetime(Request $request) //edit session
    {
        $time = Carbon::parse($request['sessionTime']);
        $sessionTimeEnd = $time->addMinutes($request['sessionDuration']);
        $sessionTimeEnd = Date('H:i', strtotime($sessionTimeEnd));
        $sessionDuration = $request['sessionDuration'];
        $sessionDate = $request->sessionDate;
        $sessionTime = date('H:i', strtotime($request->sessionTime));
        $sessionID = $request->sessionID;
        $teacherID = $request->teacherID;
        $checkFinal = false;
        info("Time : ". $time . " | sessionTimeEnd : " . $sessionTimeEnd. " | sessionDuration : " . $sessionDuration);

        $session = Session::find($sessionID);

        
        // original code start
        // if($session['sessionDate'] != $sessionDate || date('H:i', strtotime($session['sessionTime'])) != $sessionTime){
        //     // $sessions = Session::where('sessionDate', $sessionDate)->where('sessionTime', $sessionTime)->get();
        //     $sessions = Session::where('sessionDate', $sessionDate)->where('sessionTime', '<=', $sessionTime)->where('sessionTimeEnd', '>=', $sessionTime)->get();
        //     foreach($sessions as $session){
        //         if($session->class_->user->id == $teacherID){
        //             $check = true;
        //         }
        //     }
        // }else{
        //     $check = false;
        // }
        // original code end

        // info($sessionDate." || ".$sessionTime." || ".$sessionDuration." || ".$sessionTimeEnd." || sessionID ".$sessionID);


        if($session['sessionDate'] != $sessionDate || date('H:i', strtotime($session['sessionTime'])) != $sessionTime || $session['sessionDuration'] != $sessionDuration){
            $sessions = Session::
                    where(function($q) use ($sessionDate,  $sessionTime){
                        $q->where('sessionStatus', '!=', "Cancelled")->where('sessionDate', $sessionDate)->where('sessionTime', '<=', $sessionTime)->where('sessionTimeEnd', '>=', $sessionTime);}) // check new start time overlapped overlapped with exist
                    ->orwhere(function($q) use ($sessionDate,  $sessionTime, $sessionTimeEnd){
                        $q->where('sessionStatus', '!=', "Cancelled")->where('sessionDate', $sessionDate)->where('sessionTime', '>=', $sessionTime)->where('sessionTimeEnd', '<=', $sessionTimeEnd);}) // check new exist session time overlapped with new
                    ->orwhere(function($q) use ($sessionDate,  $sessionTimeEnd){
                        $q->where('sessionStatus', '!=', "Cancelled")->where('sessionDate', $sessionDate)->where('sessionTime', '<=', $sessionTimeEnd)->where('sessionTimeEnd', '>=', $sessionTimeEnd);}) // check new end time overlapped with exist
                    ->get();
            if($sessions->count() != 0){
                foreach($sessions as $session){
                    if($session->class_->user->id == $teacherID && $session->id != $sessionID){
                       info($session->id);
                        $checkFinal = true;
                    }
                }   
            }else{
                $checkFinal = false;
            }
        }else{
            $checkFinal = false;
        }
        return response()->json(['check' => $checkFinal]);
    }

    public function checkValidDatetime2(Request $request) //create session
    {
        $time = Carbon::parse($request['sessionTime']);
        $sessionTimeEnd = $time->addMinutes($request['sessionDuration']);
        $sessionTimeEnd = Date('H:i', strtotime($sessionTimeEnd));
        $sessionDate = $request->sessionDate;
        $sessionTime = date('H:i', strtotime($request->sessionTime));
        $teacherID = $request->teacherID;
        $checkFinal = false;
        info("Time : ". $sessionTime . " | sessionTimeEnd : " . $sessionTimeEnd. " | sessionDuration : " . $request['sessionDuration']);
        $sessions = Session::
                    where(function($q) use ($sessionDate,  $sessionTime){
                        $q->where('sessionStatus', '!=', "Cancelled")->where('sessionDate', $sessionDate)->where('sessionTime', '<=', $sessionTime)->where('sessionTimeEnd', '>=', $sessionTime);}) // check new start time overlapped overlapped with exist
                    ->orwhere(function($q) use ($sessionDate,  $sessionTime, $sessionTimeEnd){
                        $q->where('sessionStatus', '!=', "Cancelled")->where('sessionDate', $sessionDate)->where('sessionTime', '>=', $sessionTime)->where('sessionTimeEnd', '<=', $sessionTimeEnd);}) // check new exist session time overlapped with new
                    ->orwhere(function($q) use ($sessionDate,  $sessionTimeEnd){
                        $q->where('sessionStatus', '!=', "Cancelled")->where('sessionDate', $sessionDate)->where('sessionTime', '<=', $sessionTimeEnd)->where('sessionTimeEnd', '>=', $sessionTimeEnd);}) // check new end time overlapped with exist
                    ->get();
        if($sessions->count() != 0){
            foreach($sessions as $session){
                if($session->class_->user->id == $teacherID){
                    $checkFinal = true;
                }
            }   
        }else{
            $checkFinal = false;
        }
        return response()->json(['sessionDate' => $sessionDate, 'sessionTime' => $sessionTime, 'teacherID' => $teacherID, 'check' => $checkFinal]);
    }

    protected function checkValidDatetimeStudent(Request $request)
    {
        $time = Carbon::parse($request['sessionTime']);
        $sessionTimeEnd = $time->addMinutes($request['sessionDuration']);
        $sessionTimeEnd = Date('H:i', strtotime($sessionTimeEnd));
        $ymd = explode("/", $request['sessionDate']);
        $year = $ymd[2];
        $month = $ymd[1];
        $day = $ymd[0];
        $sessionDate = date('Y-m-d', strtotime($year.'-'.$month.'-'.$day));
        $sessionTime = date('H:i', strtotime($request->sessionTime));
        $checkFinal = false;
        $sessions = Session::where(function($q) use ($sessionDate,  $sessionTime){
                            $q->where('sessionStatus', '!=', "Cancelled")->where('sessionDate', $sessionDate)->where('sessionTime', '<=', $sessionTime)->where('sessionTimeEnd', '>=', $sessionTime);}) // check new start time overlapped overlapped with exist
                        ->orwhere(function($q) use ($sessionDate,  $sessionTime, $sessionTimeEnd){
                            $q->where('sessionStatus', '!=', "Cancelled")->where('sessionDate', $sessionDate)->where('sessionTime', '>=', $sessionTime)->where('sessionTimeEnd', '<=', $sessionTimeEnd);}) // check new exist session time overlapped with new
                        ->orwhere(function($q) use ($sessionDate,  $sessionTimeEnd){
                            $q->where('sessionStatus', '!=', "Cancelled")->where('sessionDate', $sessionDate)->where('sessionTime', '<=', $sessionTimeEnd)->where('sessionTimeEnd', '>=', $sessionTimeEnd);}) // check new end time overlapped with exist
                        ->get();
        // $sessions = Session::where('sessionStatus', '!=', 'Cancelled')
        //             ->where(function($q) use ($sessionDate,  $sessionTime){
        //                 $q->where('sessionDate', $sessionDate)->where('sessionTime', '<=', $sessionTime)->where('sessionTimeEnd', '>=', $sessionTime);}) // check new start time overlapped overlapped with exist
        //             ->orwhere(function($q) use ($sessionDate,  $sessionTime, $sessionTimeEnd){
        //                 $q->where('sessionDate', $sessionDate)->where('sessionTime', '>=', $sessionTime)->where('sessionTimeEnd', '<=', $sessionTimeEnd);}) // check new exist session time overlapped with new
        //             ->orwhere(function($q) use ($sessionDate,  $sessionTimeEnd){
        //                 $q->where('sessionDate', $sessionDate)->where('sessionTime', '<=', $sessionTimeEnd)->where('sessionTimeEnd', '>=', $sessionTimeEnd);}) // check new end time overlapped with exist
        //             ->get();
        if($sessions->count() != 0){
            foreach($sessions as $session){
                if($session->bookedBy->count() != 0){
                    foreach($session->bookedBy as $student){
                        if($student['id'] == Auth::User()->id){
                            info($student['name']);
                            $checkFinal = true;
                        }
                    }
                }
            }  
        }else{
            $checkFinal = false;
        }
        return response()->json(['sessionDate' => $sessionDate, 'sessionTime' => $sessionTime, 'check' => $checkFinal]);
        // // info($request['sessionDate'] . " || " . $sessionDate . ' || ' . $sessionTime . ' || ' . $sessionTimeEnd . " || " . $request['sessionDuration']);
        // // check new start time overlapped overlapped with exist
        // $sessions = Session::where('sessionStatus', '!=', 'Cancelled')->where('sessionDate', $sessionDate)->where('sessionTime', '<=', $sessionTime)->where('sessionTimeEnd', '>=', $sessionTime)->get();
        // // info($sessions->count());
        // if($sessions->count() != 0){
        //     foreach($sessions as $session){
        //         if($session->bookedBy->count() != 0){
        //             // info($session->bookedBy->count());
        //             foreach($session->bookedBy as $student){
        //                 if($student['id'] == Auth::User()->id){
        //                     info($student['name']);
        //                     $check = true;
        //                 }
        //             }
        //         }
        //     }  
        // }else{
        //     $check = false;
        // }

        // // check new exist session time overlapped with new
        // $sessions = Session::where('sessionStatus', '!=', 'Cancelled')->where('sessionDate', $sessionDate)->where('sessionTime', '>=', $sessionTime)->where('sessionTimeEnd', '<=', $sessionTimeEnd)->get();
        // // info($sessions->count());
        // if($sessions->count() != 0){
        //     foreach($sessions as $session){
        //         if($session->bookedBy->count() != 0){
        //             foreach($session->bookedBy as $student){
        //                 if($student['id'] == Auth::User()->id){
        //                     $check2 = true;
        //                 }
        //             }
        //         }
        //     }   
        // }else{
        //     $check2 = false;
        // }

        // // check new end time overlapped with exist
        // $sessions =  Session::where('sessionStatus', '!=', 'Cancelled')->where('sessionDate', $sessionDate)->where('sessionTime', '<=', $sessionTimeEnd)->where('sessionTimeEnd', '>=', $sessionTimeEnd)->get();
        // // info($sessions->count());
        // if($sessions->count() != 0){
        //     foreach($sessions as $session){
        //         if($session->bookedBy->count() != 0){
        //             foreach($session->bookedBy as $student){
        //                 if($student['id'] == Auth::User()->id){
        //                     $check3 = true;
        //                 }
        //             }
        //         }
        //     }  
        // }else{
        //     $check3 = false;
        // }

        // if(!$check && !$check2 && !$check3){
        //     $checkFinal = false;
        // }else{
        //     $checkFinal = true;
        // }
    }
    

    protected function editSession(Request $request)
    {
        $time = Carbon::parse($request['sessionTime']);
        $sessionTimeEnd = $time->addMinutes($request['sessionDuration']);
        $sessionTimeEnd = Date('H:i', strtotime($sessionTimeEnd));
        $id = $request['sessionID'];
        $session = Session::find($id);
        $session['sessionDate'] = $request['sessionDate'];
        $session['sessionTime'] = $request['sessionTime'];
        $session['sessionCapacity'] = $request['sessionCapacity'];
        $session['sessionDuration'] = $request['sessionDuration'];
        $session['sessionCost'] = $request['sessionCost'];
        $session['sessionTimeEnd'] = $sessionTimeEnd;
        $session->save();
        $sessionEdited = true;
        return redirect('classDetails/'.$session->class_->id.'/sessionDetails/'.$id)->with(['sessionEdited'=> $sessionEdited]);
    }

    protected function deleteSession(Request $request)
    {
        $id = $request['sessionID'];
        $session = Session::find($id);
        $deletedSession = $session;
        $session->delete();
        return redirect('classDetails/'.$session->class_->id)->with(['deletedSession'=> $deletedSession]);
    }

    public function bookSession(Request $request)
    {
        $sessionID = $request['sessionID'];
        $studentID = $request['studentID'];
        $session = Session::find($sessionID);
        if($session->bookedBy()->count() < $session['sessionCapacity']){
            $session->bookedBy()->attach($studentID);
            if($session->class_->calssType == "Normal"){
                // Credit part *******************************************
                $currentDateTime = Carbon::now();
                $currentDate = date('Y-m-d', strtotime($currentDateTime));
                $credit = Credit::create([
                    'creditTransaction' => 'Session Booking',
                    'creditDate' => $currentDate,
                    // 'creditDebit' => $session->class_->classCost,
                    'creditDebit' => $session->sessionCost
                ]);
                $credit->user()->associate($studentID);
                $credit->session()->associate($sessionID);
                $credit->save();
                // Credit part *******************************************
            }
            $sessionBookCancel = "Successfully book this session!";
        }else{
            $sessionBookCancel = "Sorry, session is full.";
        }
        return redirect('classDetails/'.$session->class_->id.'/sessionDetails/'.$sessionID)->with(['sessionBookCancel' => $sessionBookCancel]);
    }

    public function cancelBooking(Request $request)
    {
        $sessionID = $request['sessionID'];
        $studentID = $request['studentID'];
        $session = Session::find($sessionID);
        info("studentID ".$studentID);
        
        if($session->class_->classType == "Normal"){
            $credit = Credit::where('user_id', $studentID)->where('session_id', $sessionID)->first();
            $credit->delete();
        }

       
        $session->bookedBy()->detach($studentID);
        $sessionBookCancel = "Successfully cancel the booking!";
        return redirect('classDetails/'.$session->class_->id.'/sessionDetails/'.$sessionID)->with(['sessionBookCancel' => $sessionBookCancel]);
    }

    public function addSessionLink(Request $request)
    {
        $id = $request['sessionID'];
        $session = Session::find($id);
        $session['sessionLink'] = $request['sessionLink'];
        $session['sessionStatus'] = "Confirmed";
        $session->save();
        $sessionLinkAdded = true;
        return redirect('classDetails/'.$session->class_->id.'/sessionDetails/'.$id)->with(['sessionLinkAdded' => $sessionLinkAdded]);
    }

    
    public function confirmSessionBooking(Request $request)
    {
        $sessionID = $request['sessionID'];
        $session = Session::find($sessionID);
        $session['sessionStatus'] = "Confirmed";    
        $session->save();
        echo('done');
        $confirmcancelSessionBooking = "Session Confirmed!";
        return redirect('classDetails/'.$session->class_->id.'/sessionDetails/'.$sessionID)->with(['confirmcancelSessionBooking' => $confirmcancelSessionBooking]);
    }

    public function cancelSessionBooking(Request $request)
    {
        $sessionID = $request['sessionID'];
        $session = Session::find($sessionID);
        $session['sessionStatus'] = "Cancelled";
        $session->save();
        echo('done');
        $confirmcancelSessionBooking = "Session Cancelled!";
        return redirect('classDetails/'.$session->class_->id.'/sessionDetails/'.$sessionID)->with(['confirmcancelSessionBooking' => $confirmcancelSessionBooking]);
    }

    public function submitAttendance(Request $request)
    {
        $sessionID = $request['sessionID'];
        $ids = $request->input('studentID');
        $session = Session::find($sessionID);
        foreach($session->bookedBy as $student){ // set all no attend first
            $student->pivot->sessionAttendance = "Not Attend";
            $student->pivot->save();
        }
        foreach($ids as $studentID){ // then set attended for the checked ones
            $attendance = $session->bookedBy->find($studentID);
            $attendance->pivot->sessionAttendance = "Attended";
            $attendance->pivot->save();
        }
        $confirmUpdateAttendance = "Attendance Updated!";
        return redirect('classDetails/'.$session->class_->id.'/sessionDetails/'.$sessionID)->with(['confirmUpdateAttendance' => $confirmUpdateAttendance]);
    }

    public function submitSessionProgress(Request $request)
    {
        $sessionID = $request['sessionID'];
        $session = Session::find($sessionID);
        $session['sessionProgress'] = $request['sessionProgress'];
        $session->save();
        $confirmUpdateProgress = "Progress Updated!";
        return redirect('classDetails/'.$session->class_->id.'/sessionDetails/'.$sessionID)->with(['confirmUpdateProgress' => $confirmUpdateProgress]);
    }

    public function sessionHistoryView(Request $request)
    {
        $class_=Class_::find($request['classID']);
        return view('/sessionHistory', compact('class_'));
    }

    public function sessionHistory(Request $request)
    {
        $currentDateTime = Carbon::now();
        $currentDate = date('Y-m-d', strtotime($currentDateTime));
        $currentTime = date('H:i', strtotime($currentDateTime));
        $dateFrom = date('Y-m-d', strtotime($request['dateFrom']));
        $dateTo = date('Y-m-d', strtotime($request['dateTo']));
        $classID =$request['classID'];
        $sessions = Session::where(function($q) use($classID){
                        if(Auth::User()->userRole == "Student"){
                            $q->where('class__id', $classID)
                            ->whereHas('bookedBy', function($q){ //whereHas get Session->bookedBy (belongstomany), 
                                $q->where('user_id', Auth::User()->id); // where user_id = id
                            });
                        }else{
                            $q->where('class__id', $classID);
                        }
                    })
                    ->where(function($q) use($dateFrom){
                        $q->where('sessionDate', '>=', $dateFrom);
                    })
                    ->where(function($q) use($dateTo, $currentTime, $currentDate){ 
                        $q->where(function($q) use ($dateTo, $currentTime, $currentDate){
                            if($dateTo == $currentDate){
                                $q->where('sessionDate', $dateTo)->where('sessionTime', '<', $currentTime);
                            }else{
                                $q->where('sessionDate', $dateTo);
                            }
                        })
                        ->orwhere(function($q) use($dateTo){
                            $q->where('sessionDate', '<', $dateTo);
                        });
                    })
                    ->orderByDesc('sessionDate')
                    ->orderByDesc('sessionTime')
                    ->get();
        if(Auth::User()->userRole != "Student"){
            $sessions->map(function ($session) {
                $sessionAttendance = $session->bookedBy()->count();
                $session['sessionAttendance'] = $sessionAttendance;
                return $session;
            });
        }else{
            $sessions->map(function ($session) {
                $sessionAttendance = $session->bookedBy()->where('user_id', Auth::user()->id)->first()->pivot->sessionAttendance;
                $session['sessionAttendance'] = $sessionAttendance;
                return $session;
            });
        }
        info($sessions);
        return response()->json(['sessions' => $sessions]);
    }


    // ******************************************************************************************************************************************************************************** //
        // if($session['sessionDate'] != $sessionDate || date('H:i', strtotime($session['sessionTime'])) != $sessionTime || $session['sessionDuration'] != $sessionDuration){
        //     // check new start time overlapped overlapped with exist
        //     $sessions = Session::where('id', '!=',  $sessionID)->where('sessionDate', $sessionDate)->where('sessionTime', '<=', $sessionTime)->where('sessionTimeEnd', '>=', $sessionTime)->get();
        //     if($sessions->count() != 0){
        //         foreach($sessions as $session){
        //             if($session->class_->user->id == $teacherID){
        //                 $check = true;
        //             }
        //         }   
        //     }else{
        //         $check = false;
        //     }

        //     // check new exist session time overlapped with new
        //     $sessions = Session::where('id', '!=',  $sessionID)->where('sessionDate', $sessionDate)->where('sessionTime', '>=', $sessionTime)->where('sessionTimeEnd', '<=', $sessionTimeEnd)->get();
        //     if($sessions->count() != 0){
        //         foreach($sessions as $session){
        //             if($session->class_->user->id == $teacherID){
        //                 $check2 = true;
        //             }
        //         }   
        //     }else{
        //         $check2 = false;
        //     }

        //     // check new end time overlapped with exist
        //     $sessions =  Session::where('id', '!=',  $sessionID)->where('sessionDate', $sessionDate)->where('sessionTime', '<=', $sessionTimeEnd)->where('sessionTimeEnd', '>=', $sessionTimeEnd)->get();
        //     if($sessions->count() != 0){
        //         foreach($sessions as $session){
        //             if($session->class_->user->id == $teacherID){
        //                 $check3 = true;
        //             }
        //         }   
        //     }else{
        //         $check3 = false;
        //     }

        //     if(!$check && !$check2 && !$check3){
        //         $checkFinal = false;
        //     }else{
        //         $checkFinal = true;
        //     }
        // }else{
        //     $checkFinal = false;
        // }
    
}
