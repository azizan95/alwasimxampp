<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Credit;
use App\Models\Session;
use App\Models\Receipt;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;






class CreditManagementController extends Controller
{
    public function creditDetails(Request $request)
    {
        $user = User::find($request['userID']);
        $receipts = Receipt::where('user_id', $request['userID'])->where('receiptStatus','Pending')->orderBy('created_at')->get();
        return view('creditDetails', compact('user', 'receipts'));
    }

    public function topupCreditReceipt(Request $request){
        if(Auth::User()->userRole == "Admin"){
            $this->topupCredit($request);
            $topupSuccessful = "Topup Successful!";
        }else{
            $id = $request['userID'];
            $user = User::find($id);
            
            $receipt = Receipt::create([
                'receiptStatus' => 'Pending',
            ]);
            $receipt->user()->associate($user['id']);
            $receipt->save();

            $document = $request->file('fileToUpload2');
            info($document);
            if($document != null){
                $document->getRealPath();
                $document->getClientOriginalName();
                $document->getClientOriginalExtension();
                $document->getSize();
                $document->getMimeType();
                $fileExt = $document->getClientOriginalExtension();
                info($fileExt);
                $fileExt = strToLower($fileExt);
                if($fileExt == "png" || $fileExt == "jpg" || $fileExt == "jpeg"){
                    $fileName = $receipt->id.".jpg";
                }else{
                    $fileName = $receipt->id.".pdf";
                }
                $destinationPath = "images/receipts";
                $document->move($destinationPath, $fileName);
            }
            $topupSuccessful = "Receipt submitted!";
        }
        if(str_replace(url('/'), '', url()->previous()) == "/userProfile/" . $request['userID']){
            return redirect('userProfile/'. $request['userID'])->with(['topupSuccessful' => $topupSuccessful]);
        }
        if(str_replace(url('/'), '', url()->previous()) == "/userProfile/" . $request['userID'] . "creditDetails"){
            return redirect('userProfile/'. $request['userID'].'/creditDetails')->with(['topupSuccessful' => $topupSuccessful]);
        }
        return redirect('creditDetails/'.$request['userID'])->with(['topupSuccessful' => $topupSuccessful]);

    }

    public function topupCredit(Request $request)
    {
        $currentDateTime = Carbon::now();
        $currentDate = date('Y-m-d', strtotime($currentDateTime));
        info($request['receiptID']);
        
        $user = User::find($request['userID']);
        $credit = Credit::create([
            'creditTransaction' => 'Topup',
            'creditDate' => $currentDate,
            'creditCredit' => $request['topupAmount']
        ]);
        if(isset($request['receiptID'])){
            $receipt = Receipt::find($request['receiptID']);
            $receipt->receiptStatus = 'Approved';
            $receipt->save();

            $credit->creditTransaction = "Topup (Receipt ID : ".$receipt->id.")";
            $credit->receipt()->associate($receipt['id']);
        }
        $credit->user()->associate($user['id']);
        $credit->save();

        $topupSuccessful = "Topup Successful!";
        
        echo(str_replace(url('/'), '', url()->previous()));

        if(str_replace(url('/'), '', url()->previous()) == "/userProfile/" . $request['userID']){
            return redirect('userProfile/'. $user['id'])->with(['topupSuccessful' => $topupSuccessful]);
        }
        if(str_replace(url('/'), '', url()->previous()) == "/userProfile/" . $request['userID'] . "creditDetails"){
            return redirect('userProfile/'. $user['id'].'/creditDetails')->with(['topupSuccessful' => $topupSuccessful]);
        }
        return redirect('creditDetails/'.$user['id'])->with(['topupSuccessful' => $topupSuccessful]);
    }

    public function getCreditBalance(Request $request)
    {
        $creditBalance = 0;
        $user = User::find($request['userID']);
        if($user->credit()->count() > 0){
            foreach ($user->credit as $credit) {
                if ($credit->creditCredit != null) {
                    $creditBalance = $creditBalance + $credit->creditCredit;
                } else {
                    $creditBalance = $creditBalance - $credit->creditDebit;
                }
            }
        }
        return response()->json(['creditBalance' => $creditBalance]);
    }

    public function getCreditDetails(Request $request)
    {
        
        info($request['userID']);
        $credits = Credit::where('user_id', $request['userID'])->orderBy('created_at')->get();
        info($credits);
        $credits->map(function ($credit) {
            static $creditBalance = 0;
            $receiptStatus = "Approved";

            if ($credit->creditCredit != null) {
                $creditBalance += $credit->creditCredit;
            } else {
                $creditBalance -= $credit->creditDebit;
            }
            if($credit->receipt_id != null){
                $receiptStatus = $credit->receipt->receiptStatus;
            }
            $credit['creditBalance'] = $creditBalance;
            $credit['receiptStatus'] = $receiptStatus;
            return $credit;
        });
        $credits = $credits->sortByDesc(function ($q) {
            return $q;
        })->values()->all();
        return response()->json(['credits' => $credits]);
    }

    public function payTeacherCredit(Request $request)
    {
        if (Auth::User()->userRole == "Student") {
            info($request['userID']);
            info($request['sessionID']);
            $session = Session::find($request['sessionID']);
            $currentDateTime = Carbon::now();
            $currentDate = date('Y-m-d', strtotime($currentDateTime));
            if (Credit::where('student_id', Auth::User()->id)->where('user_id', $session->class_->user['id'])->where('session_id', $session['id'])->count() < 1) {
                $credit = Credit::create([
                    'creditTransaction' => 'Session Payment',
                    'creditDate' => $currentDate,
                    'creditCredit' => $session->SessionCost,
                ]);
                $credit->user()->associate($session->class_->user['id']);
                $credit->student()->associate(Auth::User()->id);
                $credit->session()->associate($session['id']);
                $credit->save();
            }
        }
        return response()->json();
    }

    public function creditManagement(){
        $users = User::all()->where('userStatus', 1)->where('userRole', 'Student');

        $users->map(function ($user) {
            $creditBalance = 0;
            // $user = User::find($user['id']);
            $credits = Credit::where('user_id', $user['id'])->orderBy('created_at')->get();

            foreach($credits as $credit){
                if ($credit->creditCredit != null) {
                    $creditBalance += $credit->creditCredit;
                } else {
                    $creditBalance -= $credit->creditDebit;
                }
            }
            $user['creditBalance'] = $creditBalance;
        });

        $receipts = Receipt::where('receiptStatus', 'Pending')->orderBy('created_at')->get();
        
        $receipts->map(function ($receipts) {
            $creditBalance = 0;
            // $user = User::find($user['id']);
            $credits = Credit::where('user_id', $receipts->user['id'])->orderBy('created_at')->get();

            foreach($credits as $credit){
                if ($credit->creditCredit != null) {
                    $creditBalance += $credit->creditCredit;
                } else {
                    $creditBalance -= $credit->creditDebit;
                }
            }
            $receipts['creditBalance'] = $creditBalance;
        });


        return view('creditManagement', compact('users', 'receipts'));
    }
}
