<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'userPhone',
        'userGender',
        'userStatus',
        'userRole',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function classes()
    {
        // return $this->belongsToMany(Class_::class);
        return $this->hasMany(Class_::class);
    }

    public function receipts()
    {
        // return $this->belongsToMany(Class_::class);
        return $this->hasMany(Receipt::class);
    }

    public function sessions()
    {
        // return $this->belongsToMany(Class_::class);
        return $this->hasManyThrough(Session::class, Class_::class);
    }

    public function enrolledIn()
    {
        return $this->belongsToMany(Class_::class)->withPivot('created_at');
    }

    public function booked()
    {
        return $this->belongsToMany(Session::class);
    }

    public function credit()
    {
        return $this->hasMany(Credit::class);
    }

    public function pay(){
        return $this->hasMany(Credit::class, 'student_id');
    }

    public function routeNotificationForWhatsApp()
    {
    return $this->userPhone;
    }
}
