<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Session extends Model
{
    use HasFactory;
    use Notifiable;

    protected $fillable = [
        'classId',
        'sessionTime',
        'sessionDate',
        'sessionReminder',
        'sessionCapacity',
        'sessionLink',
        'sessionStatus',
        'sessionDuration',
        'sessionTimeEnd',
        'sessionProgress',
        'sessionCost',
    ];

    public function class_()
    {
        return $this->belongsTo(Class_::class);
        // return $this->belongsTo('App\User', 'user_id');
    }

    public function user()
    {
        return $this->hasMany(User::class);
        // return $this->belongsTo('App\User', 'user_id');
    }

    public function bookedBy()
    {
        return $this->belongsToMany(User::class)->withPivot('created_at', 'sessionAttendance');
    }

    public function routeNotificationForTelegram()
    {
        return $this->id;
    }
}
