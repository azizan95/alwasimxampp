<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Class_ extends Model
{
    use HasFactory;

    protected $table = 'classes';
    
    protected $fillable = [
        'className',
        'classDesc',
        'classCost',
        'classType',
        'classDateStart',
        'classDateEnd',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function session()
    {
        return $this->hasMany(Session::class);
    }

    public function enrolledBy()
    {
        return $this->belongsToMany(User::class)->withPivot('created_at');
    }


}
