<?php

namespace App\Console;
use PHPMailer\PHPMailer\PHPMailer; 
use PHPMailer\PHPMailer\Exception;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Models\Session;
use Carbon\Carbon;
use App\Notifications\NotifySession;
use App\Notifications\LaravelTelegramNotification;
use Illuminate\Notifications\Notification;
use Twilio\Rest\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\WhatsAppNotificationController;
use App\Http\Controllers\NotificationController;

use App\Models\User;

// require __DIR__ . "/vendor/autoload.php";



class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        // Commands\sendSmsNotificaition::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->everyMinute();
        // $sendSMS = new ();
        // $schedule->command('send:sms')->everyMinute();

        // $schedule->call(new NotificationController)->everyMinute(); //<---- this one works.
        $schedule->call(new WhatsAppNotificationController)->everyMinute(); //<---- whatsapp luar schedule works

        $schedule->call(function () {
            
            // $sid = "AC6c706137228816bc95e0098884b0fbf3"; // Your Account SID from www.twilio.com/console
            // $token = "e56c751a13fcb4cfdf0c247afbbe9225"; // Your Auth Token from www.twilio.com/console
            // $client = new Client($sid, $token);
            // $message = $client->messages->create(
            //     '+60187881871', // Text this number
            //     [
            //         'from' => '+16822435503', // From a valid Twilio number
            //         'body' => 'Hello from Twilio!'
            //     ]
            // );
            

            // $dotenv = Dotenv\Dotenv::create(__DIR__);
            // $dotenv->load();

            // $twilioSid    = getenv('TWILIO_SID');
            // $twilioToken  = getenv('TWILIO_TOKEN');

            // $twilio = new Client($twilioSid, $twilioToken);

            // $message = $twilio->messages
            //                 ->create(
            //                     "whatsapp:+60187881871",
            //                     array(
            //                             "body" => "Greetings from Twilio :-)",
            //                             "from" => "whatsapp:+16822435503"
            //                         )
            //                 );
            
            $currentDateTime = Carbon::now();
            $currentDate = date('Y-m-d', strtotime($currentDateTime));
            $currentTimePlus10Mins = date('H:i', strtotime($currentDateTime->addMinutes(10)));
            // info("Current Datetime : ". $currentDateTime);

            $sessions = Session::where('sessionDate', '=', $currentDate)->where('sessionTime', '=', $currentTimePlus10Mins)->get();

            foreach($sessions as $session){
                foreach($session->bookedBy as $student){
                    // $student->notify(new NotifySession($session)); //idk
                    require_once "vendor/autoload.php";
                    
                    if($session->sessionLink == "" || $session->sessionLink == null){
                        $sessionLink = "http://localhost/public/classDetails/".$session->class_->id."/sessionHistory/sessionDetails/".$session->id;
                    }else{
                        $sessionLink = $session->sessionLink;
                    }

                    // $basic  = new \Nexmo\Client\Credentials\Basic('8df8dea3', '4P3Yxqu6nHiMqZeE');
                    // $client = new \Nexmo\Client($basic);
            
                    // $message = $client->message()->send([
                    //     'to' => "+6".$student->userPhone,
                    //     'from' => 'Al-Wasim',
                    //     'text' => $sessionLink
                    // ]);
                    // print $message->sid;

                    info("Current Datetime 2: ". $currentDateTime);

                    try {
			            $mail = new PHPMailer(true); // Passing `true` enables exceptions

                        //gmail configuration
                        // $mail->SMTPDebug = 4; // Enable verbose debug output
                        // $mail->isSMTP(); // Set mailer to use SMTP
                        // $mail->Host = 'smtp.gmail.com'; // Specify main and backup SMTP servers
                        // $mail->SMTPAuth = true; // Enable SMTP authentication
                        // $mail->Username = 'mr.azizanh@gmail.com'; // SMTP username
                        // $mail->Password = '96031905f'; // SMTP password
                        // $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
                        // $mail->Port = 587; // TCP port to connect to
                        // $mail->setFrom('mr.azizanh@gmail.com', 'Azizan');
                        // // $mail->addAddress($_POST['email']); // Add a recipient, Name is optional
                        // // $mail->addCC($_POST['email-cc']);
                        // // $mail->addBCC($_POST['email-bcc']);
                        // $mail->addReplyTo('mr.azizanh@gmail.com', 'Azizan');
                        //gmail configuration


                        // Mail server settings
                        // require_once "vendor/autoload.php";
                        $mail->SMTPDebug = 4; // Enable verbose debug output
                        $mail->isSMTP(); // Set mailer to use SMTP
                        $mail->Host = 'smtp.mailtrap.io'; // Specify main and backup SMTP servers
                        $mail->SMTPAuth = true; // Enable SMTP authentication
                        $mail->Username = '1af7e87f5650a2'; // SMTP username
                        $mail->Password = '322aff66ca2ee3'; // SMTP password
                        $mail->SMTPSecure = 'tls'; // Enable TLS encryption, `ssl` also accepted
                        $mail->Port = 2525; // TCP port to connect to
        
                        $mail->setFrom('mr.azizanh@gmail.com', 'Azizan');
                        // $mail->addAddress($_POST['email']); // Add a recipient, Name is optional
                        $mail->addAddress($student->email); // Add a recipient, Name is optional
                        
                        // $mail->addCC($_POST['email-cc']);
                        // $mail->addBCC($_POST['email-bcc']);
                        $mail->addReplyTo('mr.azizanh@gmail.com', 'Azizan');
                        // print_r($_FILES['file']); exit;
                        // for ($i=0; $i < count($_FILES['file']['tmp_name']) ; $i++) { 
                        // 	$mail->addAttachment($_FILES['file']['tmp_name'][$i], $_FILES['file']['name'][$i]); // Optional name
                        // }
        
                        $mail->isHTML(true); // Set email format to HTML
        
                        // $mail->Subject = $_POST['subject'];
                        $mail->Subject = 'Al-Wasim : Session start alert!';

                        
                        // $mail->Body    = $_POST['message'];
                        $mail->Body    = 'You have a session with '. $session->class_->user->name .' for '. $session->class_->className .' in 10 minutes. <br> Session Link : <a href = "'.$sessionLink.'">Here</a>';
                        
                        // $mail->AltBody = plain text version of your message;
                        
                        if( !$mail->send() ) {
                        echo 'Message could not be sent.';
                        info( 'Mailer Error: ' . $mail->ErrorInfo);
                        } else {
                        	echo 'Message has been sent';
                        }
        
                    } catch (Exception $e) {
                        // return back()->with('error','Message could not be sent.');
                        info( 'Mailer Error: ' . $mail->ErrorInfo);
                    }
                }
                // $this->sendSmsNotificaition($session->sessionLink, $session->bookedBy->toArray());
            }
        // redirect('send-sms-notification');
        })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }

    // protected function sendSmsNotificaition(String $sessionLink, Array $students)
    // {
    //     foreach($students as $student){
    //         $basic  = new \Nexmo\Client\Credentials\Basic('8df8dea3', '4P3Yxqu6nHiMqZeE');
    //         $client = new \Nexmo\Client($basic);

    //         $message = $client->message()->send([
    //             'to' => "+6".$student->userPhone,
    //             'from' => 'Al-Wasim',
    //             'text' => $sessionLink
    //         ]);
    //     }
    //     // dd('SMS message has been delivered.');
    // }
}
