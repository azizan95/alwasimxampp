<?php

namespace App\Notifications;

use App\Channels\Messages\WhatsAppMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use App\Channels\WhatsAppChannel;
Use App\Models\Session;

class NotifySession extends Notification
{
  use Queueable;


  public $session;
  
  public function __construct(Session $session)
  {
    $this->session = $session;
  }
  
  public function via($notifiable)
  {
    return [WhatsAppChannel::class];
  }
  
  public function toWhatsApp($notifiable)
  {
    // $orderUrl = url("/orders/{$this->session->id}");
    // $company = 'Acme';
    // $deliveryDate = $this->session->created_at->addDays(4)->toFormattedDateString();

    // Your {$company} order of {$this->order->name} has shipped and should be delivered on {$deliveryDate}. Details: {$orderUrl}
    return (new WhatsAppMessage)
        ->content("You have a session with {$this->session->class_->user->name} for {$this->session->class_->className} in 10 minutes. Link : {$this->session->sessionLink}");
  }
}