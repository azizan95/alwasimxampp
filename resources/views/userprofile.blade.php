<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }
    
    th, td {
        padding: 8px;
        text-align: left;
        border-top: 1px solid black;
    }

    tr:nth-child(odd) {
        background-color: #f2f2f2;
    }

    th {
        text-align: center;
        font-weight: normal;
        font-size: 15;
        background-color: #242d5f;
        color: white;
    }

    .buttonCleared:disabled {
        text-align: center;
        background-color: #4CAF50; 
        color: white; 
        border: 2px solid #409143;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .button1:enabled {
        background-color: white; 
        color: black; 
        border: 2px solid #4CAF50;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    #topupSubmitButton:disabled {
        background-color: white; 
        color: black; 
        border: 2px solid #4CAF50;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .button1:hover {
        background-color: #4CAF50;
        color: white;
        }
    
    .button1:disabled{
        background-color: #ffffff;
        color: grey; 
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonNoInfo:disabled {
        background-color: #a10c25; 
        color: white; 
        border: 2px solid #8a0b20;
        border-radius: 4px;
        /* width: 75px; */
        margin-top: 5px;
        margin-right: 0px;
        text-align: center;
    }

    .buttonReject:enabled {
        background-color: white; 
        color: black; 
        border: 2px solid #a10c25;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonReject:hover {
        background-color: #a10c25;
        color: white;
        }
    
    .buttonReject:disabled{
        background-color: #ffffff;
        color: grey; 
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .nostyle{
        -webkit-appearance: none; 
        border:1px solid;
       }

    .modal {
        display: block; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    .modal-content {
        /* background-color: #fefefe; */
        background-color: #3D3D3D;
        text-align: center;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        /* width: 10%; */
    } 

    .tdNoStyle {
    /* margin: 0; */
        padding: 0;
        border-top: none;
        border-left: none;
        border-right: none;
        outline: 0;
        font-size: 100%;
        vertical-align: baseline;
        background-color: #a10c25;
    }
</style>

@guest
please login

@else
    @extends('layouts.app')
    @section('content')
    
    @if(Auth::user()->userStatus == 0)
        {{ Session::flush() }}
        <script>
            window.alert("Your account is yet to be approved. Please wait for approval email from Admin");
            window.location = "/";
        </script>
    @else
        @if((Auth::user()->id != $user['id'] && Auth::user()->userRole != "Admin" && $user['userRole'] != 'Teacher'))
            <script>
                window.location ='{{ url("userProfile/".Auth::user()->id) }}';
            </script>
        @endif
        <script>
        </script>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="background-color: #a10c25; color:white; font-size:20; padding-bottom:0; height:50px; vertical-align:middle">
                            <table style="border:none; margin-bottom:0; padding:0;">
                                <tr>
                                    <th class="tdNoStyle" style="font-size:20; vertical-align:middle">
                                        <b>{{$firstName[0]}}</b>
                                    </th>
                                    <th class="tdNoStyle" style="text-align: right; padding:0; vertical-align:middle">
                                        @if(Auth::user()->id == $user['id'] || Auth::user()->userRole == "Admin")
                                            <button type="button" style="background-color:transparent; box-shadow: 0px 0px 0px transparent; border: none; text-shadow: 0px 0px 0px transparent; height:30; padding:0 " onclick="editModalShow()" >
                                                <img class="deletebutton1" src="/images/icons/editblue.png" style="width: 25" title="Edit user"/>
                                            </button>
                                            <button type="button" style="background-color:transparent; box-shadow: 0px 0px 0px transparent; border: none; text-shadow: 0px 0px 0px transparent; height:30; padding:0 " onclick="changePasswordModalShow()" >
                                                <img class="deletebutton1" src="/images/icons/changepasswordyellow.png" style="width: 25" title="Change password"/>
                                            </button>
                                        @endif
                                        @if(Auth::user()->userRole == "Admin")
                                            <button form="deleteForm" style="background-color:transparent; box-shadow: 0px 0px 0px transparent; border: none; text-shadow: 0px 0px 0px transparent; height:30; padding:0; margin-right: 2" onclick="return confirm('Confirm delete?')" >
                                                <img class="deletebutton2" src="/images/icons/deletered.png" style="width: 25" title="Delete user" />
                                            </button>
                                            <form name="deleteForm" id="deleteForm"  method="POST" action="{{ url('/deleteUser') }}">
                                                @csrf
                                                <input type="hidden" name="userID" value="{{$user['id']}}">
                                            </form>
                                        @endif
                                    </th>
                                </tr>
                            </table>
                        </div>
                        <div class="card-body" style="text-align: right">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <form name="ApprovalForm" method="POST" action="{{ url('/deleteUser') }}">
                                @csrf
                                <table style="border-collapse: collapse;">
                                    <tr>
                                        @php
                                            $ppLink = "images/profilepictures/".$user['id'].".jpg"; // no / at the link begining
                                        @endphp
                                        @if(file_exists($ppLink))
                                            <td style="text-align: center; border-top:none">
                                                <img id="ppLink" src="/public/{{$ppLink}}" height="250" alt="profile picture"> {{-- add / the begining to work. weird.... --}}
                                            </td>
                                        @endif
                                    </tr>
                                </table>
                                <table style="border: 1px solid black; width:100%;">
                                    <tr style="font-weight: bold">
                                        <th style="width: 5%; font-weight: bold; border:1px solid none">
                                            ID
                                        </th>
                                        <th colspan="" style="font-weight: bold; border:1px solid none">
                                            Name
                                        </th>
                                        <th colspan="" style="font-weight: bold; border:1px solid none">
                                            Email
                                        </th>
                                        <th colspan="2" style="font-weight: bold; border:1px solid none"> 
                                            Phone Number
                                        </th>
                                    </tr>
                                    <tr>
                                        <td style="font-weight: ; border:1px solid none">
                                            <span id='userID'>{{ __($user['id'])}}</span>
                                        </td>
                                        <td colspan="" style="width:30%; font-weight: ; border:1px solid none">
                                            {{ __($user['name'])}}
                                        </td>
                                        <td colspan="" style="font-weight: ; border:1px solid none">
                                            {{ __($user['email'])}}
                                        </td>
                                        <td colspan="2" style="border:1px solid none">
                                            {{ __($user['userPhone'])}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th colspan="2" style="font-weight: bold; border:1px solid none;">
                                            Gender
                                        </th>
                                        <th style="font-weight: bold; border:1px solid none">
                                            Role
                                        </th>
                                        <th style="font-weight: bold; border:1px solid none">
                                            @if ((Auth::User()->userRole == "Admin" || Auth::User()->id == $user['id']) && Auth::User()->userRole != "Teacher")
                                                Credit
                                            @endif
                                        </th>
                                        <th class="" style="text-align:; padding:0; vertical-align:middle">
                                            @if ((Auth::User()->userRole == "Admin" || Auth::User()->id == $user['id']) && $user->userRole != "Teacher")
                                                <button form="" style="background-color:transparent; box-shadow: 0px 0px 0px transparent; border: none; text-shadow: 0px 0px 0px transparent; height:30; padding:0; margin-right: 2" onclick="topupModalShow()" >
                                                    <img class="deletebutton2" src="/images/icons/topuporange.png" style="width: 25" title="Topup Credit" />
                                                </button>
                                            @endif
                                        </th>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style=" border:1px solid none">
                                            {{ __($user['userGender'])}}
                                        </td>
                                        <td style=" border:1px solid none">
                                            {{ __($user['userRole'])}}
                                        </td>
                                        <td colspan="2" style="width: 20%; border:1px solid none; padding-right: 0">
                                            @if ((Auth::User()->userRole == "Admin" || Auth::User()->id == $user['id']) && Auth::User()->userRole != "Teacher")
                                                <a id="creditBalance" style="text-decoration: none; color:black" href="{{url('/userProfile/'.$user['id'].'/creditDetails')}}"></a>
                                            @endif
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        @if($user['userRole'] == "Student")
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header" style="background-color: #a10c25; color:white; font-size:20"><b>Class Enrolled</b></div>
                            <div class="card-body" style="text-align: right">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <table style="border:none; width:50%">
                                    <tr class="nostyle" style="background-color: white; border:none;">
                                        <td style="border-top: none">
                                            Class name :
                                        </td>
                                        <td style="border-top: none">
                                            <input class="form-control" style="border-radius: 6px; padding-left:10px" type="text" id="classSearchInput" onkeyup="filterStudent()" placeholder="Filter by class name.." title="Type in a class name">
                                        </td>
                                    </tr>
                                </table>
                                <table style="border: 1px solid black; width:100%" id="classTable">
                                    <tr>
                                        <th style="font-weight: bold; width:10%; text-align:center">
                                            No
                                        </th>
                                        <th style="font-weight: bold">
                                            Name
                                        </th>
                                        <th style="font-weight: bold; text-align: center; width:10%">
                                            Sessions
                                        </th>
                                    </tr>
                                    @if($user->enrolledIn->count() == 0)
                                        <tr>
                                            <td colspan="3" style="text-align: center;">
                                                Not enrolled in any class yet!
                                            </td>
                                        </tr>
                                    @else
                                        @php
                                            $counter = 1;
                                        @endphp
                                        @foreach($user->enrolledIn as $class_)
                                        <tr>
                                            <td class="tdNo" style="text-align:center">
                                            </td>
                                            <td class="tdClassName">
                                                <a style="text-decoration: none; color:black" href="{{url('classDetails/'.$class_['id'])}}">{{ __($class_['className'])}}</a>
                                            </td>
                                            <td style="text-align: center">
                                                @php
                                                    $currentTimeDate = Date('d-m-Y H:i');
                                                    $sessionCounter = 0;
                                                    foreach($class_->session as $session1){
                                                        $sessionTimeDate = Date($session1['sessionDate'] . $session1['sessionTime']);
                                                        $sessionTimeDate = Date('d-m-Y H:i', strtotime($sessionTimeDate));
                                                        if(Date(strtotime($currentTimeDate)) < Date(strtotime($sessionTimeDate))){
                                                            $sessionCounter ++;
                                                        }
                                                    }
                                                @endphp
                                                {{$sessionCounter}}
                                            </td>
                                        @endforeach
                                        <tr id="trNoResult" class="trBorderLeftRight" style="display: none; ">
                                            <td colspan="3" style="text-align:center">
                                                No class matched you filter...
                                            </td>
                                        </tr>
                                    @endif
                                </table>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if($user['userRole'] == "Teacher")
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header" style="background-color: #a10c25; color:white; font-size:20"><b>Teaching</b></div>
                            <div class="card-body" style="text-align: right">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <table style="border:none; width:50%">
                                    <tr class="nostyle" style="background-color: white; border:none;">
                                        <td style="border-top: none">
                                            Class name :
                                        </td>
                                        <td style="border-top: none">
                                            <input class="form-control" style="border-radius: 6px; padding-left:10px" type="text" id="classSearchInput" onkeyup="filterStudent()" placeholder="Filter by class name.." title="Type in a class name">
                                        </td>
                                    </tr>
                                </table>
                                <table style="border: 1px solid black; width:100%" id="classTable">
                                    <tr>
                                        <th style="font-weight: bold; width:10%; text-align:center">
                                            No
                                        </th>
                                        <th style="font-weight: bold">
                                            Name
                                        </th>
                                        <th style="font-weight: bold; text-align: center; width:10%">
                                            Sessions
                                        </th>
                                    </tr>
                                    @if($user->classes->count() == 0)
                                        <tr>
                                            <td colspan="3" style="text-align: center;">
                                                Not teaching in any class yet!
                                            </td>
                                        </tr>
                                    @else
                                        @php
                                            $counter = 1;
                                        @endphp
                                        @foreach($user->classes as $class_)
                                        <tr>
                                            <td class="tdNo" style="text-align:center">
                                            </td>
                                            <td class="tdClassName">
                                                <a style="text-decoration: none; color:black" href="{{url('classDetails/'.$class_['id'])}}">{{ __($class_['className'])}}</a>
                                            </td>
                                            <td style="text-align: center">
                                                @php
                                                    $currentTimeDate = Date('d-m-Y H:i');
                                                    $sessionCounter = 0;
                                                    foreach($class_->session as $session1){
                                                        $sessionTimeDate = Date($session1['sessionDate'] . $session1['sessionTime']);
                                                        $sessionTimeDate = Date('d-m-Y H:i', strtotime($sessionTimeDate));
                                                        if(Date(strtotime($currentTimeDate)) < Date(strtotime($sessionTimeDate))){
                                                            $sessionCounter ++;
                                                        }
                                                    }
                                                @endphp
                                                {{$sessionCounter}}
                                            </td>
                                        @endforeach
                                        <tr id="trNoResult" class="trBorderLeftRight" style="display: none; ">
                                            <td colspan="3" style="text-align:center">
                                                No class matched you filter...
                                            </td>
                                        </tr>
                                    @endif
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <br>
        <div class="modal" id="editModal" style="border: solid">
            <div class="row justify-content-center" id="editModal2" >
                <div class="col-md-8 row justify-content-center" id="editModal3" style="border: none; text-align:center" >
                    <div class="card" id="editCard" style="border: none; width: 60%">
                        <div class="card-header" style="background-color:#a10c25; color:white"><b>{{ __('Edit Profile') }}</b></div>
        
                        <div class="card-body">
                            <form method="POST" action="{{ url('/editUser') }}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="userID" value="{{ __($user['id'])}}">

                                <div class="form-group row">
                                    <label for="userPp" class="col-md-4 col-form-label text-md-right">{{ __('Profile Picture') }}</label>
        
                                    <div class="col-md-6 justify-content-center" >
                                        <input type="file" id="real-input" name="fileToUpload">
                                            <button class="browse-btn" style="display: none">
                                                Browse Files
                                            </button>
                                            <span class="file-info" style="display: none">Upload a file</span>
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                        @php
                                            $ppLink = "images/profilepictures/".$user['id'].".jpg"; // no / at the link begining
                                        @endphp
                                        @if(file_exists($ppLink))
                                            <br><br><img id="newPpImg" src="/public/{{$ppLink}}" height="100" alt="profile picture"> {{-- add / the begining to work. weird.... --}}
                                        @else
                                            <br><br><img id="newPpImg" src="/public/{{$ppLink}}" height="1" alt="profile picture" style="visibility:hidden"> {{-- add / the begining to work. weird.... --}}
                                        @endif
                                        <span id="invalidFileExt" class="invalid-feedback-man" role="alert" style="">
                                            <strong>Please select .JPG or .PNG file for Profile Picture</strong>
                                        </span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="name" type="name" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') == "" ? $user['name'] : old('name') }}" placeholder="{{ old('email') == "" ? $user['name'] : old('name') }}" required autocomplete="name" autofocus>
        
                                        @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Email') }}</label>
                                    <div class="col-md-6">
                                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') == "" ? $user['email'] : old('email') }}" placeholder="{{ old('email') == "" ? $user['email'] : old('email') }}" required autocomplete="email" autofocus>
                                        @error('email')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="Phone Number" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>
                                    <div class="col-md-6">
                                        <input type="tel" id="userPhone" name="userPhone" class="form-control"  value="{{ old('userPhone') == "" ? $user['userPhone'] : old('userPhone') }}" placeholder="{{ old('userPhone') == "" ? $user['userPhone'] : old('userPhone') }}" pattern="[0-9]{10}" required>
                                        @error('userPhone')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="userGender" class="col-md-4 col-form-label text-md-right">{{ __('Gender') }}</label>
                                    <div class="col-md-6">
                                        <select name="userGender" class="form-control" required>
                                            <option selected hidden value="{{ old('userGender') == "" ? $user['userGender'] : old('userGender') }}">{{ old('userGender') == "" ? $user['userGender'] : old('userGender') }}</option>
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="userRole" class="col-md-4 col-form-label text-md-right">{{ __('Role') }}</label>
                                    <div class="col-md-6">
                                        @if(Auth::user()->userRole == "Admin")
                                            <select name="userRole" class="form-control" required>
                                                <option class="form-control" selected hidden value="{{ old('userRole') == "" ? $user['userRole'] : old('userRole') }}">{{ old('userRole') == "" ? $user['userRole'] : old('userRole') }}</option>
                                                <option class="form-control" value="Admin">Admin</option>
                                                <option class="form-control" value="Teacher">Teacher</option>
                                                <option class="form-control" value="Student">Student</option>
                                            </select>
                                        @else
                                            <input type="hidden" name="userRole" value="{{ __($user['userRole'])}}">
                                            <select name="" class="form-control" disabled required>
                                                <option class="form-control" selected hidden value="{{ old('userRole') == "" ? $user['userRole'] : old('userRole') }}">{{ old('userRole') == "" ? $user['userRole'] : old('userRole') }}</option>
                                            </select>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row mb-0 justify-content-center" style="text-align: center;">
                                    <button type="submit" class="button1" style="width: 100">
                                        {{ __('Submit') }}
                                    </button>&nbsp &nbsp
                                    <button type="reset" class="buttonReject" style="width: 100" onclick="eventCloseEditFunction()">
                                        {{ __('Cancel') }}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" id="changePasswordModal" style="border: solid">
            <div class="row justify-content-center" id="changePasswordModal2" >
                <div class="col-md-8 row justify-content-center" id="changePasswordModal3" style="border: none; text-align:center" >
                    <div class="card" id="editCard" style="border: none; width: 60%">
                        <div class="card-header" style="background-color:#a10c25; color:white"><b>{{ __('Change Password') }}</b></div>
                        <div class="card-body">
                            <form method="POST" action="{{ url('/changePassword') }}">
                                @csrf
                                <input type="hidden" name="userID" value="{{ __($user['id'])}}">
                                <div class="form-group row">
                                    <label for="currentPassword" class="col-md-4 col-form-label text-md-right">{{ __('Current Password') }}</label>
                                    <div class="col-md-6">
                                        <input id="currentPassword" type="password" class="form-control @error('currentPassword') is-invalid @enderror" name="currentPassword" required autofocus>
                                        @error('currentPassword')
                                            <span class="invalid-feedback-password" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="newPassword" class="col-md-4 col-form-label text-md-right">{{ __('New Password') }}</label>
                                    <div class="col-md-6">
                                        <input id="newPassword" type="password" class="form-control @error('newPassword') is-invalid @enderror" name="newPassword" required autofocus>
                                        @error('newPassword')
                                            <span class="invalid-feedback-password" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="confirm_newPassword" class="col-md-4 col-form-label text-md-right">{{ __('Confirm New Password') }}</label>
                                    <div class="col-md-6">
                                        <input id="confirm_newPassword" type="password" class="form-control @error('confirm_newPassword') is-invalid @enderror" name="confirm_newPassword" required autofocus>
                                        @error('confirm_newPassword')
                                            <span class="invalid-feedback-password" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-0 justify-content-center" style="text-align: center;">
                                    <button type="submit" class="button1" style="width: 100">
                                        {{ __('Submit') }}
                                    </button>&nbsp &nbsp
                                    <button type="reset" class="buttonReject" style="width: 100" onclick="eventCloseEditFunction()">
                                        {{ __('Cancel') }}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if ($user['userRole'] != "Teacher")
            <div class="modal" id="topupModal" style="border: solid">
                <div class="row justify-content-center" id="topupModal2" >
                    <div class="col-md-8 row justify-content-center" id="topupModal3" style="border: none; text-align:center" >
                        <div class="card" id="editCard" style="border: none; width: 40%">
                            <div class="card-header" style="background-color:#a10c25; color:white"><b>{{ __('Topup Credit') }}</b></div>
                            <div class="card-body">
                                <form method="POST" action="{{ url('/topupCreditReceipt') }}" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="userID" value="{{ __($user['id'])}}">
                                    <div class="form-group row">
                                        <label for="Name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" value="{{$user['name']}}" disabled autofocus>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="Phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" value="{{$user['userPhone']}}" disabled autofocus>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="Current_Credit" class="col-md-4 col-form-label text-md-right">{{ __('Current Credit') }}</label>
                                        <div class="col-md-6">
                                            <input id="inputCreditBalance" type="text" class="form-control" value="0" disabled autofocus>
                                        </div>
                                    </div>
                                    
                                    @if (Auth::User()->userRole == "Student")
                                        <label for="userPp" class="col-md-4 col-form-label text-md-right">{{ __('Payment Proof') }}</label>
                
                                        <div class="col-md-6 justify-content-center" >
                                            <input type="file" id="real-input2" name="fileToUpload2">
                                                <button class="browse-btn2" style="display: none">
                                                    Browse Files
                                                </button>
                                                <span class="file-info2" style="display: none">Upload a file</span>
                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            @php
                                                // $ppLink = "images/profilepictures/".$user['id'].".jpg"; // no / at the link begining
                                            @endphp
                                            {{-- @if(file_exists($ppLink)) --}}
                                                {{--<br><br><img id="newPpImg" src="/public/{{$ppLink}}" height="100" alt="profile picture">--}} {{-- add / the begining to work. weird.... --}}
                                            {{-- @else --}}
                                                {{-- <br><br><img id="newPpImg" src="/public/{{$ppLink}}" height="1" alt="profile picture" style="visibility:hidden"> add / the begining to work. weird.... --}}
                                            {{-- @endif --}}
                                            <span id="invalidFileExt2" class="invalid-feedback-man" role="alert" style="">
                                                <strong>Please select .JPG, .PNG or .PDF file for Proof of payment</strong>
                                            </span>
                                        </div>
                                    @else
                                        <div class="form-group row">
                                            <label for="topupAmount" class="col-md-4 col-form-label text-md-right">{{ __('Topup Amount') }}</label>
                                            <div class="col-md-6">
                                                <input id="topupAmount" type="number" class="form-control @error('topupAmount') is-invalid @enderror" name="topupAmount" min=1 required autofocus>
                                                @error('topupAmount')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                    @endif
                                    <div class="form-group row">
                                        
                                    </div>
                                    <div class="form-group row mb-0 justify-content-center" style="text-align: center;">
                                        <button id="topupSubmitButton" type="submit" class="button1" style="width: 100">
                                            {{ __('Submit') }}
                                        </button>&nbsp &nbsp
                                        <button type="reset" class="buttonReject" style="width: 100" onclick="eventCloseEditFunction()">
                                            {{ __('Cancel') }}
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        
        @if(session()->get('changePasswordSuccess') != null)
            <span id="changePasswordSuccess" style="display: none"></span>
        @endif
        @if(session()->get('editProfileSuccess') != null)
            <span id="editProfileSuccess" style="display: none"></span>
        @endif
        @if(session()->get('topupSuccessful') != null)
            <span id="topupSuccessful" style="display: none">{{session()->get('topupSuccessful')}}</span>
        @endif
    @endif

    <script>
        window.onload=function(){
            filterStudent();
            var timestamp = new Date().getTime();     
     
            var el = document.getElementById("ppLink");    
            var userID = document.getElementById("userID").innerText;
            el.src = "/public/images/profilepictures/" + userID + ".jpg?t=" + timestamp; //force hard refresh for the link
            
            if(document.getElementsByClassName('invalid-feedback')[0]!= null){
                var editModal = document.getElementById("editModal");
                editModalShow();                    
            }
            if(document.getElementsByClassName('invalid-feedback-password')[0]!= null){
                var editModal = document.getElementById("changePasswordModal");
                changePasswordModalShow();                    
            }
            if(document.getElementById('changePasswordSuccess') != null){
                setTimeout(function changePasswordSuccessAlert(){
                    alert("Password updated!");
                },50);
            }
            if(document.getElementById('editProfileSuccess') != null){
                setTimeout(function editProfileSuccess(){
                    alert("Profile updated!");
                },50);
            }
            if(document.getElementById('topupSuccessful') != null){
                setTimeout(function topupSuccessful(){
                    alert(document.getElementById('topupSuccessful').innerHTML);
                },50);
            }
            getCreditBalance();
        }
    
        var editModal = document.getElementById("editModal");
        var editModal2 = document.getElementById("editModal2");
        var editModal3 = document.getElementById("editModal3");

        var changePasswordModal = document.getElementById("changePasswordModal");
        var changePasswordModal2 = document.getElementById("changePasswordModal2");
        var changePasswordModal3 = document.getElementById("changePasswordModal3");

        var topupModal = document.getElementById("topupModal");
        var topupModal2 = document.getElementById("topupModal2");
        var topupModal3 = document.getElementById("topupModal3");

        function editModalShow() {
            editModal.style.display = "block";
            editModal.style.overflowY = "";
            const body = document.body;
            body.style.overflowY = 'hidden';
        };

        function changePasswordModalShow() {
            changePasswordModal.style.display = "block";
            changePasswordModal.style.overflowY = "";
            const body = document.body;
            body.style.overflowY = 'hidden';
        };

        function topupModalShow() {
            topupModal.style.display = "block";
            topupModal.style.overflowY = "";
            const body = document.body;
            body.style.overflowY = 'hidden';
        };

        document.addEventListener('keydown', function(event){
            if(event.key === "Escape"){
                eventCloseEditFunction();
            }
        });

        document.addEventListener("click", function(e)
        {
            if ((e.target==editModal || e.target==editModal2 || e.target==editModal3 || 
                 e.target==changePasswordModal || e.target==changePasswordModal2 || e.target==changePasswordModal3 ||
                 e.target==topupModal || e.target==topupModal2 || e.target==topupModal3)) 
            {
                eventCloseEditFunction();
            }
        });

        function eventCloseEditFunction() {
            const body = document.body;
            body.style.overflowY = '';
            editModal.style.display = "none";
            changePasswordModal.style.display = "none";
            topupModal.style.display = "none";

        };
    
        const uploadButton = document.querySelector('.browse-btn');
        const fileInfo = document.querySelector('.file-info');
        const realInput = document.getElementById('real-input');
        var newPpImg = document.getElementById('newPpImg');
        var oldInput = "";
    
        uploadButton.addEventListener('click', (e) => {
            realInput.click();
            }
        );
    
        realInput.addEventListener('change', () => {
            var fileExt = realInput.value;
            var fileExt = fileExt.substring(fileExt.lastIndexOf('.') + 1).toLowerCase();
            if(fileExt == "png" || fileExt == "jpg" || fileExt == "jpeg" || fileExt == ""){
                document.getElementById('invalidFileExt').style.display = '';
                const name = realInput.value.split(/\\|\//).pop();
                const truncated = name.length > 20 ? name.substr(name.length - 20) : name;
                fileInfo.innerHTML = truncated;
                if(fileExt != ""){
                    newPpImg.src = URL.createObjectURL(event.target.files[0]); // change img src preview in the form
                    newPpImg.style.visibility = "visible"; // change img visibility
                    newPpImg.style.height = "100"; // change img height
                }else{
                    newPpImg.src = "<?php echo '/'.$ppLink; ?>"; // change img src preview in the form
                    newPpImg.style.visibility = "visible"; // change img visibility
                    newPpImg.style.height = "100";
                }
            }else if(fileExt != ""){
                realInput.value="";
                document.getElementById('invalidFileExt').style.display = 'Block';
            }
        });

        const uploadButton2 = document.querySelector('.browse-btn2');
        const fileInfo2 = document.querySelector('.file-info2');
        const realInput2 = document.getElementById('real-input2');
        var oldInput = "";
    
        uploadButton2.addEventListener('click', (e) => {
            realInput2.click();
            }
        );
    
        realInput2.addEventListener('change', () => {
            var fileExt = realInput2.value;
            alert(fileExt);
            var fileExt = fileExt.substring(fileExt.lastIndexOf('.') + 1).toLowerCase();
            if(fileExt == "png" || fileExt == "jpg" || fileExt == "jpeg" || fileExt == "pdf"){
                document.getElementById('invalidFileExt2').style.display = '';
                const name = realInput.value.split(/\\|\//).pop();
                const truncated = name.length > 20 ? name.substr(name.length - 20) : name;
                fileInfo2.innerHTML = truncated;
                document.getElementById('topupSubmitButton').enabled = 'true';
            }else if(fileExt != "" || fileExt == ""){
                realInput.value="";
                document.getElementById('invalidFileExt2').style.display = 'Block';
                document.getElementById('topupSubmitButton').disabled = 'true';
            }
        });

        function filterStudent() {
            var classSearchInput, classTable, trStudentList, tdClassName, i, txtTdClassName, trCounter = 0;
            classSearchInput = document.getElementById("classSearchInput");
            classSearchInput = classSearchInput.value.toUpperCase();
            classTable = document.getElementById("classTable");
            trClassList = classTable.getElementsByTagName("tr");
            for (i = 0; i < trClassList.length; i++) {
                tdClassName = trClassList[i].getElementsByClassName("tdClassName")[0];
                tdNo = trClassList[i].getElementsByClassName("tdNo")[0];
                if (tdClassName) {
                    txtTdClassName = tdClassName.textContent || tdClassName.innerText;
                    if (txtTdClassName.toUpperCase().indexOf(classSearchInput) > -1) {
                        trClassList[i].style.display = "";
                        tdNo.innerHTML= ++trCounter;
                    } else {
                        trClassList[i].style.display = "none";
                    }
                }                 
            }

            var trNoResult = document.getElementById('trNoResult');
            if(trNoResult != null){
                if(trCounter == 0){
                    trNoResult.style.display = "";
                }else{
                    trNoResult.style.display = "none";
                }
            }
        }

        function getCreditBalance(){
            var userID = document.getElementById("userID").innerText;
            var creditBalance = document.getElementById("creditBalance");
            var inputCreditBalance = document.getElementById("inputCreditBalance");
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'post',
                url: '{{url("getCreditBalance")}}',
                dataType: 'JSON',
                async:false, 
                data: {
                    userID : userID,
                },
                success: function (data) {
                    if(data.creditBalance != null){
                        creditBalance.innerHTML = data.creditBalance;
                        if(inputCreditBalance != null){
                            inputCreditBalance.value = data.creditBalance;
                        }
                    }else{
                        creditBalance.innerHTML = 0;
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
            });
        }
    </script>
    @endsection
@endguest



