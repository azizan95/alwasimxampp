<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }
    
    th, td {
        padding: 8px;
        text-align: left;
        border-top: 1px solid black;
    }

    tr:nth-child(odd) {
        background-color: #f2f2f2;
    }

    th {
        text-align: center;
        font-weight: normal;
        font-size: 15;
        background-color: #242d5f;
        color: white;
    }

    .buttonCleared:disabled {
        text-align: center;
        background-color: #4CAF50; 
        color: white; 
        border: 2px solid #409143;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .button1:enabled {
        background-color: white; 
        color: black; 
        border: 2px solid #4CAF50;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .button1:hover {
        background-color: #4CAF50;
        color: white;
        }
    
    .button1:disabled{
        background-color: #ffffff;
        color: grey; 
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonNoInfo:disabled {
        background-color: #a10c25; 
        color: white; 
        border: 2px solid #8a0b20;
        border-radius: 4px;
        /* width: 75px; */
        margin-top: 5px;
        margin-right: 0px;
        text-align: center;
    }

    .buttonReject:enabled {
        background-color: white; 
        color: black; 
        border: 2px solid #a10c25;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonReject:hover {
        background-color: #a10c25;
        color: white;
        }
    
    .buttonReject:disabled{
        background-color: #ffffff;
        color: grey; 
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .nostyle{
        -webkit-appearance: none; 
        border:1px solid;
       }

    .modal {
        display: block; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    .modal-content {
        /* background-color: #fefefe; */
        background-color: #3D3D3D;
        text-align: center;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        /* width: 10%; */
    } 

    .tdNoStyle {
    /* margin: 0; */
        padding: 0;
        border-top: none;
        border-left: none;
        border-right: none;
        outline: 0;
        font-size: 100%;
        vertical-align: baseline;
        background-color: #a10c25;
    }
    .receiptIcon:hover{
        cursor: pointer;
    }
</style>

@guest
please login

@else
    @extends('layouts.app')
    @section('content')
    
    @if(Auth::user()->userStatus == 0)
        {{ Session::flush() }}
        <script>
            window.alert("Your account is yet to be approved. Please wait for approval email from Admin");
            window.location = "/";
        </script>
    @else
        @if((Auth::user()->id != $user['id'] && Auth::user()->userRole != "Admin") || $user['userRole'] == "Teacher")
            @if($user['userRole'] == "Teacher")
                <script>
                    window.location ='{{ url("userProfile/".$user->id)}}';
                </script>
            @endif
            @if(Auth::user()->userRole == "Teacher")
                <script>
                    window.location ='{{ url("userProfile/".Auth::user()->id)}}';
                </script>
            @endif
            @if(Route::current()->getName() == 'creditDetails')
                <script>
                    window.location ='{{ url("creditDetails/".Auth::user()->id)}}';
                </script>
            @else
                <script>
                    window.location ='{{ url("userProfile/".Auth::user()->id)."/creditDetails" }}';
                </script>
            @endif
        @endif
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="background-color: #a10c25; color:white; font-size:20; padding-bottom:0; height:50px; vertical-align:middle">
                            <table style="border:none; margin-bottom:0; padding:0;">
                                <tr>
                                    <th class="tdNoStyle" style="font-size:20; vertical-align:middle">
                                        <b>Student</b>
                                    </th>
                                </tr>
                            </table>
                        </div>
                        <div class="card-body" style="text-align: right">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <table style="border: 1px solid black; width:100%;">
                                <tr style="font-weight: bold">
                                    <th style="width: 5%; font-weight: bold; border:1px solid none">
                                        ID
                                    </th>
                                    <th colspan="" style="font-weight: bold; border:1px solid none">
                                        Name
                                    </th>
                                    <th colspan="" style="font-weight: bold; border:1px solid none">
                                        Email
                                    </th>
                                    <th style="font-weight: bold; border:1px solid none"> 
                                        Phone Number
                                    </th>
                                </tr>
                                <tr>
                                    <td style="font-weight: ; border:1px solid none">
                                        <span id='userID'>{{ __($user['id'])}}</span>
                                    </td>
                                    <td colspan="" style="width:30%; font-weight: ; border:1px solid none">
                                        {{ __($user['name'])}}
                                    </td>
                                    <td colspan="" style="font-weight: ; border:1px solid none">
                                        {{ __($user['email'])}}
                                    </td>
                                    <td style="border:1px solid none">
                                        {{ __($user['userPhone'])}}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div id="containerUserList2" class="container " style="border: black none 1px;">
            <div class="row justify-content-center" style="border:none red; width:; ">
                <div class="col-md-8" style="border:none cyan;;">
                    <div class="card" style="border:none yellow;;">
                        <div class="card-header" name="userList" style="background-color: #a10c25; color:white; font-size:20"><b>Pending Receipt</b></div>
                        <div class="card-body" style="text-align: right; overflow-x:auto; border:none purple; ">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <table style="border: none; width:100%" id="myTable2">
                                <tr class="" style="border:black solid 1px">
                                    <th>
                                        No.
                                    </th>
                                    <th>
                                        Date
                                    </th>
                                    <th style="text-align: center">
                                        Receipt
                                    </th>
                                    <th>
                                        Status
                                    </th>
                                </tr>
                                @if($receipts->count() == 0)
                                    <tr class="" style="border:black solid 1px">
                                        <td colspan="6" style="text-align: center;">
                                            No receipt need to be approved!
                                        </td>
                                    </tr>
                                @else
                                    @php
                                        $counter = 1;   
                                    @endphp
                                    @foreach($receipts as $receipt)
                                        <tr class="trBorderLeftRight" style="border:black solid 1px" >
                                            <td class="tdNo2">
                                                {{$counter++}}
                                            </td>
                                            <td class="tdName2">
                                                {{ date('d/m/Y', strtotime($receipt['created_at']))}}
                                            </td>
                                            <td class="tdEmail2" style="text-align: center">
                                                @php
                                                    $ppLink = "images/receipts/".$receipt['id'].".jpg"; // no / at the link begining
                                                @endphp
                                                @if(file_exists($ppLink))
                                                    <img class="receiptIcon" onclick="topupModalShow2('{{$receipt['id']}}.jpg', '{{$receipt['receiptStatus']}}')" src="/images/icons/jpgicon.png" height="25" alt="profile picture" > {{-- add / the begining to work. weird.... --}}
                                                @else
                                                    <img class="receiptIcon" onclick="topupModalShow2('{{$receipt['id']}}.pdf', '{{$receipt['receiptStatus']}}')" src="/images/icons/pdficon.png" height="25" alt="profile picture" style=""> {{-- add / the begining to work. weird.... --}}
                                                @endif
                                            </td>
                                            <td class="tdPhoneNo2" style="text-align:;" >
                                                {{ __($receipt->receiptStatus)}}
                                            </td>
                                        </tr>
                                    @endforeach
                                    <tr id="trNoResult2" class="trBorderLeftRight" style="display: none; ">
                                        <td colspan="6" style="text-align:center">
                                            No receipt matched you filter...
                                        </td>
                                    </tr>  
                                @endif
                            </table>
                            <div id="pagingDiv2" style="border:none;">
                                <span id="pagingSpan2" style="font-size:"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="background-color: #a10c25; color:white; font-size:20; padding-bottom:0; height:50px; vertical-align:middle">
                            <table style="border:none; margin-bottom:0; padding:0;">
                                <tr>
                                    <th class="tdNoStyle" style="font-size:20; vertical-align:middle">
                                        <b>Credit Details</b>
                                    </th>
                                    <th class="" style="text-align: right; padding:0; vertical-align:middle; background-color:#a10c25; border:none">
                                        @if ((Auth::User()->userRole == "Admin" || Auth::User()->id == $user['id']) && $user->userRole != "Teacher")
                                            <button form="" style="background-color:transparent; box-shadow: 0px 0px 0px transparent; border: none; text-shadow: 0px 0px 0px transparent; height:30; padding:0; margin-right: 2" onclick="topupModalShow()" >
                                                <img class="deletebutton2" src="/images/icons/topuporange.png" style="width: 25" title="Topup Credit" />
                                            </button>
                                        @endif
                                    </th>
                                </tr>
                            </table>
                        </div>
                        <div class="card-body" style="text-align: right">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <table id="tableCreditDetails" style="border: 1px solid black; width:100%;">
                                <tr style="font-weight: bold">
                                    <th style="width: 5%; font-weight: bold; border:1px solid none; width:5%">
                                        No
                                    </th>
                                    <th colspan="" style="font-weight: bold; border:1px solid none; width:20%">
                                        Date
                                    </th>
                                    <th colspan="" style="font-weight: bold; border:1px solid none">
                                        Transaction
                                    </th>
                                    <th colspan="" style="text-align:center; font-weight: bold; border:1px solid none">
                                        Receipt
                                    </th>
                                    <th style="font-weight: bold; border:1px solid none; width:10%; text-align:right"> 
                                        Credit
                                    </th>
                                    <th style="font-weight: bold; border:1px solid none; width:10%; text-align:right"> 
                                        Debit
                                    </th>
                                    <th style="font-weight: bold; border:1px solid none; width:10%; text-align:right"> 
                                        Balance
                                    </th>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" id="topupModal" style="border: solid">
            <div class="row justify-content-center" id="topupModal2" >
                <div class="col-md-8 row justify-content-center" id="topupModal3" style="border: none; text-align:center" >
                    <div class="card" id="editCard" style="border: none; width: 40%">
                        <div class="card-header" style="background-color:#a10c25; color:white"><b>{{ __('Topup Credit') }}</b></div>
                        <div class="card-body">
                            <form method="POST" action="{{ url('/topupCreditReceipt') }}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="userID" value="{{ __($user['id'])}}">
                                <div class="form-group row">
                                    <label for="Name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" value="{{$user['name']}}" disabled autofocus>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="Phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" value="{{$user['userPhone']}}" disabled autofocus>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="Current_Credit" class="col-md-4 col-form-label text-md-right">{{ __('Current Credit') }}</label>
                                    <div class="col-md-6">
                                        <input id="inputCreditBalance" type="text" class="form-control" value="0" disabled autofocus>
                                    </div>
                                </div>
                                
                                @if (Auth::User()->userRole == "Student")
                                    <div class="form-group row">
                                        <label for="userPp" class="col-md-4 col-form-label text-md-right">{{ __('Payment Proof') }}</label>
            
                                        <div class="col-md-6 justify-content-center" >
                                            <input type="file" id="real-input2" name="fileToUpload2" required>
                                                <button class="browse-btn2" style="display: none">
                                                    Browse Files
                                                </button>
                                                <span class="file-info2" style="display: none">Upload a file</span>
                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            @php
                                                // $ppLink = "images/profilepictures/".$user['id'].".jpg"; // no / at the link begining
                                            @endphp
                                            {{-- @if(file_exists($ppLink)) --}}
                                                {{--<br><br><img id="newPpImg" src="/public/{{$ppLink}}" height="100" alt="profile picture">--}} {{-- add / the begining to work. weird.... --}}
                                            {{-- @else --}}
                                                {{-- <br><br><img id="newPpImg" src="/public/{{$ppLink}}" height="1" alt="profile picture" style="visibility:hidden"> add / the begining to work. weird.... --}}
                                            {{-- @endif --}}
                                            <span id="invalidFileExt2" class="invalid-feedback-man" role="alert" style="">
                                                <strong>Please select .JPG, .PNG or .PDF file for Proof of payment</strong>
                                            </span>
                                        </div>
                                    </div>
                                @else
                                    <div class="form-group row">
                                    <label for="topupAmount" class="col-md-4 col-form-label text-md-right">{{ __('Topup Amount') }}</label>
                                    <div class="col-md-6">
                                        <input id="topupAmount" type="number" class="form-control @error('topupAmount') is-invalid @enderror" name="topupAmount" min=1 required autofocus>
                                        @error('topupAmount')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                @endif
                                
                                <div class="form-group row mb-0 justify-content-center" style="text-align: center;">
                                    <button id="topupSubmitButton" type="submit" class="button1" style="width: 100">
                                        {{ __('Submit') }}
                                    </button>&nbsp &nbsp
                                    <button type="reset" class="buttonReject" style="width: 100" onclick="eventCloseEditFunction()">
                                        {{ __('Cancel') }}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal " id="topupModala" style="border: solid; text-align: center">
            <div class="row justify-content-center" id="topupModala2" style="text-align: center" >
                <div class="col-md-8 row justify-content-center " id="topupModala3" style="border: none; text-align:center" >
                    <div class="card" id="editCard" style="border: none; width: 100%; overflow-y:auto; text-align: center">
                        <div class="card-header" style="background-color:#a10c25; color:white">
                            <b>{{ __('Receipt') }}</b>
                        </div>
                        <div class="card-body" style="text-align: center">
                            <form method="POST" action="{{ url('/topupCredit') }}" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" id="userID" name="userID" value="{{ __($user['id'])}}">
                                <input type="hidden" id="receiptID" name="receiptID">
                                <input type="hidden" id="userEmail" name="userEmail">
                                <div class="form-group row">
                                    <label for="Name" class="col-md-4 col-form-label text-md-right ">{{ __('') }}</label>
                                    <div class="col-md-6" style="overflow-x:auto; overflow-y:hidden">
                                        <span id="proofJPG"></span>
                                    </div>
                                </div>
                                @if (Auth::User()->userRole == "Admin")
                                    <div id="topupForm">
                                        <div class="form-group row">
                                            <label for="topupAmount" class="col-md-4 col-form-label text-md-right">{{ __('Topup Amount') }}</label>
                                            <div class="col-md-6">
                                                <input id="topupAmount" type="number" class="form-control @error('topupAmount') is-invalid @enderror" name="topupAmount" min=1 required autofocus>
                                                @error('topupAmount')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row mb-0 justify-content-center" style="text-align: center;">
                                            <button id="topupSubmitButton" type="submit" class="button1" style="width: 100">
                                                {{ __('Submit') }}
                                            </button>&nbsp &nbsp
                                            <button type="reset" class="buttonReject" style="width: 100" onclick="eventCloseEditFunction()">
                                                {{ __('Cancel') }}
                                            </button>
                                        </div>
                                    </div>
                                @endif
                            </form>
                            {{-- <div class="form-group row" style="text-align: center" >
                                <div class="col-md-6"  style="text-align: center">
                                    <span id="proofJPG" style="text-align: center" ></span>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        @if(session()->get('topupSuccessful') != null)
            <span id="topupSuccessful" style="display: none">{{session()->get('topupSuccessful')}}</span>
        @endif
    @endif
    <script>
        window.onload = function(){ //run these after page loaded
            getCreditDetails();

            if(document.getElementById('topupSuccessful') != null){
                setTimeout(function topupSuccessful(){
                    alert(document.getElementById('topupSuccessful').innerHTML);
                },50);
            }         
        }

        function topupModalShow() {
            topupModal.style.display = "block";
            topupModal.style.overflowY = "";
            const body = document.body;
            body.style.overflowY = 'hidden';
        };

        document.addEventListener('keydown', function(event){
            if(event.key === "Escape"){
                eventCloseEditFunction();
            }
        });

        document.addEventListener("click", function(e)
        {
            if ((e.target==topupModal || e.target==topupModal2 || e.target==topupModal3 ||
                e.target==topupModala || e.target==topupModala2 || e.target==topupModala3)) 
            {
                eventCloseEditFunction();
            }
        });

        function eventCloseEditFunction() {
            const body = document.body;
            body.style.overflowY = '';
            topupModal.style.display = "none";
            topupModala.style.display = "none";
        };

        function getCreditDetails(){
            var userID = document.getElementById("userID").innerText;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'post',
                url: '{{url("/getCreditDetails")}}',
                dataType: 'JSON',
                async:false, 
                data: {
                    userID : userID,
                },
                success: function (data) {
                    if(data.credits.length == 0){
                        $('#tableCreditDetails .trCreditDetails').remove();
                        $('#tableCreditDetails')
                        .append($('<tr>', {class : "trCreditDetails"})
                            .append($('<td>', { colspan:7, 
                                                style : "color:black; text-align:center", 
                                                text : 'No credit history yet!'} ))
                        );
                    }else{
                        $('#tableCreditDetails .trCreditDetails').remove();
                        for (var d in data.credits) {
                            var credit = data.credits[d];
                            var fullDate = new Date(credit.creditDate);
                            var year = fullDate.getFullYear();
                            var month = fullDate.getMonth()+1;
                            var date = fullDate.getDate();
                            var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                            var day = days[fullDate.getDay()];
                            $('#tableCreditDetails')
                                .append($('<tr>', { class   : "trCreditDetails", 
                                                    onclick : ""})
                                    .append($('<td>', { class   : "tdNo", 
                                                        style   : "color : black; text-align: center;", 
                                                        text    : ++d}))
                                    .append($('<td>', { text    : date +"/"+ month +"/"+ year}))
                                    .append($('<td>', { style   : "text-align:;", 
                                                        text    : credit.creditTransaction}))
                                    .append($('<td>', { style   : "text-align:center;", 
                                                    //  text    : checkReceiptExtention(credit.receipt_id),
                                                        text    : ""})
                                        .append($('<img>', {
                                                            src     : checkReceiptExtention(credit.receipt_id),
                                                            height  : "25",
                                                            onclick : receiptOnclick(credit.receipt_id, credit.receiptStatus),
                                                            class   : "receiptIcon"}))
                                    )
                                    .append($('<td>', { style   : "text-align:right;", 
                                                        text    : credit.creditCredit}))
                                    .append($('<td>', { style   : "text-align:right;", 
                                                        text    : credit.creditDebit}))
                                    .append($('<td>', { style   : "text-align:right;", 
                                                        text    : credit.creditBalance}))
                                );
                        }
                        document.getElementById("inputCreditBalance").value = data.credits[0].creditBalance;
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
            });
        }
        // credit.creditTransaction
        var globalVars = {unloaded:false};
        $(window).bind('beforeunload', function(){
            globalVars.unloaded = true;
        });

        var valuee = "";
        function checkReceiptExtention(receiptID){
            valuee = null;
            if (receiptID != null) {
                valuee = receiptID;
                // var urls = "/public/images/receipts/"+receiptID;
                $.ajax({
                    url: "/public/images/receipts/"+receiptID+".jpg",
                    type: 'HEAD',
                    async:false, 
                    error: function (XMLHttpRequest, textStatus, errorThrown){
                        // alert("pdf");
                        valuee = "1";
                    },
                    success: function(){
                        // alert("jpg");
                        valuee = "/images/icons/jpgicon.png";
                    },
                    statusCode: {
                        404: function() {
                            valuee = "/images/icons/pdficon.png";
                        }
                    }
                });
            } 
            return valuee;
        };

        var valueee = "";
        function receiptOnclick(receiptID, receiptStatus){
            valueee = null;
            if (receiptID != null) {
                valueee = receiptID;
                // var urls = "/public/images/receipts/"+receiptID;
                $.ajax({
                    url: "/public/images/receipts/"+receiptID+".jpg",
                    type: 'HEAD',
                    async:false, 
                    error: function (XMLHttpRequest, textStatus, errorThrown){
                        // alert("pdf");
                        valueee = "1";
                    },
                    success: function(){
                        // alert("jpg");
                        valueee = "topupModalShow2('"+receiptID+".jpg', '"+receiptStatus+"')";
                    },
                    statusCode: {
                        404: function() {
                            valueee = "topupModalShow2('"+receiptID+".pdf', '"+receiptStatus+"')";
                        }
                    }
                });
            } 
            return valueee;
        };
        // valuee = "<img onclick='topupModalShow("+receiptID+".jpg' src='/images/icons/jpgicon.png' height='25' alt='profile picture'>";

        // error: function (XMLHttpRequest, textStatus, errorThrown){
        //                 // alert("pdf");
        //                 valuee = "<img onclick='topupModalShow("+receiptID+".pdf' src='/images/icons/pdficon.png' height='25' alt='profile picture'";
        //             },
        // success: function(){
        //     // alert("jpg");
        //     valuee = "<img onclick='topupModalShow("+receiptID+".jpg' src='/images/icons/jpgicon.png' height='25' alt='profile picture'";

        if(document.querySelector('.browse-btn2') != null){
            const uploadButton2 = document.querySelector('.browse-btn2');
            const fileInfo2 = document.querySelector('.file-info2');
            const realInput2 = document.getElementById('real-input2');
            var oldInput = "";
            uploadButton2.addEventListener('click', (e) => {
                realInput2.click();
                }
            );
            realInput2.addEventListener('change', () => {
                var fileExt = realInput2.value;
                alert(fileExt);
                var fileExt = fileExt.substring(fileExt.lastIndexOf('.') + 1).toLowerCase();
                if(fileExt == "png" || fileExt == "jpg" || fileExt == "jpeg" || fileExt == "pdf"){
                    document.getElementById('invalidFileExt2').style.display = '';
                    const name = realInput.value.split(/\\|\//).pop();
                    const truncated = name.length > 20 ? name.substr(name.length - 20) : name;
                    fileInfo2.innerHTML = truncated;
                    document.getElementById('topupSubmitButton').enabled = 'true';
                }else if(fileExt != "" || fileExt == ""){
                    realInput.value="";
                    document.getElementById('invalidFileExt2').style.display = 'Block';
                    document.getElementById('topupSubmitButton').disabled = 'true';
                }
            });
        }
    
        
    
        

        var topupModala = document.getElementById("topupModala");
        var topupModala2 = document.getElementById("topupModala2");
        var topupModala3 = document.getElementById("topupModala3");

        function topupModalShow2(receiptID, receiptStatus) {
            var fileExt = receiptID.substring(receiptID.lastIndexOf('.') + 1).toLowerCase();
            if(fileExt == "png" || fileExt == "jpg" || fileExt == "jpeg"){
                document.getElementById("proofJPG").innerHTML = "<img src='/public/images/receipts/"+receiptID+"' style='height:250;'>";
            }else{
                document.getElementById("proofJPG").innerHTML = "<iframe src='/public/images/receipts/"+receiptID+"' style='width:100%; height:250; overflow-x:auto' frameborder='0'></iframe>";
            }
            if(document.getElementById('topupForm') != null){
                if(receiptStatus == "Approved"){
                    document.getElementById('topupForm').style.display = 'none';
                }else{
                    document.getElementById('topupForm').style.display = 'block';
                }
            }
            document.getElementById('receiptID').value = receiptID.substring(0, receiptID.lastIndexOf('.')).toLowerCase();
            topupModala.style.display = "block";
            topupModala.style.overflowY = "";
            const body = document.body;
            body.style.overflowY = 'hidden';
        };
    </script>
    @endsection
@endguest



