<style>
    .tdSessionDetails{
        cursor: pointer;
    }

    table {
        border-collapse: collapse;
        width: 100%;
    }

    th, td {
        padding: 8px;
        text-align: left;
        /* border: 1px solid black; */
        border-top: 1px solid black;
    }

    tr:nth-child(odd) {
        background-color: #f2f2f2;
    }

    th {
        text-align: center;
        font-weight: normal;
        font-size: 15;
        background-color: #242d5f;
        color: white;
    }

    .buttonCleared:disabled {
        text-align: center;
        background-color: #4CAF50;
        color: white;
        border: 2px solid #409143;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .button1:enabled {
        background-color: white;
        color: black;
        border: 2px solid #4CAF50;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .button1:hover {
        background-color: #4CAF50;
        color: white;
        }

    .button1:disabled{
        /* background-color: white;
        color: black;
        border: 2px solid #4CAF50;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px; */
        background-color: #ffffff;
        color: grey;
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonNoInfo:disabled {
        background-color: #a10c25;
        color: white;
        border: 2px solid #8a0b20;
        border-radius: 4px;
        /* width: 75px; */
        margin-top: 5px;
        margin-right: 0px;
        text-align: center;
    }

    .buttonReject:enabled {
        background-color: white;
        color: black;
        border: 2px solid #a10c25;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonReject:hover {
        background-color: #a10c25;
        color: white;
        }

    .buttonReject:disabled{
        background-color: #ffffff;
        color: grey;
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .nostyle{
        -webkit-appearance: none;
        border:1px solid;
       }

    .modal {
        display: block; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    .modal-content {
        /* background-color: #fefefe; */
        background-color: #3D3D3D;
        text-align: center;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        /* width: 10%; */
    }

    .tdNoStyle {
    /* margin: 0; */
        padding: 0;
        border-top: none;
        border-left: none;
        border-right: none;
        outline: 0;
        font-size: 100%;
        vertical-align: baseline;
        background-color: #a10c25;
    }
    .tdNoStyle2 {
    /* margin: 0; */
        padding: 0;
        border-top: none;
        border-left: none;
        border-right: none;
        outline: 0;
        font-size: 100%;
        vertical-align: baseline;
        background-color: #ffffff;
    }

    .trBorderLeftRight{
        border-left: 1px solid black;
        border-right: 1px solid black;
        border-bottom: 1px solid black;
    }

    .livesearchA{
        all: unset;
    }
</style>



@guest
please login

@else
    @extends('layouts.app')
    @section('content')

    @if(Auth::user()->userStatus == 0)
        {{ Session::flush() }}
        <script>
            window.alert("Your account is yet to be approved. Please wait for approval email from Admin");
            window.location = "/";
        </script>
    @else
        @php
            $currentTimeDate = Date('d-m-Y H:i');
            $sessionTimeDate = Date($session['sessionDate'] . $session['sessionTime']);
            $sessionTimeDate = Date('d-m-Y H:i', strtotime($sessionTimeDate));
            $sessiondate = new DateTime(Date('d-m-Y H:i', strtotime($sessionTimeDate)));
            $currentdate = new DateTime(Date('d-m-Y H:i', strtotime($currentTimeDate)));
            $timeDiff = $currentdate->diff($sessiondate);
            $minutes = $timeDiff->days * 24 * 60;
            $minutes += $timeDiff->h * 60;
            $minutes += $timeDiff->i;
            $timeDiff = $timeDiff->format('%r'); //get + or - time diff // + means before time session, - means after time session
            $timeDiff = $timeDiff.$minutes; //put + or - in front timediff in minutes

            $boolTimeDiff = $minutes <= 15; //time diff +-15 // timenow +-15 = timesession // 15minit before and after time session
            $boolTimeDiffPos = ($timeDiff >= 15); //time diff +15 // timenow +15 = timesession // 15minit before time session
            $boolSessionTimeDate = (new DateTime(Date('d-m-Y H:i', strtotime($currentTimeDate))) >= new DateTime(Date('d-m-Y H:i', strtotime($sessionTimeDate))));
            $boolIsAdmin = (Auth::user()->userRole == "Admin");
            $boolIsClassTeacher = (Auth::user()->id == $session->class_->user['id']);
            $booldIsStudent = (Auth::user()->userRole == "Student");
            $boolIsStudentBooked = Auth::user()->booked()->where('session_id', $session['id'])->count() > 0;
            $boolIsStudentJoinedClass = Auth::user()->enrolledIn()->where('class__id', $session->class_['id'])->count() > 0;
            $boolIsSessionNotFull = ($session->bookedBy()->count() < $session['sessionCapacity']);
            echo( $boolSessionTimeDate);

        @endphp
        {{-- @if(Date(strtotime($currentTimeDate)) >= Date(strtotime($sessionTimeDate)))
            <script>
                window.location ='{{ url("classDetails/".$session->class_->id) }}';
            </script>
        @endif --}}
        {{-- @if(Auth::user()->id != $session->class_['id'])
            <script>
                window.location ='{{ url("userProfile/".Auth::user()->id) }}';
            </script>
        @endif --}}
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="background-color: #a10c25; color:white; font-size:20; padding-bottom:0; height:50px; vertical-align:middle">
                            <table style="border:none; margin-bottom:0; padding:0;">
                                <tr>
                                    <th class="tdNoStyle" style="font-size:20; vertical-align:middle;">
                                        <b>Session</b>
                                    </th>
                                    @if(($boolIsAdmin || $boolIsClassTeacher) && $boolTimeDiffPos && $session['sessionStatus'] != "Cancelled")
                                        <th class="tdNoStyle" style="text-align: right; padding:0; vertical-align:middle;">
                                            <button type="button" style="background-color:transparent; box-shadow: 0px 0px 0px transparent; border: none; text-shadow: 0px 0px 0px transparent; height:30; padding:0 " onclick="editModalShow2()" >
                                                <img class="deletebutton1" src="/images/icons/editblue.png" style="width: 25" title="Edit user"/>
                                            </button>
                                            <button form="deleteForm" style="background-color:transparent; box-shadow: 0px 0px 0px transparent; border: none; text-shadow: 0px 0px 0px transparent; height:30; padding:0; margin-right: 2" onclick="return confirm('Confirm delete?')" >
                                                <img class="deletebutton2" src="/images/icons/deletered.png" style="width: 25" title="Delete user" />
                                            </button>
                                            <form name="deleteForm" id="deleteForm" method="POST" action="{{ url('/deleteSession') }}">
                                                @csrf
                                                <input type="hidden" name="sessionID" value="{{$session['id']}}">
                                            </form>
                                        </th>
                                    @endif
                                </tr>
                            </table>
                        </div>
                        <div class="card-body shadow-sm" style="text-align: right">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <table style="border-bottom: 1px solid black; width:100%;">
                                {{-- @if($booldIsStudent && $boolIsStudentJoinedClass && $boolSessionTimeDate && $session['sessionStatus'] != "Cancelled" && $boolTimeDiffPos) --}}
                                @if($booldIsStudent && $boolIsStudentJoinedClass && $session['sessionStatus'] != "Cancelled" && $boolTimeDiffPos)
                                    <tr style="border-top:1px solid none">
                                        <td class="tdNoStyle2" style="border-top:none; padding-top:0" colspan="5">
                                            <form id="BookForm" method="POST" action="{{ url('/bookSession') }}">
                                                @csrf
                                                <input type="hidden" name="sessionID" value="{{$session['id']}}">
                                                <input type="hidden" id="studentID" name="studentID" value="{{Auth::user()->id}}">
                                            </form>
                                            <form id="CancelForm" method="POST" action="{{ url('/cancelBooking') }}">
                                                @csrf
                                                <input type="hidden" name="sessionID" value="{{$session['id']}}">
                                                <input type="hidden" name="studentID" value="{{Auth::user()->id}}">
                                            </form>
                                            @if($boolIsStudentBooked)
                                                <button form="CancelForm" class="buttonReject" style="width: auto; margin-bottom:2; margin-top:0;" onclick="return confirm('Confirm cancel this booking?')">Cancel Booking</button>
                                            @else
                                                @if($boolIsSessionNotFull)
                                                    <button form="BookForm" id="BookFormButton" class="button1" style="width: auto; margin-bottom:2; margin-top:0;" onclick="return confirm('Confirm book this session?')">Book Session</button>
                                                @else
                                                    <button form="BookForm" class="buttonReject" style="width: auto; margin-bottom:2; margin-top:0; display:none" disabled>Book Session</button>
                                                    <span class="invalid-feedback-man" role="alert" style="display:block">
                                                        <strong>Session is fully booked!</strong>
                                                    </span>
                                                @endif
                                                <span class="invalid-feedback-man" id="invalidStudentBookSameDateTime" role="alert" style="display:none">
                                                    <strong>You have booked other session with the same date and time!</strong>
                                                </span>
                                            @endif
                                            <span class="invalid-feedback-man" id="invalidCreditAmount" role="alert" style="display:none">
                                                <strong>You dont have enough credit to book this session!</strong>
                                            </span>
                                        </td>
                                    </tr>
                                @endif
                                <tr style="font-weight: bold" class="trBorderLeftRight">
                                    <th style="width: 5%; font-weight: bold">
                                        ID
                                    </th>
                                    <th colspan="2" style="width: 45%;font-weight: bold">
                                        Class
                                    </th>
                                    <th colspan="2" style="width: 100%;font-weight: bold">
                                        Teacher
                                    </th>
                                    <th colspan="" style="width: 100%;font-weight: bold; text-align:center">
                                        @if ($session->class_->classType == 'Normal')
                                            Cost
                                        @endif
                                    </th>
                                </tr>
                                <tr class="trBorderLeftRight">
                                    <td>
                                        {{ __($session['id'])}}
                                    </td>
                                    <td colspan="2">
                                        <a style="text-decoration: none; color:black" href="{{url('classDetails/'.$session->class_['id'])}}">{{ __($session->class_['className'])}}</a>
                                    </td>
                                    <td colspan="2">
                                        <a style="text-decoration: none; color:black" href="{{url('userProfile/'.$session->class_->user->id)}}">{{ __($session->class_->user->name)}}</a>
                                    </td>
                                    <td style="text-align:center">
                                        @if ($session->class_->classType == 'Normal')
                                            <a style="text-decoration: none; color:black; ">{{ __($session->sessionCost)}} Credit</a>
                                        @endif
                                        <span id="sessionCost" style="display: none">{{ __($session->sessionCost)}}</span>
                                    </td>
                                </tr>
                                <tr class="trBorderLeftRight">
                                    <th colspan="2" style="font-weight: bold; width:30%">
                                        Date
                                    </th>
                                    <th colspan="" style="width: 20%;font-weight: bold">
                                        Time
                                    </th>
                                    <th colspan="" style="width: 15%;font-weight: bold">
                                        Duration
                                    </th>
                                    <th colspan="" style="width: 15%;font-weight: bold; text-align:center">
                                        Capacity
                                    </th>
                                    <th style="width: 20%;font-weight: bold; text-align:center"> 
                                        Status
                                    </th>
                                </tr>
                                <tr class="trBorderLeftRight">
                                    <td colspan="2" style="max-width: 1px;">
                                        <span id="placeholderSessionDate" value="{{date('Y-m-d', strtotime($session['sessionDate']))}}" style="display:none">{{date('d/m/Y', strtotime($session['sessionDate']))}}</span>
                                        {{date('d/m/Y', strtotime($session['sessionDate']))}} ({{date('l', strtotime($session['sessionDate']))}})
                                    </td>
                                    <td colspan="" style="max-width: 1px;">
                                        <span id="placeholderSessionTime" value="{{ date('H:i', strtotime($session['sessionTime']))}}" style="display:none">{{ date('H:i', strtotime($session['sessionTime']))}}</span>

                                        {{ date('H:i', strtotime($session['sessionTime']))}}
                                    </td>
                                    <td colspan="" style="max-width: 1px;">
                                        <span id="placeholderSessionDuration" value="{{ __($session['sessionDuration'])}}" style="display:none">{{ __($session['sessionDuration'])}}</span>

                                        @if($session['sessionDuration'] < 60)
                                            {{ __($session['sessionDuration'])}} Minutes
                                        @else
                                        {{ __($session['sessionDuration']/60)}} Hour
                                        @endif
                                    </td>
                                    <td colspan="" style="max-width: 1px; text-align:center">
                                        {{$session->bookedBy->count()}} / {{ __($session['sessionCapacity'])}}
                                    </td>
                                    @if($session['sessionStatus'] == "Pending")
                                        <td style="max-width: 1px; text-align:center; background-color:var(--myYellow); color:white">
                                    @elseif($session['sessionStatus'] == "Confirmed")
                                        <td style="max-width: 1px; text-align:center; background-color:#4CAF50; color:white">
                                    @elseif($session['sessionStatus'] == "Cancelled")
                                        <td style="max-width: 1px; text-align:center; background-color:#a10c25; color:white">
                                    @endif
                                        {{$session['sessionStatus']}}
                                    </td>
                                </tr>
                                @if($boolIsAdmin || $boolIsStudentBooked || $boolIsClassTeacher)
                                    <tr class="trBorderLeftRight">
                                        <th colspan="5" style="font-weight: bold;">
                                            Link
                                        </th>
                                        <th colspan="" style="text-align:right; font-weight: bold;padding-top:0; padding-bottom:0">
                                            {{-- @if(($boolIsAdmin || $boolIsClassTeacher)) --}}
                                            @if(($boolIsAdmin || $boolIsClassTeacher) && $boolTimeDiff)
                                                <button type="button" style="background-color:transparent; box-shadow: 0px 0px 0px transparent; border: none; text-shadow: 0px 0px 0px transparent; height:30; padding:0 " onclick="addChangeLinkModalShow()" >
                                                    <img class="deletebutton1" src="/images/icons/linkgreen.png" style="width: 25" title="Add / Change link"/>
                                                </button>
                                            @endif
                                        </th>
                                    </tr>
                                    <tr class="trBorderLeftRight">
                                        <td colspan="6" style="max-width: 1px;">
                                            @if($session['sessionLink'] == null)
                                                No link posted yet!
                                            @else
                                                <span id="creditUserID" style="display: none;" value="{{Auth::User()->id}}">{{Auth::User()->id}}</span>
                                                <span id="creditSessionID" style="display: none;" value="{{$session['id']}}">{{$session['id']}}</span>
                                                @if (strpos($session['sessionLink'], "https://") !== false || strpos($session['sessionLink'], "http://") !== false )
                                                    <span id="sessionLink"><a href="{{$session['sessionLink']}}" target="_blank" onclick="payTeacherCredit()">{{ $session['sessionLink']}}</a></span>
                                                @else 
                                                    <span id="sessionLink"><a href="//{{$session['sessionLink']}}" target="_blank" onclick="payTeacherCredit()">{{ $session['sessionLink']}}</a></span>
                                                @endif
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                            </table>
                            @if($session['sessionStatus'] == "Pending" && $boolIsClassTeacher)
                                <form id="confirmSessionBooking" method="POST" action="{{ url('/confirmSessionBooking') }}">
                                    @csrf
                                    <input type="hidden" name="sessionID" value="{{$session['id']}}">
                                </form>
                                <form id="cancelSessionBooking" method="POST" action="{{ url('/cancelSessionBooking') }}">
                                    @csrf
                                    <input type="hidden" name="sessionID" value="{{$session['id']}}">
                                </form>
                                <div class="form-group row mb-0 justify-content-center" style="text-align: center;">
                                    <button id="confirmBookingButtonasd" form="confirmSessionBooking" type="submit" class="button1" style="width: 250" onclick="return confirm('Confirm this session?')">
                                        {{ __('Confirm Session') }}
                                    </button>
                                    &nbsp &nbsp
                                    <button id="cancelBookingButton" form="cancelSessionBooking" type="submit" class="buttonReject" style="width: 250" onclick="return confirm('Confirm cancel this session?')()">
                                        {{ __('Cancel Session') }}
                                    </button>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if ($boolSessionTimeDate)
            <br>
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header" style="background-color: #a10c25; color:white; font-size:20">
                                <table style="border:none; margin-bottom:0; padding:0;">
                                    <tr>
                                        <th class="tdNoStyle" style="font-size:20; vertical-align:middle;">
                                            <b>Progress</b>
                                        </th>
                                        @if(($boolIsAdmin || $boolIsClassTeacher) && $boolSessionTimeDate && $session['sessionStatus'] != "Cancelled")
                                            <th class="tdNoStyle" style="text-align: right; padding:0; vertical-align:middle;">
                                                <button type="button" style="background-color:transparent; box-shadow: 0px 0px 0px transparent; border: none; text-shadow: 0px 0px 0px transparent; height:30; padding:0 " onclick="progressTextAreaEnable()" >
                                                    <img class="deletebutton1" src="/images/icons/editblue.png" style="width: 25" title="Edit user"/>
                                                </button>
                                            </th>
                                        @endif
                                    </tr>
                                </table>
                            </div>
                            <div class="card-body" style="text-align: right">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <table style="border: 1px solid black; width:100%;" id="studentProgressTable">
                                    <form name="submitSessionProgress" id="submitSessionProgress"  method="POST" action="{{ url('/submitSessionProgress') }}">
                                        @csrf
                                        <input type="hidden" name="sessionID" value="{{$session['id']}}">
                                        <tr>
                                            <td style="text-align: center">
                                                <textarea id="textAreaProgress" name="sessionProgress" class="form-control" col="" disabled>{{$session['sessionProgress']}}</textarea>
                                            </td>
                                        </tr>
                                    </form>
                                </table>
                                @if (($boolIsClassTeacher || $boolIsAdmin) && ($session['sessionStatus'] != "Cancelled") && ($session->bookedBy->count() > 0) && ($boolSessionTimeDate))
                                {{-- @if (($boolIsClassTeacher || $boolIsAdmin) && ($session['sessionStatus'] != "Cancelled") && ($session->bookedBy->count() > 0)) --}}
                                    <div style="">
                                        <button id="addSessionProgressButton" form="submitSessionProgress" type="submit" class="button1" style="width: 150; display:none; margin-right:0px" onclick="return confirm('Confirm this session?')">
                                            {{ __('Submit Progress') }}
                                        </button>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
        <br>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="background-color: #a10c25; color:white; font-size:20"><b>Students</b></div>
                        <div class="card-body" style="text-align: right">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <table style="width:100%">
                                <tr class="nostyle" style="background-color: white; border:none">
                                    <td style="border-right: 1px none black;border-top: none; width:10%">
                                        Name :
                                    </td>
                                    <td style="border-right: 1px none black;border-top: none; width:35%">
                                        <input class="form-control" style="border-radius: 6px; padding-left:10px" type="text" id="studentSearchName" onkeyup="filterStudent()" placeholder="Filter by name.." title="Type in a name">
                                    </td>
                                    <td style="border-right: 1px none black;border-top: none; width:%; text-align:right">
                                        Date :
                                    </td>
                                    <td style="border-right: 1px none black;border-top: none; width:35%">
                                        <input id="studentSearchDate" type="date" class="datepicker1 form-control @error('sessionDate') is-invalid @enderror" name="studentSearchDate" value="{{ old('sessionDate')}}" onchange="filterStudent()" autocomplete="sessionDate" required>
                                    </td>
                                </tr>
                            </table>
                            <table style="border: 1px solid black; width:100%" id="studentListTable">
                                <tr>
                                    <th style="font-weight: bold; width:5%">
                                        No
                                    </th>
                                    <th style="font-weight: bold">
                                        Name
                                    </th>
                                    <th style="font-weight: bold; width:15%; text-align: center">
                                        Booked on
                                    </th>
                                    <th style="font-weight: bold; width:15%; text-align: center">
                                        Attendance
                                    @if(($boolIsClassTeacher || $boolIsAdmin) && ($session['sessionStatus'] != "Cancelled") && $session->bookedBy->count() != 0 && ($boolSessionTimeDate))
                                        {{-- @if(($boolIsClassTeacher || $boolIsAdmin) && ($session['sessionStatus'] != "Cancelled") && $session->bookedBy->count() != 0) --}}
                                            <input type="checkbox" id="parentCheckBoxAttendance" name="parentCheckBox" onchange="selectAllAttendance()">
                                        @endif
                                    </th>
                                </tr>
                                @if($session->bookedBy->count() == 0)
                                    <tr>
                                        {{-- <td colspan="3" style="text-align: center;"> --}}
                                        <td colspan="4" style="text-align:center">
                                            No student book yet!
                                        </td>
                                    </tr>
                                @else
                                    @php
                                        $counter = 1;
                                    @endphp
                                    <form name="submitAttendanceForm" id="submitAttendanceForm"  method="POST" action="{{ url('/submitAttendance') }}">
                                        @csrf
                                        <input type="hidden" name="sessionID" value="{{$session['id']}}">
                                        @foreach($session->bookedBy as $student)
                                            <tr class="trStudentList">
                                                <td class="tdNo" style="text-align: center">
                                                </td>
                                                <td class="tdStudentName">
                                                    {{ __($student['name'])}}
                                                </td>
                                                <td class="tdEnrollDate">
                                                    {{date('d/m/Y', strtotime($student->pivot->created_at))}}
                                                </td>
                                                @if(($boolIsClassTeacher || $boolIsAdmin) && ($session['sessionStatus'] != "Cancelled") && $boolSessionTimeDate)
                                                {{-- @if(($boolIsClassTeacher || $boolIsAdmin) && ($session['sessionStatus'] != "Cancelled")) --}}
                                                    <td class="tdAttendance" style="text-align: center">
                                                        <input type="checkbox" class="childCheckBoxAttendance" name="studentID[]" onchange="enableSubmitButton()" value="{{ __($student['id'])}}" {{$student->pivot->sessionAttendance == "Attended" ? 'checked' : ''}} >
                                                    </td>
                                                @else
                                                    <td class="tdAttendance" style="text-align: center; color:white; {{$student->pivot->sessionAttendance == 'Attended' ? 'background-color:#4CAF50' : 'background-color:#a10c25'}}" >
                                                        {{$student->pivot->sessionAttendance}}
                                                    </td>
                                                @endif
                                            </tr>
                                        @endforeach
                                    </form>
                                    <tr id="trNoResult" class="trBorderLeftRight" style="display: none; ">
                                        {{-- <td colspan="{{(($boolIsClassTeacher || $boolIsAdmin) && ($session['sessionStatus'] != 'Cancelled') && ($boolSessionTimeDate)) ? '4' : '3' }}" style="text-align:center"> --}}
                                        <td colspan="4" style="text-align:center">
                                            No student matched you filter...
                                        </td>
                                    </tr>
                                @endif
                            </table>
                            @if (($boolIsClassTeacher || $boolIsAdmin) && ($session['sessionStatus'] != "Cancelled") && ($session->bookedBy->count() > 0) && ($boolSessionTimeDate))
                            {{-- @if (($boolIsClassTeacher || $boolIsAdmin) && ($session['sessionStatus'] != "Cancelled") && ($session->bookedBy->count() > 0)) --}}
                                <div>
                                    <button id="confirmAttendanceButton" form="submitAttendanceForm" type="submit" class="button1" style="width: 150" onclick="return confirm('Confirm this session?')">
                                        {{ __('Update Attendance') }}
                                    </button>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if($boolIsAdmin || $boolIsClassTeacher)
            <div id="editModal12" class="modal" style="border: none;">
                <div class="row justify-content-center" id="editModal22" >
                    <div class="col-md-8 row justify-content-center" id="editModal32" style="border: none; text-align:center" >
                        <div class="card" id="editCard2" style="border: none; width: 600">
                            <div class="card-header" style="background-color:#a10c25; color:white; width:100%"><b>{{ __('Edit session') }}</b></div>

                            <div class="card-body">
                                {{-- <form method="POST" action="{{ route('createSession') }}"> --}}
                                <form method="POST" action="{{ route('editSession') }}">
                                    @csrf
                                    <div class="form-group row">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Class') }}</label>
                                        <div class="col-md-6">
                                            <input type="hidden" name="classID" value="{{$session->class_['id']}}">
                                            <input id="inputSessionID" type="hidden" name="sessionID" value="{{$session['id']}}">
                                            <input id="inputSessionTeacherID" type="hidden" name="teacherID" value="{{$session->class_->user['id']}}">
                                            <input id="className2" type="text" class="form-control @error('className') is-invalid @enderror" name="className" value="{{$session->class_['className']}}" disabled autocomplete="className" autofocus>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="sessionDate" class="col-md-4 col-form-label text-md-right">{{ __('Date') }}</label>
                                        <div class="col-md-6">
                                            <input id="inputSessionDate" type="date" class="datepicker1 form-control @error('sessionDate') is-invalid @enderror" name="sessionDate" value="{{ old('sessionDate') == "" ? $session['sessionDate'] : old('sessionDate') }}" onchange="datetimePickerValidate()" autocomplete="sessionDate" required>
                                            @error('sessionDate')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            <span id="invalidDate" class="invalid-feedback-man" role="alert" style="">
                                                <strong>Date cannot before today's</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="sessionTime" class="col-md-4 col-form-label text-md-right">{{ __('Time') }}</label>
                                        <div class="col-md-6" style="border:blue none; padding-bottom:0">
                                            <input id="inputSessionTime" type="time" class="timePicker1 form-control @error('sessionTime') is-invalid @enderror" name="sessionTime" value="{{ old('sessionTime') == "" ? $session['sessionTime'] : old('sessionTime') }}" onchange="datetimePickerValidate()" step="" autocomplete="sessionTime" required autofocus>
                                            @error('sessionDate')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            <span id="invalidTime" class="invalid-feedback-man" role="alert" style="">
                                                <strong>Time and date cannot before current time</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="sessionTime" class="col-md-4 col-form-label text-md-right">{{ __('Duration') }}</label>
                                        <div class="col-md-6" style="border:blue none; padding-bottom:0">
                                            <select id="inputSessionDuration" name="sessionDuration" class="form-control" onchange="datetimePickerValidate()">
                                                <option value="{{ old('sessionTime') == "" ? $session['sessionDuration'] : old('sessionDuration') }}" selected hidden>{{ old('sessionDuration') == "" ? $session['sessionDuration'] : old('sessionDuration') }} Minutes</option>
                                                <option value="15">15 Minutes</option>
                                                <option value="30">30 Minutes</option>
                                                <option value="45">45 Minutes</option>
                                                <option value="60">1 Hour</option>
                                            </select>
                                            @error('sessionDuration')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="sessionCost" class="col-md-4 col-form-label text-md-right">{{ __('Cost') }}</label>
                                        <div class="col-md-6" style="border:blue none; padding-bottom:0">
                                            <input id="sessionCost" type="number" class="form-control @error('sessionCost') is-invalid @enderror" name="sessionCost" value="{{ old('sessionCost') == "" ? $session['sessionCost'] : old('sessionCost') }}" step="1" min="0" autocomplete="sessionCost" required autofocus>
                                            @error('sessionCost')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="sessionCapacity" class="col-md-4 col-form-label text-md-right">{{ __('Capacity') }}</label>
                                        <div class="col-md-6" style="border:blue none; padding-bottom:0">
                                            <input id="sessionCapacity" type="number" class="form-control @error('sessionCapacity') is-invalid @enderror" name="sessionCapacity" value="{{ old('sessionCapacity') == "" ? $session['sessionCapacity'] : old('sessionCapacity') }}" step="1" min="1" autocomplete="sessionCapacity" required autofocus>
                                            @error('sessionCapacity')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row justify-content-center">
                                        <span id="invalidDateTime" class="invalid-feedback-man" role="alert" style="">
                                            <strong>Date and Time selected already has a session</strong>
                                        </span>
                                        <span id="invalidCurrentTimeDate" class="invalid-feedback-man" role="alert" style="">
                                            <strong>Date and time cannot before current date and time</strong>
                                        </span>
                                        <span id="invalidCurrentTimeDateTeacher" class="invalid-feedback-man" role="alert" style="">
                                            <strong>Teacher already has a session on the selected date and time</strong>
                                        </span>
                                    </div>
                                    <div class="form-group row mb-0 justify-content-center" style="text-align: center;">
                                        <input type="hidden" name="adminReg" value="1">
                                        <button id="submitButton1" type="submit" class="button1" style="width: 100" onclick="datePickerValidate()">
                                            {{ __('Submit') }}
                                        </button>
                                        &nbsp &nbsp
                                        <button type="button" class="buttonReject" style="width: 100" onclick="eventCloseEditFunction2()">
                                            {{ __('Cancel') }}
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="addchangeLinkModal" class="modal" style="border: none;">
                <div class="row justify-content-center" id="addchangeLinkModal2" >
                    <div class="col-md-8 row justify-content-center" id="addchangeLinkModal3" style="border: none; text-align:center" >
                        <div class="card" id="addchangeLinkCard" style="border: none; width: 600">
                            <div class="card-header" style="background-color:#a10c25; color:white; width:100%"><b>{{ __('Add session link') }}</b></div>
                            <div class="card-body">
                                <form method="POST" action="{{ route('addSessionLink') }}">
                                    @csrf
                                    <div class="form-group row">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Class') }}</label>
                                        <div class="col-md-6">
                                            <input id="inputSessionID2" type="hidden" name="sessionID" value="{{$session['id']}}">
                                            <input id="className22" type="text" class="form-control @error('className') is-invalid @enderror" name="className" value="{{$session->class_['className']}}" disabled autocomplete="className" autofocus>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="sessionDate" class="col-md-4 col-form-label text-md-right">{{ __('Date') }}</label>
                                        <div class="col-md-6">
                                            <input id="inputSessionDate2" type="date" class="datepicker1 form-control @error('sessionDate') is-invalid @enderror" name="sessionDate" value="{{ old('sessionDate') == "" ? $session['sessionDate'] : old('sessionDate') }}" onchange="datetimePickerValidate()" autocomplete="sessionDate" disabled>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="sessionTime" class="col-md-4 col-form-label text-md-right">{{ __('Time') }}</label>
                                        <div class="col-md-6" style="border:blue none; padding-bottom:0">
                                            <input id="inputSessionTime2" type="time" class="timePicker1 form-control @error('sessionTime') is-invalid @enderror" name="sessionTime" value="{{ old('sessionTime') == "" ? $session['sessionTime'] : old('sessionTime') }}" onchange="datetimePickerValidate()" step="" autocomplete="sessionTime" disabled >
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="sessionLink" class="col-md-4 col-form-label text-md-right">{{ __('Link') }}</label>
                                        <div class="col-md-6" style="border:blue none; padding-bottom:0">
                                            <input id="sessionLink" type="text" class="form-control @error('sessionLink') is-invalid @enderror" name="sessionLink" value="{{ old('sessionLink') == "" ? $session['sessionLink'] : old('sessionLink') }}" autocomplete="sessionLink" required autofocus>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-0 justify-content-center" style="text-align: center;">
                                        <input type="hidden" name="adminReg" value="1">
                                        <button id="submitButton12" type="submit" class="button1" style="width: 100" onclick="">
                                            {{ __('Submit') }}
                                        </button>
                                        &nbsp &nbsp
                                        <button type="button" class="buttonReject" style="width: 100" onclick="eventCloseAddChangeLink()">
                                            {{ __('Cancel') }}
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endif

    @if(session()->get('sessionEdited') != null)
        <span id="sessionEdited" style="display: none"></span>
    @endif
    @if(session()->get('sessionBookCancel') != null)
        <span id="sessionBookCancel" style="display: none">{{session()->get('sessionBookCancel')}}</span>
    @endif
    @if(session()->get('sessionLinkAdded') != null)
        <span id="sessionLinkAdded" style="display: none"></span>
    @endif
    @if(session()->get('confirmcancelSessionBooking') != null)
        <span id="confirmcancelSessionBooking" style="display: none">{{session()->get('confirmcancelSessionBooking')}}</span>
    @endif
    @if(session()->get('confirmUpdateAttendance') != null)
        <span id="confirmUpdateAttendance" style="display: none">{{session()->get('confirmUpdateAttendance')}}</span>
    @endif
    @if(session()->get('confirmUpdateProgress') != null)
        <span id="confirmUpdateProgress" style="display: none">{{session()->get('confirmUpdateProgress')}}</span>
    @endif
    
    
    

    <script>
        window.onload = function(){ //run these after page loaded

            var confirmAttendanceButton = document.getElementById("confirmAttendanceButton");
            if(confirmAttendanceButton != null){
                confirmAttendanceButton.disabled = true;
            }

            validateDateTimeStudent();
            filterStudent();
            

            if(document.getElementById('sessionEdited') != null){
                setTimeout(function sessionEdited(){
                    alert("Successfully edit this session!");
                },50);
            }
            if(document.getElementById('sessionBookCancel') != null){
                setTimeout(function sessionBookCancel(){
                    alert(document.getElementById('sessionBookCancel').innerHTML);
                },50);
            }
            if(document.getElementById('sessionLinkAdded') != null){
                setTimeout(function sessionLinkAdded(){
                    alert("Successfully add link to the session!");
                },50);
            }
            if(document.getElementById('confirmcancelSessionBooking') != null){
                setTimeout(function confirmcancelSessionBookingAlert(){
                    alert(document.getElementById('confirmcancelSessionBooking').innerHTML);
                },50);
            }
            if(document.getElementById('confirmUpdateAttendance') != null){
                setTimeout(function confirmcancelSessionBookingAlert(){
                    alert(document.getElementById('confirmUpdateAttendance').innerHTML);
                },50);
            }
            if(document.getElementById('confirmUpdateProgress') != null){
                setTimeout(function confirmcancelSessionBookingAlert(){
                    alert(document.getElementById('confirmUpdateProgress').innerHTML);
                },50);
            }
            
            if( document.getElementById("BookFormButton")!=null){
                getCreditBalance();
            }
        };

        

        function getCreditBalance(){
            var studentID = document.getElementById("studentID").value;
            var sessionCost = document.getElementById("sessionCost").innerHTML;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'post',
                url: '{{url("/getCreditBalance")}}',
                dataType: 'JSON',
                async:false, //ensure process finish then exit the function
                // data: {
                //     _token: CSRF_TOKEN,
                //     date1: inputSessionDate,
                //     time1: inputSessionTime
                data: {
                    userID : studentID,
                },
                success: function (data) {
                    if(data.creditBalance < sessionCost){
                        document.getElementById("BookFormButton").disabled = true;
                        document.getElementById("BookFormButton").style.display = 'none';
                        document.getElementById("invalidCreditAmount").style.display = "block";
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
            });
        }

        function progressTextAreaEnable(){
            var textAreaProgress = document.getElementById("textAreaProgress");
            var addSessionProgressButton = document.getElementById("addSessionProgressButton");

            if(textAreaProgress.disabled == true){
                textAreaProgress.disabled = false;
                addSessionProgressButton.style.display = '';

            }else{
                textAreaProgress.disabled = true;
                addSessionProgressButton.style.display = 'none';
            }
            
        }

        function enableSubmitButton(){
            document.getElementById("confirmAttendanceButton").disabled = false;
        }

        function selectAllAttendance(){
            var box = document.getElementsByClassName("childCheckBoxAttendance");
            var sbmt = document.getElementById("confirmAttendanceButton");
            if(document.getElementById("parentCheckBoxAttendance").checked == true){
                for(i = 0; i<box.length; i++ ){
                    box[i].checked = true;
                }
                sbmt.disabled = false;
            }else{
                deselectAll();
            }
        }

        function deselectAll(){
            var superCheckBox = document.getElementById("parentCheckBoxAttendance");
            superCheckBox.checked = false;
            var box = document.getElementsByClassName("childCheckBoxAttendance");
            for(i = 0; i<box.length; i++ ){
                box[i].checked = false;
            }
        }


        function datetimePickerValidate(){
            var boolDate = datePickerValidate();
            var boolTime = timePickerValidate();
            var boolDateTimeTeacher;
            var inputSessionDate = document.getElementById('inputSessionDate').value;
            var inputSessionTime = document.getElementById('inputSessionTime').value;
            var inputSessionDuration = document.getElementById('inputSessionDuration').value;
            var inputSessionID = document.getElementById('inputSessionID').value;
            var inputSessionTeacherID = document.getElementById('inputSessionTeacherID').value;
            var hours = inputSessionTime.split(":")[0];
            var minutes = inputSessionTime.split(":")[1];
            var date = new Date();
            var currentTimeValue = new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(),0,0);
            var inputSessionDateFull = new Date(document.getElementById('inputSessionDate').value);
            inputSessionTimeFull = new Date(inputSessionDateFull.getFullYear(), inputSessionDateFull.getMonth(), inputSessionDateFull.getDate(), hours, minutes, 0, 0);
            
            if(inputSessionTimeFull >= currentTimeValue){
                boolDateTimeTeacher = validateDateTimeTeacher(inputSessionDate, inputSessionTime, inputSessionDuration, inputSessionID, inputSessionTeacherID);
            }else{
                document.getElementById('invalidCurrentTimeDateTeacher').style.display = 'none';
            }
            isInvalidClass(boolDate, boolTime, boolDateTimeTeacher);
            if(boolDate && boolTime && boolDateTimeTeacher){
                var submitButton1 = document.getElementById('submitButton1').disabled = false;
            }else{
                var submitButton1 = document.getElementById('submitButton1').disabled = true;
            }
        }

        function isInvalidClass(boolDate, boolTime, boolDateTimeTeacher){
            
            if(!boolDate){
                document.getElementById('inputSessionDate').classList.add('is-invalid');
            }else if(boolDate){
                document.getElementById('inputSessionDate').classList.remove('is-invalid');
            }
            if(!boolTime){
                document.getElementById('inputSessionTime').classList.add('is-invalid');
            }else if(boolTime){
                document.getElementById('inputSessionTime').classList.remove('is-invalid');
            }
            if(!boolDateTimeTeacher){
                document.getElementById('inputSessionTime').classList.add('is-invalid');
                document.getElementById('inputSessionDate').classList.add('is-invalid');
            }else if(boolDateTimeTeacher && boolDate && boolTime){
                document.getElementById('inputSessionTime').classList.remove('is-invalid');
                document.getElementById('inputSessionDate').classList.remove('is-invalid');
            }
        }

        function validateDateTimeTeacher(inputSessionDate, inputSessionTime, inputSessionDuration, inputSessionID, inputSessionTeacherID){
            // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var check;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'post',
                url: '{{url("/ajaxcheckValidDatetime")}}',
                dataType: 'JSON',
                async:false, //ensure process finish then exit the function
                // data: {
                //     _token: CSRF_TOKEN,
                //     date1: inputSessionDate,
                //     time1: inputSessionTime
                data: {
                    sessionDate : inputSessionDate,
                    sessionTime : inputSessionTime,
                    sessionDuration : inputSessionDuration,
                    sessionID : inputSessionID,
                    teacherID : inputSessionTeacherID,
                },
                success: function (data) {
                    if(data.check){
                        document.getElementById('invalidCurrentTimeDateTeacher').style.display = 'block';
                        check = false;
                    }else{
                        document.getElementById('invalidCurrentTimeDateTeacher').style.display = 'none';
                        check = true;
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
            });
            return check;
        }

        function datePickerValidate(){
            var inputSessionDate = new Date(document.getElementById('inputSessionDate').value);
            var date = new Date();
            var currentDateValue = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 8,0,0,0);
            if(inputSessionDate < currentDateValue){
                var alertMessage = document.getElementById('invalidCurrentTimeDate').style.display = 'block';
                return false;
            }else{
                var alertMessage = document.getElementById('invalidCurrentTimeDate').style.display = 'none';
                return true;
            }
        }

        function timePickerValidate(){
            var inputSessionTime = document.getElementById('inputSessionTime').value;
            var hours = inputSessionTime.split(":")[0];
            var minutes = inputSessionTime.split(":")[1];
            var date = new Date();
            var currentTimeValue = new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(),0,0);
            var inputSessionDate = new Date(document.getElementById('inputSessionDate').value);
            inputSessionTime = new Date(inputSessionDate.getFullYear(), inputSessionDate.getMonth(), inputSessionDate.getDate(), hours, minutes, 0, 0);
            if(inputSessionTime <= currentTimeValue){
                var alertMessage = document.getElementById('invalidCurrentTimeDate').style.display = 'block';
                return false;
            }else{
                var alertMessage = document.getElementById('invalidCurrentTimeDate').style.display = 'none';
                return true;
            }
        }

        function validateDateTimeStudent(){
            // var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
            var placeholderSessionDate = document.getElementById('placeholderSessionDate').innerHTML;
            var placeholderSessionTime = document.getElementById('placeholderSessionTime').innerHTML;
            var placeholderSessionDuration = document.getElementById('placeholderSessionDuration').innerHTML;

            var check1;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'post',
                url: '{{url("/checkValidDatetimeStudent")}}',
                dataType: 'JSON',
                async:false, //ensure process finish then exit the function
                data: {
                    sessionDate : placeholderSessionDate,
                    sessionTime : placeholderSessionTime,
                    sessionDuration : placeholderSessionDuration,
                },
                success: function (data) {
                    if(data.check){
                        check1 = false;
                    }else{
                        check1 = true;
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
            });
            if(!check1){
                if( document.getElementById("invalidStudentBookSameDateTime")!=null){
                    document.getElementById("invalidStudentBookSameDateTime").style.display = "block";
                }
                if( document.getElementById("BookFormButton")!=null){
                    document.getElementById("BookFormButton").disabled = true;
                    document.getElementById("BookFormButton").style.display = 'none';
                }
            }
        }

        var editModal12 = document.getElementById("editModal12");
        var editModal22 = document.getElementById("editModal22");
        var editModal32 = document.getElementById("editModal32");
        var editCard2 = document.getElementById("editCard2");

        function eventCloseEditFunction2() {
            const body = document.body;
            body.style.overflowY = '';
            editModal12.style.display = "none";
        };

        function editModalShow2() {
            editModal12.style.display = "block";
            editModal12.style.overflowY = "";
            const body = document.body;
            body.style.overflowY = 'hidden';
        };


        document.addEventListener("click", function(e)
        {
            if ((e.target==editModal12 || e.target==editModal22 || e.target==editModal32))
            {
                eventCloseEditFunction2();
            }
        });

        // *********************************************************************************** down

        var addchangeLinkModal = document.getElementById("addchangeLinkModal");
        var addchangeLinkModal2 = document.getElementById("addchangeLinkModal2");
        var addchangeLinkModal3 = document.getElementById("addchangeLinkModal3");
        var addchangeLinkCard = document.getElementById("addchangeLinkCard");

        function eventCloseAddChangeLink() {
            const body = document.body;
            body.style.overflowY = '';
            addchangeLinkModal.style.display = "none";
        };

        function addChangeLinkModalShow() {
            addchangeLinkModal.style.display = "block";
            addchangeLinkModal.style.overflowY = "";
            const body = document.body;
            body.style.overflowY = 'hidden';
        };

        document.addEventListener("click", function(e)
        {
            if ((e.target==addchangeLinkModal || e.target==addchangeLinkModal2 || e.target==addchangeLinkModal3))
            {
                eventCloseAddChangeLink();
            }
        });

        // *********************************************************************************** up

        function filterStudent() {
            var studentSearchName, studentListTable, trStudentList, tdStudentName, tdEnrollDate, i, txtTdStudentName, txtTdEnrollDate, studentSearchDate, trCounter = 0;
            studentSearchName = document.getElementById("studentSearchName");
            studentSearchDate = document.getElementById("studentSearchDate");
            if(studentSearchDate.value != ""){
                studentSearchDate = new Date(studentSearchDate.value);
                studentSearchDate = studentSearchDate.getDate() + "/" + (studentSearchDate.getMonth()+1) + "/" + studentSearchDate.getFullYear();
            }else{
                studentSearchDate = "";
            }
            studentSearchName = studentSearchName.value.toUpperCase();
            studentListTable = document.getElementById("studentListTable");
            trStudentList = studentListTable.getElementsByTagName("tr");
            for (i = 0; i < trStudentList.length; i++) {
                tdStudentName = trStudentList[i].getElementsByClassName("tdStudentName")[0];
                tdEnrollDate = trStudentList[i].getElementsByClassName("tdEnrollDate")[0];
                tdNo = trStudentList[i].getElementsByClassName("tdNo")[0];
                if (tdStudentName && tdEnrollDate) {
                    txtTdStudentName = tdStudentName.textContent || tdStudentName.innerText;
                    txtTdEnrollDate = tdEnrollDate.textContent || tdEnrollDate.innerText;
                    if(studentSearchDate.value != ""){
                        var day = txtTdEnrollDate.split("/")[0];
                        var month = txtTdEnrollDate.split("/")[1];
                        var year = txtTdEnrollDate.split("/")[2];
                        txtTdEnrollDate = new Date(year, month, day);
                        txtTdEnrollDate = txtTdEnrollDate.getDate() + "/" + (txtTdEnrollDate.getMonth()) + "/" + txtTdEnrollDate.getFullYear();
                    }
                    if (txtTdStudentName.toUpperCase().indexOf(studentSearchName) > -1 && txtTdEnrollDate.toUpperCase().indexOf(studentSearchDate) > -1) {
                        trStudentList[i].style.display = "";
                        tdNo.innerHTML= ++trCounter;
                    } else {
                        trStudentList[i].style.display = "none";
                    }
                }                 
            }

            var trNoResult = document.getElementById('trNoResult');
            if(trNoResult != null){
                if(trCounter == 0){
                    trNoResult.style.display = "";
                }else{
                    trNoResult.style.display = "none";
                }
            }
        }


        // function payTeacherCredit(){
        //     var creditUserID = document.getElementById("creditUserID").innerHTML;
        //     var creditSessionID = document.getElementById("creditSessionID").innerHTML;
        //     $.ajax({
        //         headers: {
        //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        //         },
        //         type: 'post',
        //         url: '/payTeacherCredit',
        //         dataType: 'JSON',
        //         async:false, 
        //         data: {
        //             userID : creditUserID,
        //             sessionID : creditSessionID
        //         },
        //         success: function (data) {
        //         },
        //         error: function (XMLHttpRequest, textStatus, errorThrown) {
        //         },
        //     });
        // }
    </script>
    @endsection
@endguest