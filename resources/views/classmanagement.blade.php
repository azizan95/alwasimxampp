<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }
    
    th, td {
        padding: 8px;
        text-align: left;
        border-top: 1px solid black;
        font-size: 14;
        
    }

    tr:nth-child(odd) {
        background-color: #f2f2f2;
        
    }

    th {
        text-align: center;
        font-weight: normal;
        font-size: 15;
        background-color: #242d5f;
        color: white;
    }

    .trBorderLeftRight{
        border-left: 1px solid black;
        border-right: 1px solid black;
        border-bottom: 1px solid black;
    }

    .trBorderLeftRight:hover{
        background-color: #ffd52b;
    }

    .tdNoStyle {
    /* margin: 0; */
        padding: 0;
        border-top: none;
        border-left: none;
        border-right: none;
        outline: 0;
        font-size: 100%;
        vertical-align: baseline;
        background-color: white;
    }

    .btnReg:enabled {
        background-color: white; 
        color: black; 
        border: 2px solid #4CAF50;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .btnReg:hover {
        background-color: #4CAF50;
        color: white;
        }
    
    .btnReg:disabled{
        background-color: #ffffff;
        color: grey; 
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .button1:enabled {
        background-color: white; 
        color: black; 
        border: 2px solid #ffcd04;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonPaging {
        background-color: white; 
        color: black; 
        border: 1px solid #4CAF50;
        border-radius: 4px;
        width: 30px;
        margin-top: 2px;
        margin-right: 1px;
        font-size: 12;
    }

    .buttonPagingCurrent{
        background-color: #4CAF50;
        border: 1px solid #4CAF50;
        border-radius: 4px;
        margin-right: 1px;
        margin-top: 2px;
        width: 30px;
        color: white;
        font-size: 12;
    }

    .button1:hover {
        background-color: #ffd52b;
        color: white;
    }
    .buttonPaging:hover {
        background-color: #4CAF50;
        color: white;
    }
    
    .button1:disabled{
        background-color: #ffffff;
        color: grey; 
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonReject:enabled {
        background-color: white; 
        color: black; 
        border: 2px solid #a10c25;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonReject:hover {
        background-color: #a10c25;
        color: white;
        }
    
    .buttonReject:disabled{
        background-color: #ffffff;
        color: grey; 
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .nostyle{
        -webkit-appearance: none; 
        border:1px solid;
    }

    .nostyle-text{
        text-decoration: none; 
    }
    a:link {
        text-decoration: none;
    }
    a:visited {
        text-decoration: none;
    }

    a:hover {
        text-decoration: none;
    }

    a:active {
        text-decoration: none;
    }

    .modal {
        display: block; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 0; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }
    .modal-content {
        /* background-color: #fefefe; */
        background-color: #3D3D3D;
        text-align: center;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        /* width: 10%; */
    }
    .livesearchA{
        all: unset;
    }

    .tdNameHover:hover{
        cursor: pointer;
    }   

    
</style>

@guest
please login

@else
    @extends('layouts.app')
    @section('content')
        @php
            unset($_POST);   
        @endphp
        @if(Auth::user()->userStatus == 0)
            {{ Session::flush() }}
            <script>
                window.alert("Your account is yet to be approved. Please wait for approval email from Admin");
                window.location = "/";
            </script>
        @else
            @if(Auth::user()->userRole == "Student")
                <div id="containerUserList2" class="container container75" style="border: black solid none; min-height:80%;">
                    <div class="row justify-content-center" style="border:solid red none;">
                        <div class="col-md-8" style="border:solid cyan none;height:80%;">
                            <div class="card" style="border:solid yellow none;height:80%;">
                                <div class="card-header" name="userList" style="background-color: #a10c25; color:white; font-size:20"><b>Class Enrolled</b></div>

                                <div class="card-body" style="text-align: right;  border:solid purple none; height:80%">
                                    @if (session('status'))
                                        <div class="alert alert-success" role="alert">
                                            {{ session('status') }}
                                        </div>
                                    @endif
                                    <table style="width:100%">
                                        <tr class="" style="background-color: white; border:none; ">
                                            <td style="border-top: none">
                                                Name
                                            </td>
                                            <td style="border-top: none">
                                                <input class="nostyle form-control" style="border-radius: 6px; padding-left:10px" type="text" id="myInput2" onkeyup="myFunctionReset2()" placeholder="Filter by class name.." title="Type in a class name">
                                            </td>
                                            <td style="border-top: none">
                                                &nbsp
                                            </td>
                                            <td style="border-top: none">
                                                &nbsp
                                            </td>
                                            <td style="border-top: none">
                                                &nbsp
                                            </td>
                                            <td style="border-top: none">
                                                Teacher
                                            </td>
                                            <td style="border-top: none">
                                                <input class="nostyle form-control" style="border-radius: 6px; padding-left:10px" type="text" id="myInputPhoneNo2" onkeyup="myFunctionReset2()" placeholder="Filter by teacher's name.." title="Type in a teacher's name">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: none;">
                                                Description
                                            </td>
                                            <td style="border-top: none">
                                                <input class="nostyle form-control" style="border-radius: 6px; padding-left:10px" type="text" id="myInputEmail2" onkeyup="myFunctionReset2()" placeholder="Filter by class description.." title="Type in a class description">
                                            </td>
                                            <td style="border-top: none">
                                                &nbsp
                                            </td>
                                            <td style="border-top: none">
                                                &nbsp
                                            </td>
                                            <td style="border-top: none">
                                                &nbsp
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="border: none; width:100%;overflow-x:hidden;" id="myTable2">
                                        <tr>
                                            <td class="tdNoStyle" colspan="2">
                                            </td>
                                            <td colspan="3" class="tdNoStyle" style="font-size:12; text-align:right; vertical-align:bottom">
                                                <span>
                                                    Show entries : 
                                                    <select class="" id="inputShowEntries2" onchange="myFunctionReset2()" style="margin-bottom: 2; margin-right:">
                                                        <option selected>10</option>
                                                        <option>20</option>
                                                        <option>30</option>
                                                        <option>100</option>
                                                    </select>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr class="trBorderLeftRight">
                                            <th style="font-weight: bold; width:5%">
                                                No.
                                            </th>
                                            <th style="font-weight: bold;">
                                                Name
                                            </th>
                                            <th style="font-weight: bold;">
                                                Description
                                            </th>
                                            <th style="font-weight: bold;">
                                                Teacher
                                            </th>
                                            <th style="font-weight: bold; width:10%">
                                                Sessions
                                            </th>
                                        </tr>
                                        @if(Auth::user()->enrolledIn()->count() == 0)
                                            <tr class="trBorderLeftRight">
                                                <td colspan="5" style="text-align: center;">
                                                    No class enrolled in yet! 
                                                </td>
                                            </tr>
                                        @else
                                            @php
                                                $counterClass = 0;
                                            @endphp
                                            @foreach(Auth::user()->enrolledIn as $class_)
                                                <tr class="trBorderLeftRight">
                                                    <td class="tdNo2" style="text-align: center">
                                                    </td>
                                                    <td class="tdName2 tdNameHover" onclick="window.location='classDetails/{{$class_['id']}}'">
                                                        {{ __($class_['className'])}}
                                                    </td>
                                                    <td class="tdEmail2" style=" max-width: 300px;">
                                                        {{ __($class_['classDesc'])}}
                                                    </td>
                                                    <td class="tdPhoneNo2 tdNameHover" style="text-align: left;" onclick="window.location='userProfile/{{$class_->user->id}}'">
                                                        {{ __($class_->user->name)}}
                                                    </td>
                                                    <td class="tdRole2" style="text-align: center">
                                                        @php
                                                            $currentTimeDate = Date('d-m-Y H:i');
                                                            $sessionCounter = 0;
                                                            foreach($class_->session as $session1){
                                                                $sessionTimeDate = Date($session1['sessionDate'] . $session1['sessionTime']);
                                                                $sessionTimeDate = Date('d-m-Y H:i', strtotime($sessionTimeDate));
                                                                if(Date(strtotime($currentTimeDate)) < Date(strtotime($sessionTimeDate))){
                                                                    $sessionCounter ++;
                                                                }
                                                            }
                                                        @endphp
                                                        {{$sessionCounter}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                            <tr id="trNoResult2" class="trBorderLeftRight" style="display: none; ">
                                                <td colspan="5" style="text-align:center">
                                                    No class matched you filter...
                                                </td>
                                            </tr>  
                                        @endif
                                    </table>
                                    <div id="pagingDiv2" style="border:none;">
                                        <span id="pagingSpan2" style="font-size:"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @elseif(Auth::user()->userRole == "Teacher")
                <div id="containerUserList2" class="container container75" style="border: black solid none; min-height:80%;">
                    <div class="row justify-content-center" style="border:solid red none;">
                        <div class="col-md-8" style="border:solid cyan none;height:80%;">
                            <div class="card" style="border:solid yellow none;height:80%;">
                                <div class="card-header" name="userList" style="background-color: #a10c25; color:white; font-size:20"><b>Class Teaching</b></div>

                                <div class="card-body" style="text-align: right;  border:solid purple none; height:80%">
                                    @if (session('status'))
                                        <div class="alert alert-success" role="alert">
                                            {{ session('status') }}
                                        </div>
                                    @endif
                                    <table style="width:100%">
                                        <tr class="" style="background-color: white; border:none; ">
                                            <td style="border-top: none">
                                                Name
                                            </td>
                                            <td style="border-top: none">
                                                <input class="nostyle form-control" style="border-radius: 6px; padding-left:10px" type="text" id="myInput2" onkeyup="myFunctionReset2()" placeholder="Filter by class name.." title="Type in a class name">
                                            </td>
                                            <td style="border-top: none">
                                                &nbsp
                                            </td>
                                            <td style="border-top: none">
                                                &nbsp
                                            </td>
                                            <td style="border-top: none">
                                                &nbsp
                                            </td>
                                            <td style="border-top: none;">
                                                Description
                                            </td>
                                            <td style="border-top: none">
                                                <input class="nostyle form-control" style="border-radius: 6px; padding-left:10px" type="text" id="myInputEmail2" onkeyup="myFunctionReset2()" placeholder="Filter by class description.." title="Type in a class description">
                                            </td>
                                            
                                        </tr>
                                        <tr style="display:none">
                                            <td style="border-top: none">
                                                Teacher
                                                &nbsp
                                            </td>
                                            <td style="border-top: none">
                                                <input class="nostyle form-control" style="border-radius: 6px; padding-left:10px" type="text" id="myInputPhoneNo2" onkeyup="myFunctionReset2()" placeholder="Filter by teacher's name.." title="Type in a teacher's name">
                                                &nbsp
                                            </td>
                                            <td style="border-top: none">
                                                &nbsp
                                            </td>
                                            <td style="border-top: none">
                                                &nbsp
                                            </td>
                                            <td style="border-top: none">
                                                &nbsp
                                            </td>
                                        </tr>
                                    </table>
                                    <table style="border: none; width:100%;overflow-x:hidden;" id="myTable2">
                                        <tr>
                                            <td class="tdNoStyle" colspan="2">
                                            </td>
                                            <td colspan="2" class="tdNoStyle" style="font-size:12; text-align:right; vertical-align:bottom">
                                                <span>
                                                    Show entries : 
                                                    <select class="" id="inputShowEntries2" onchange="myFunctionReset2()" style="margin-bottom: 2; margin-right:">
                                                        <option selected>10</option>
                                                        <option>20</option>
                                                        <option>30</option>
                                                        <option>100</option>
                                                    </select>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr class="trBorderLeftRight">
                                            <th style="font-weight: bold; width:5%">
                                                No.
                                            </th>
                                            <th style="font-weight: bold;">
                                                Name
                                            </th>
                                            <th style="font-weight: bold;">
                                                Description
                                            </th>
                                            <th style="font-weight: bold; display:none">
                                                Teacher
                                            </th>
                                            <th style="font-weight: bold; width:10%">
                                                Sessions
                                            </th>
                                        </tr>
                                        @if(Auth::user()->classes()->count() == 0)
                                            <tr class="trBorderLeftRight">
                                                <td colspan="4" style="text-align: center;">
                                                    No class enrolled in yet! 
                                                </td>
                                            </tr>
                                        @else
                                            @php
                                                $counterClass = 0;
                                            @endphp
                                            @foreach(Auth::user()->classes as $class_)
                                                <tr class="trBorderLeftRight">
                                                    <td class="tdNo2" style="text-align: center">
                                                    </td>
                                                    <td class="tdName2 tdNameHover" onclick="window.location='classDetails/{{$class_['id']}}'">
                                                        {{ __($class_['className'])}}
                                                    </td>
                                                    <td class="tdEmail2" style=" max-width: 300px;">
                                                        {{ __($class_['classDesc'])}}
                                                    </td>
                                                    <td class="tdPhoneNo2" style="text-align: left;display:none">
                                                        <a style="text-decoration: none; color:black" href="{{url('userProfile/'.$class_->user->id)}}">{{ __($class_->user->name)}}</a>
                                                    </td>
                                                    <td class="tdRole2" style="text-align: center">
                                                        @php
                                                            $currentTimeDate = Date('d-m-Y H:i');
                                                            $sessionCounter = 0;
                                                            foreach($class_->session as $session1){
                                                                $sessionTimeDate = Date($session1['sessionDate'] . $session1['sessionTime']);
                                                                $sessionTimeDate = Date('d-m-Y H:i', strtotime($sessionTimeDate));
                                                                if(Date(strtotime($currentTimeDate)) < Date(strtotime($sessionTimeDate))){
                                                                    $sessionCounter ++;
                                                                }
                                                            }
                                                        @endphp
                                                        {{$sessionCounter}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                            <tr id="trNoResult2" class="trBorderLeftRight" style="display: none; ">
                                                <td colspan="4" style="text-align:center">
                                                    No class matched you filter...
                                                </td>
                                            </tr>  
                                        @endif
                                    </table>
                                    <div id="pagingDiv2" style="border:none;">
                                        <span id="pagingSpan2" style="font-size:"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            <br>
            @if(Auth::user()->userRole == "Student" || Auth::user()->userRole == "Admin")
                <div id="containerUserList" class="container container75" style="border: black solid none; min-height:80%;">
                    <div class="row justify-content-center" style="border:solid red none;">
                        <div class="col-md-8" style="border:solid cyan none;height:80%;">
                            <div class="card" style="border:solid yellow none;height:80%;">
                                <div class="card-header" name="userList" style="background-color: #a10c25; color:white; font-size:20"><b>Class List</b></div>

                                <div class="card-body" style="text-align: right;  border:solid purple none; height:80%">
                                    @if (session('status'))
                                        <div class="alert alert-success" role="alert">
                                            {{ session('status') }}
                                        </div>
                                    @endif
                                    <table style="width:100%">
                                        <tr class="" style="background-color: white; border:none; ">
                                            <td style="border-top: none">
                                                Name
                                            </td>
                                            <td style="border-top: none">
                                                <input class="nostyle form-control" style="border-radius: 6px; padding-left:10px" type="text" id="myInput" onkeyup="myFunctionReset()" placeholder="Filter by class name.." title="Type in a class name">
                                            </td>
                                            <td style="border-top: none">
                                                &nbsp
                                            </td>
                                            <td style="border-top: none">
                                                &nbsp
                                            </td>
                                            <td style="border-top: none">
                                                &nbsp
                                            </td>
                                            <td style="border-top: none">
                                                Teacher
                                            </td>
                                            <td style="border-top: none">
                                                <input class="nostyle form-control" style="border-radius: 6px; padding-left:10px" type="text" id="myInputPhoneNo" onkeyup="myFunctionReset()" placeholder="Filter by teacher's name.." title="Type in a teacher's name">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="border-top: none;">
                                                Description
                                            </td>
                                            <td style="border-top: none">
                                                <input class="nostyle form-control" style="border-radius: 6px; padding-left:10px" type="text" id="myInputEmail" onkeyup="myFunctionReset()" placeholder="Filter by class description.." title="Type in a class description">
                                            </td>
                                            <td style="border-top: none">
                                                &nbsp
                                            </td>
                                            <td style="border-top: none">
                                                &nbsp
                                            </td>
                                            <td style="border-top: none">
                                                &nbsp
                                            </td>
                                        </tr>
                                    </table>
                                    <form name="ApprovalForm" method="POST" action="{{ url('/sendemail') }}">
                                    @csrf
                                        <table style="border: none; width:100%;overflow-x:hidden;" id="myTable">
                                            <tr>
                                                <td class="tdNoStyle" colspan="2">
                                                    @if(Auth::user()->userRole == "Admin")
                                                        <button type="button" id="addUserButton" value="Create Class" class="button1" style="width: auto; margin-bottom:2" onclick="editModalShow()">Create Class</button>
                                                    @endif
                                                </td>
                                                <td colspan="3" class="tdNoStyle" style="font-size:12; text-align:right; vertical-align:bottom">
                                                    <span>
                                                        Show entries : 
                                                        <select class="" id="inputShowEntries" onchange="myFunctionReset()" style="margin-bottom: 2; margin-right:">
                                                            <option selected>10</option>
                                                            <option>20</option>
                                                            <option>30</option>
                                                            <option>100</option>
                                                        </select>
                                                    </span>
                                                </td>
                                            </tr>
                                            <tr class="trBorderLeftRight">
                                                <th style="font-weight: bold; width:5%">
                                                    No.
                                                </th>
                                                <th style="font-weight: bold;">
                                                    Name
                                                </th>
                                                <th style="font-weight: bold;">
                                                    Description
                                                </th>
                                                <th style="font-weight: bold;">
                                                    Teacher
                                                </th>
                                                <th style="font-weight: bold; width:10%">
                                                    Sessions
                                                </th>
                                            </tr>
                                            @if($classes == null)
                                                <tr>
                                                    <td colspan="5" style="text-align: center;">
                                                        No class created yet!
                                                    </td>
                                                </tr>
                                            @else
                                                @php
                                                    $counterClass = 0;   
                                                @endphp
                                                @foreach($classes as $class_)
                                                    @if(Auth::user()->enrolledIn()->where('class__id', $class_['id'])->count() < 1)
                                                        <tr class="trBorderLeftRight">
                                                            <td class="tdNo" style="text-align: center">
                                                            </td>
                                                            <td class="tdName tdNameHover   " onclick="window.location='classDetails/{{$class_->id}}'">
                                                                {{ __($class_['className'])}}
                                                            </td>
                                                            <td class="tdEmail" style=" max-width: 300px;">
                                                                {{ __($class_['classDesc'])}}
                                                            </td>
                                                            <td class="tdPhoneNo tdNameHover" style="text-align: left;" onclick="window.location='userProfile/{{$class_->user->id}}'">
                                                                {{ __($class_->user->name)}}
                                                            </td>
                                                            {{-- <td class="tdPhoneNo" style="text-align: left;">
                                                                {{ __($class_->user->name)}}
                                                            </td> --}}
                                                            <td class="tdRole" style="text-align: center">
                                                                @php
                                                                    $currentTimeDate = Date('d-m-Y H:i');
                                                                    $sessionCounter = 0;
                                                                    foreach($class_->session as $session1){
                                                                        $sessionTimeDate = Date($session1['sessionDate'] . $session1['sessionTime']);
                                                                        $sessionTimeDate = Date('d-m-Y H:i', strtotime($sessionTimeDate));
                                                                        if(Date(strtotime($currentTimeDate)) < Date(strtotime($sessionTimeDate))){
                                                                            $sessionCounter ++;
                                                                        }
                                                                    }
                                                                @endphp
                                                                {{$sessionCounter}}
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                                <tr id="trNoResult" class="trBorderLeftRight" style="display: none; ">
                                                    <td colspan="5" style="text-align:center">
                                                        No class matched you filter...
                                                    </td>
                                                </tr>  
                                            @endif
                                        </table>
                                        <div id="pagingDiv" style="border:none;">
                                            <span id="pagingSpan" style="font-size:"></span>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            {{-- ---------------------------------------------------EDIT MODDALS--------------------------------------------------- --}}
            @if(Auth::user()->userRole == "Admin")
                <div id="editModal" class="modal" style="border: none;">
                    <div class="row justify-content-center" id="editModal2" >
                        <div class="col-md-8 row justify-content-center" id="editModal3" style="border: none; text-align:center" >
                            <div class="card" id="editCard" style="border: none; width: 600">
                                <div class="card-header" style="background-color:#a10c25; color:white; width:100%"><b>{{ __('Create new class') }}</b></div>
            
                                <div class="card-body">
                                    <form method="POST" action="{{ route('createClass') }}">
                                        @csrf
                                        <div class="form-group row">
                                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                            <div class="col-md-6">
                                                <input id="className" type="text" class="form-control @error('className') is-invalid @enderror" name="className" value="{{ old('className') }}" required autocomplete="className" autofocus>
                                                @error('className')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="classDesc" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>
                                            <div class="col-md-6">
                                                <textarea name="classDesc" class="form-control" value="{{ old('classDesc') }}">{{ old('classDesc') }}</textarea>
                                                @error('classDesc')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row">
                                            <label for="classType" class="col-md-4 col-form-label text-md-right">{{ __('Type') }}</label>
                                            <div class="col-md-6">
                                                <select name="classType" class="form-control" id="inputClassType" onchange="inputSelectTypeChanged()" required>
                                                    <option {{ old('classType') == "Normal" ? 'selected' : '' }} value="Normal">Normal</option>
                                                    <option {{ old('classType') == "Special" ? 'selected' : '' }} value="Special">Special</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row" id="inputClassDateStart" style="display: none">
                                            <label for="classDateStart" class="col-md-4 col-form-label text-md-right">{{ __('Start') }}</label>
                                            <div class="col-md-6">
                                                <input id="classDateStart" type="date" class="datepicker1 form-control @error('classDateStart') is-invalid @enderror" name="classDateStart" value="{{ old('classDateStart')}}" min="{{ now()->toDateString('Y-m-d') }}" onchange="classDateStartChanged()" autocomplete="classDateStart">
                                                @error('classDateStart')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row" id="inputClassDateEnd" style="display: none">
                                            <label for="classDateEnd" class="col-md-4 col-form-label text-md-right">{{ __('End') }}</label>
                                            <div class="col-md-6">
                                                <input id="classDateEnd" type="date" class="datepicker1 form-control @error('classDateEnd') is-invalid @enderror" name="classDateEnd" value="{{ old('classDateEnd')}}" min="{{ now()->toDateString('Y-m-d') }}" autocomplete="classDateEnd">
                                                @error('classDateEnd')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row" id="inputClassCost" style="display: none">
                                            <label for="classCost" class="col-md-4 col-form-label text-md-right">{{ __('Cost') }}</label>
                                            <div class="col-md-6">
                                                <input id="classCost" type="number" class="form-control @error('classCost') is-invalid @enderror" name="classCost" value="{{ old('classCost') }}" autocomplete="classCost" min="1">
                                                @error('classCost')
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $message }}</strong>
                                                    </span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Teacher') }}</label>
                                            <div class="col-md-6" style="border:blue none; padding-bottom:0">
                                                <select class="form-controla" name="teacherID" style="width: 100%; max-height:100%; height:100%; padding-top:0;" required>
                                                    @if(Request::old('teacherID') != NULL)
                                                        @foreach($teachers as $teacher)
                                                            @if($teacher['id'] == Request::old('teacherID'))
                                                                {{$oldTeacherName = $teacher['name']}}
                                                                {{$oldTeacherPhone = $teacher['userPhone']}}
                                                            @endif
                                                        @endforeach
                                                        <option value="{{Request::old('teacherID')}}">{{$oldTeacherName}} {{$oldTeacherPhone}}</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row mb-0 justify-content-center" style="text-align: center;">
                                            <input type="hidden" name="adminReg" value="1">
                                            <button type="submit" class="btnReg" style="width: 100">
                                                {{ __('Submit') }}
                                            </button>
                                            &nbsp &nbsp
                                            <button type="reset" class="buttonReject" style="width: 100" onclick="eventCloseEditFunction()">
                                                {{ __('Cancel') }}
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            {{-- ---------------------------------------------------EDIT MODDALS--------------------------------------------------- --}}
        @endif

        @if(isset($deleteSuccess))
            <span id="deleteSuccess" style="display: none">{{$deleteSuccess}}</span>
            <span id="deletedClass" style="display: none">{{$deletedClass['className']}}</span>
        @endif
        @if(isset($approvedUsersCounter))
            <span id="approvedUsersCounter" style="display: none">{{$approvedUsersCounter}}</span>
        @endif
        @if(isset($rejectedUsersCounter))
            <span id="rejectedUsersCounter" style="display: none">{{$rejectedUsersCounter}}</span>
        @endif
        @if(session()->get('addedClass') != null)
            <span id="addedClass" style="display: none">Successfully Added : {{session()->get('addedClass')}}</span>
        @endif

        <script>
            window.onload = function(){ //run these after page loaded
                if(document.getElementById("myInput")!=null){
                    myFunction();
                }
                if(document.getElementById("myInput2")!=null){
                    myFunction2();
                }
                // selectFunction();

                if(document.getElementsByClassName('invalid-feedback')[0]!= null)
                {
                    var editModal = document.getElementById("editModal");
                    var addUserButton = document.getElementById("addUserButton");
                    addUserButton.scrollIntoView();
                    editModalShow();                    
                }
                if(document.getElementById('deleteSuccess') != null){
                    setTimeout(function userDeletedAlert(){
                        alert("Successfully Deleted : " + document.getElementById('deletedClass').innerHTML);
                    },500);
                }
                if(document.getElementById('addedClass') != null){
                    setTimeout(function userRegisteredAlert(){
                        alert(document.getElementById('addedClass').innerHTML);
                    },500);
                }
                if(document.getElementById('approvedUsersCounter') != null){
                    setTimeout(function usersApprovedAlert(){
                        alert("Successfully Approved " + document.getElementById('approvedUsersCounter').innerHTML + " user(s)");
                    },500);
                }
                if(document.getElementById('rejectedUsersCounter') != null){
                    setTimeout(function usersRejectedAlert(){
                        alert("Successfully Rejected " + document.getElementById('rejectedUsersCounter').innerHTML + " user(s)");
                    },500);
                }

                inputSelectTypeChanged();
                classDateStartChanged();
            };
            
            EnableSubmit = function(val)
            {
                var counter = 0;
                var counter1 = 0;
                var sbmt = document.getElementById("abc");
                var sbmt1 = document.getElementById("ghi");

                var superCheckBox = document.getElementById('superCheckBox');
                var checkBoxSelect = document.getElementsByClassName('checkBoxSelect');

                for(i = 0; i < checkBoxSelect.length; i++){
                    if(checkBoxSelect[i].parentElement.parentElement.style.display=="" && checkBoxSelect[i].checked == true){
                        counter++;
                    }
                }
                if(counter>0){
                    sbmt.disabled = false;
                    sbmt1.disabled = false;
                }else{
                    sbmt.disabled = true;
                    sbmt1.disabled = true;
                    superCheckBox.checked = false;
                }
            };
            
            EnableSubmit2 = function(val2)
            {
                var counter2 = 0;
                var counter3 = 0;
                var sbmt2 = document.getElementById("mno");
                
                if (val2.checked == true)
                {   
                    counter2 = counter2 + 1;
                }
                else
                {
                    counter2 = counter2 - 1; 
                }

                if (counter2 > 0)
                {   
                    sbmt2.disabled = false;
                }
                else
                {
                    sbmt2.disabled = true;
                }

                var sbmt3 = document.getElementById("pqr");
                
                if (val2.checked == true)
                {   
                    counter3 = counter3 + 1;
                }
                else
                {
                    counter3 = counter3 - 1; 
                }

                if (counter3 > 0)
                {   
                    sbmt3.disabled = false;
                }
                else
                {
                    sbmt3.disabled = true;
                }

            };

            // *********** user list filter + paging script starts here ***************
            var currentPage = 1; //default current page
            var showEntries = 10; //default show entries
            var checkRegSuccess = 0;

            function myFunctionReset(){
                currentPage = 1; //reset current page position
                showEntries = document.getElementById("inputShowEntries").value;
                
                myFunction();
                var pagingSpan = document.getElementById('pagingSpan');
                if(showEntries <= 20){
                    pagingSpan.scrollIntoView(); // scroll the page make sure user can see full table witouth scrolling
                }
            };
            
            function myFunction() {
                var input, filter, table, tr, td, tdState, tdNo, tdRole, tdPhoneNo, i, txtValue, txtValueState, inputState, filterState, inputRole, txtValueRole, inputPhoneNo, filterPhoneNo, txtValuePhoneNo, pagingSpan, counterTr = 0, counterPageButton = 0, oldPagingButton = 0;
                input = document.getElementById("myInput");
                inputState = document.getElementById("myInputEmail");
                inputRole = document.getElementById("myInputRole");
                inputPhoneNo = document.getElementById("myInputPhoneNo");
                filter = input.value.toUpperCase();
                filterState = inputState.value.toUpperCase();
                // filterRole = inputRole.value.toUpperCase();
                filterPhoneNo = inputPhoneNo.value.toUpperCase();
                table = document.getElementById("myTable");
                tr = table.getElementsByTagName("tr");
                for (i = 0; i < tr.length; i++) {
                    tdNo = tr[i].getElementsByClassName("tdNo")[0];
                    td = tr[i].getElementsByClassName("tdName")[0];
                    tdState = tr[i].getElementsByClassName("tdEmail")[0];
                    tdRole = tr[i].getElementsByClassName("tdRole")[0];
                    tdPhoneNo = tr[i].getElementsByClassName("tdPhoneNo")[0];
                    if (td && tdState && tdRole && tdPhoneNo) {
                        txtValue = td.textContent || td.innerText;
                        txtValueState = tdState.textContent || tdState.innerText;
                        txtValueRole = tdRole.textContent || tdRole.innerText;
                        txtValuePhoneNo = tdPhoneNo.textContent || tdPhoneNo.innerText;
                        // if(inputRole.value.toUpperCase() == "ALL"){
                        if (txtValue.toUpperCase().indexOf(filter) > -1 && txtValueState.toUpperCase().indexOf(filterState) > -1 && txtValuePhoneNo.toUpperCase().indexOf(filterPhoneNo) > -1) {
                            if((counterTr >= (currentPage-1)*showEntries) && ((counterTr+1) <= (currentPage*showEntries))){
                                tr[i].style.display = "";
                                tdNo.innerHTML=counterTr+1;
                            }else{
                                tr[i].style.display = "none";
                            }
                            counterTr++;
                        }else{
                            tr[i].style.display = "none";
                        }
                        // }
                        // else{
                            // if (txtValue.toUpperCase().indexOf(filter) > -1 && txtValueState.toUpperCase().indexOf(filterState) > -1 && txtValueRole.toUpperCase().indexOf(filterRole) > -1 && txtValuePhoneNo.toUpperCase().indexOf(filterPhoneNo) > -1) {
                            //     if((counterTr >= (currentPage-1)*showEntries) && ((counterTr+1) <= (currentPage*showEntries))){
                            //         tr[i].style.display = "";
                            //         tdNo.innerHTML=counterTr+1;
                            //     }else{
                            //         tr[i].style.display = "none";
                            //     }
                            //     counterTr++;
                            // }else{
                            //     tr[i].style.display = "none";
                            // }
                        // }
                    }
                }

                pagingSpan = document.getElementById("pagingSpan");
                pagingSpan.innerHTML = "";


                if((document.getElementById('regSuccess') != null) && (checkRegSuccess == 0)){
                        currentPage = Math.ceil(counterTr/showEntries);
                        checkRegSuccess++;
                        myFunction();
                        pagingSpan.scrollIntoView();
                }

                var trNoResult = document.getElementById('trNoResult');


                if(counterTr == 0){
                    trNoResult.style.display = "";
                }else{
                    trNoResult.style.display = "none";
                }

                if(counterTr/showEntries > 1){
                    pagingSpan.innerHTML = "Page : ";
                    for(i = 1; i <= Math.ceil(counterTr/showEntries); i++){
                        var pagingButton = document.createElement('button');
                        if(i == currentPage){
                            pagingButton.className = "buttonPagingCurrent";
                            pagingButton.disabled = true;
                        }
                        else{
                            pagingButton.className = "buttonPaging";
                        }
                        pagingButton.type = "button";
                        pagingButton.addEventListener('click', pageNumberClicked);
                        pagingButton.value = i;
                        pagingButton.innerHTML = i;
                        pagingSpan.appendChild(pagingButton); 
                        counterPageButton++;
                    }
                }
            };

            function pageNumberClicked(e){
                currentPage = e.target.value;
                myFunction();
                var pagingSpan = document.getElementById('pagingSpan');
                var addUserButton = document.getElementById('addUserButton');
                if(showEntries <= 20){
                    pagingSpan.scrollIntoView(); // scroll the page make sure user can see full table witouth scrolling
                }else{
                    addUserButton.scrollIntoView();
                }
            };
            
            // ************ user list filter + paging ends here****************

            var currentPage2 = 1; //default current page
            var showEntries2 = 10; //default show entries
            var checkRegSuccess2 = 0;

            function myFunctionReset2(){
                currentPage2 = 1; //reset current page position
                showEntries2 = document.getElementById("inputShowEntries2").value;
                
                myFunction2();
                var pagingSpan = document.getElementById('pagingSpan2');
                // if(showEntries <= 20){
                //     pagingSpan.scrollIntoView(); // scroll the page make sure user can see full table witouth scrolling
                // }
            };


            function myFunction2() {
                var input, filter, table, tr, td, tdState, tdNo, tdRole, tdPhoneNo, i, txtValue, txtValueState, inputState, filterState, inputRole, txtValueRole, inputPhoneNo, filterPhoneNo, txtValuePhoneNo, pagingSpan, counterTr = 0, counterPageButton = 0, oldPagingButton = 0;
                input = document.getElementById("myInput2");
                inputState = document.getElementById("myInputEmail2");
                inputRole = document.getElementById("myInputRole2");
                inputPhoneNo = document.getElementById("myInputPhoneNo2");
                filter = input.value.toUpperCase();
                filterState = inputState.value.toUpperCase();
                // filterRole = inputRole.value.toUpperCase();
                filterPhoneNo = inputPhoneNo.value.toUpperCase();
                table = document.getElementById("myTable2");
                tr = table.getElementsByTagName("tr");
                for (i = 0; i < tr.length; i++) {
                    tdNo = tr[i].getElementsByClassName("tdNo2")[0];
                    td = tr[i].getElementsByClassName("tdName2")[0];
                    tdState = tr[i].getElementsByClassName("tdEmail2")[0];
                    tdRole = tr[i].getElementsByClassName("tdRole2")[0];
                    tdPhoneNo = tr[i].getElementsByClassName("tdPhoneNo2")[0];
                    if (td && tdState && tdRole && tdPhoneNo) {
                        txtValue = td.textContent || td.innerText;
                        txtValueState = tdState.textContent || tdState.innerText;
                        txtValueRole = tdRole.textContent || tdRole.innerText;
                        txtValuePhoneNo = tdPhoneNo.textContent || tdPhoneNo.innerText;
                        if (txtValue.toUpperCase().indexOf(filter) > -1 && txtValueState.toUpperCase().indexOf(filterState) > -1 && txtValuePhoneNo.toUpperCase().indexOf(filterPhoneNo) > -1) {
                            if((counterTr >= (currentPage2-1)*showEntries2) && ((counterTr+1) <= (currentPage2*showEntries2))){
                                tr[i].style.display = "";
                                tdNo.innerHTML=counterTr+1;
                            }else{
                                tr[i].style.display = "none";
                            }
                            counterTr++;
                        }else{
                            tr[i].style.display = "none";
                        }
                    }
                }

                pagingSpan = document.getElementById("pagingSpan2");
                pagingSpan.innerHTML = "";

                var trNoResult = document.getElementById('trNoResult2');

                if((document.getElementById('regSuccess2') != null) && (checkRegSuccess2 == 0)){
                        currentPage2 = Math.ceil(counterTr/showEntries2);
                        checkRegSuccess2++;
                        myFunction();
                        pagingSpan.scrollIntoView();
                }

                if(counterTr == 0){
                    trNoResult.style.display = "";
                }else{
                    trNoResult.style.display = "none";
                }

                if(counterTr/showEntries2 > 1){
                    pagingSpan.innerHTML = "Page : ";
                    for(i = 1; i <= Math.ceil(counterTr/showEntries2); i++){
                        var pagingButton = document.createElement('button');
                        if(i == currentPage2){
                            pagingButton.className = "buttonPagingCurrent";
                            pagingButton.disabled = true;
                        }
                        else{
                            pagingButton.className = "buttonPaging";
                        }
                        pagingButton.type = "button";
                        pagingButton.addEventListener('click', pageNumberClicked2);
                        pagingButton.value = i;
                        pagingButton.innerHTML = i;
                        pagingSpan.appendChild(pagingButton); 
                        counterPageButton++;
                    }
                }
            };

            function pageNumberClicked2(e){
                currentPage2 = e.target.value;
                myFunction2();
                var pagingSpan = document.getElementById('pagingSpan2');
                var addUserButton = document.getElementById('addUserButton2');
                // if(showEntries2 <= 20){
                //     pagingSpan.scrollIntoView(); // scroll the page make sure user can see full table witouth scrolling
                // }else{
                //     addUserButton.scrollIntoView();
                // }
            };

            var editModal = document.getElementById("editModal");
            var editModal2 = document.getElementById("editModal2");
            var editModal3 = document.getElementById("editModal3");

            var disabledButton = document.getElementById("buttonDisabled");
            var editCard = document.getElementById("editCard");

            function eventCloseEditFunction() {
                const body = document.body;
                body.style.overflowY = '';
                editModal.style.display = "none";
            };

            function editModalShow() {
                editModal.style.display = "block";
                editModal.style.overflowY = "";
                const body = document.body;
                body.style.overflowY = 'hidden';
            };

            document.addEventListener("click", function(e)
            {
                if ((e.target==editModal || e.target==editModal2 || e.target==editModal3)) 
                {
                    eventCloseEditFunction();
                }
            });

            var br = document.createElement("br");

            $('.form-controla').select2({
                language: {
                    noResults: function (params) {
                        if(document.getElementsByClassName('select2-search__field')[0].value == ""){
                            return "Enter a name";
                        }else{
                            return "That's a miss.";
                        }
                    }   
                },
                minimumInputLength: 3,
                placeholder: 'Select a teacher',
                ajax: {
                    url: '{{url("/ajax-autocomplete-search")}}',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data) {
                        if(document.getElementsByClassName('select2-search__field')[0].value != ""){
                            return {
                                results: $.map(data, function (item) {
                                    return {
                                        text: item.name + " (" + item.userPhone +")",
                                        id: item.id,
                                        value:item.id
                                    }
                                })
                            };
                        }else{
                            return {
                                results: $.map(data, function (item) {
                                    return null
                                })
                            };
                        }
                    },
                    cache: true,
                    // formatResult: formatDesign,
                    // formatSelection: formatDesign,
                }
            });

            var inputClassType = document.getElementById("inputClassType");
            function inputSelectTypeChanged(){
                if(inputClassType.value == "Special"){
                    document.getElementById("inputClassCost").style.display = "";
                    document.getElementById("inputClassDateStart").style.display = "";
                    document.getElementById("inputClassDateEnd").style.display = "";
                    document.getElementById("inputClassCost").required = true;
                    document.getElementById("classDateStart").required = true;
                    document.getElementById("classDateEnd").required = true;
                    document.getElementById("classDateEnd").disabled = true;

                }else{
                    document.getElementById("inputClassCost").style.display = "None";
                    document.getElementById("inputClassDateStart").style.display = "None";
                    document.getElementById("inputClassDateEnd").style.display = "None";
                    document.getElementById("inputClassCost").required = false;
                    document.getElementById("classDateStart").required = false;
                    document.getElementById("classDateEnd").required = false;
                    document.getElementById("classDateEnd").disabled = false;
                }
            }

            function classDateStartChanged(){
                var classDateStart =  document.getElementById("classDateStart");
                var classDateEnd = document.getElementById("classDateEnd");
                classDateEnd.disabled = false;
                classDateEnd.min = classDateStart.value;
            }
        </script>
    @endsection
@endguest

