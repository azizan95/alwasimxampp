<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ ('Al - Wasim') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}" defer></script> -->

    <!-- Styles -->
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    {{-- *************css for select2************ --}}
    {{-- <link href="{{ asset('css/select2.min.css')}}" rel="stylesheet" />
    <script src="{{ asset('js/select2.min.js')}}"></script> --}}
    {{-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.0-alpha1/css/bootstrap.min.css"> --}}
    {{-- <link rel="stylesheet" type="text/css" href="{{ asset('css/select2/bootstrapSelect2.min.css')}}"> --}}
    {{-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> --}}
    <script src="/css/select2/select2proper/jquery/dist/jquery.js"></script>
    {{-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" /> --}}
    <link href="/css/select2/select2proper/css/select2.min.css" rel="stylesheet" type="text/css" />
    {{-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script> --}}
    <script src="/css/select2/select2.min.js"></script>
    {{-- node_modules\jquery\dist\jquery.js
    D:\wamp64\www\fyp2021\alwasim\node_modules\jquery\dist\jquery.js --}}
    {{-- *************css for select2************ --}}

    {{-- *************css for datepicker************ --}}
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet"> --}}
    {{-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> --}}
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script> --}}
    {{-- *************css for datepicker************ --}}
        <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light shadow-sm " style="background-color:#ffcd04">
            <div class="container">
            @guest
                <a class="navbar-brand" href="{{ url('/') }}" style="">
            @else    
                <a class="navbar-brand" href="{{ url('/home') }}" style="color:rgb(0, 0, 0)">
            @endguest
                <img class="img-fluid rounded mb-4 mb-lg-0" src="/images/icons/alwasimbigwhitered.png" width="100">
                </a>
                {{-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button> --}}

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if(Route::current()->getName() == 'register')
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}" style="color:black">{{ __('Login') }}</a>
                                </li>
                            @endif
                            
                            @if(Route::current()->getName() == 'login')
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}" style="color:black">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:rgb(0, 0, 0)" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    
                                    @if(Auth::user()->userRole == "Admin")
                                        <a class="dropdown-item" href="{{ url('/userManagement') }}">
                                        <!-- <a class="dropdown-item" href="{{ url('/userlist') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> -->
                                            {{ __('User Management') }}
                                        </a>
                                        <a class="dropdown-item" href="{{ url('/classManagement') }}">
                                            <!-- <a class="dropdown-item" href="{{ url('/userlist') }}"onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> -->
                                            {{ __('Class Management') }}
                                        </a>
                                        <a class="dropdown-item" href="{{ url('/creditManagement') }}">
                                            <!-- <a class="dropdown-item" href="{{ url('/userlist') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> -->
                                            {{ __('Credit Management') }}
                                        </a>
                                    @else
                                        <a class="dropdown-item" href="{{ url('/classManagement') }}">
                                            {{ __('Class Management') }}
                                        </a>
                                        <a class="dropdown-item" href="{{url('userProfile/'.Auth::user()->id)}}">
                                            {{ __('My Profile') }}
                                        </a>
                                    @endif
                                    {{-- <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="return confirm('Sure logout?');event.preventDefault(); document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a> --}}
                                    <a class="dropdown-item" href="{{ url('/index/logout') }}"
                                       onclick="return confirm('Sure logout?')">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4" style="">
            @yield('content')
        </main>
    </div>
</body>
</html>
