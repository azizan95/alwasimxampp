<style>
    .tdSessionDetails{ 
        cursor: pointer;
    }
    
    table {
        border-collapse: collapse;
        width: 100%;
    }
    
    th, td {
        padding: 8px;
        text-align: left;
        border-top: 1px solid black;
    }

    tr:nth-child(odd) {
        background-color: #f2f2f2;
    }

    th {
        text-align: center;
        font-weight: normal;
        font-size: 15;
        background-color: #242d5f;
        color: white;
    }

    .buttonCleared:disabled {
        text-align: center;
        background-color: #4CAF50; 
        color: white; 
        border: 2px solid #409143;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .button1:enabled {
        background-color: white; 
        color: black; 
        border: 2px solid #4CAF50;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .button1:hover {
        background-color: #4CAF50;
        color: white;
        }
    
    .button1:disabled{
        background-color: white;
        color: black;
        border: 2px solid #4CAF50;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px;
        /* background-color: #ffffff;
        color: grey; 
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px */
    }

    .buttonNoInfo:disabled {
        background-color: #a10c25; 
        color: white; 
        border: 2px solid #8a0b20;
        border-radius: 4px;
        /* width: 75px; */
        margin-top: 5px;
        margin-right: 0px;
        text-align: center;
    }

    .buttonReject:enabled {
        background-color: white; 
        color: black; 
        border: 2px solid #a10c25;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonReject:hover {
        background-color: #a10c25;
        color: white;
        }
    
    .buttonReject:disabled{
        background-color: #ffffff;
        color: grey; 
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .nostyle{
        -webkit-appearance: none; 
        border:1px solid;
       }

    .modal {
        display: block; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    .modal-content {
        /* background-color: #fefefe; */
        background-color: #3D3D3D;
        text-align: center;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        /* width: 10%; */
    } 

    .tdNoStyle {
    /* margin: 0; */
        padding: 0;
        border-top: none;
        border-left: none;
        border-right: none;
        outline: 0;
        font-size: 100%;
        vertical-align: baseline;
        background-color: #a10c25;
    }
    .tdNoStyle2 {
    /* margin: 0; */
        padding: 0;
        border-top: none;
        border-left: none;
        border-right: none;
        outline: 0;
        font-size: 100%;
        vertical-align: baseline;
        background-color: #ffffff;
    }

    .trBorderLeftRight{
        border-left: 1px solid black;
        border-right: 1px solid black;
        border-bottom: 1px solid black;
    }
    
    .livesearchA{
        all: unset;
    }   
</style>



@guest
please login

@else
    @extends('layouts.app')
    @section('content')

    @if(Auth::user()->userStatus == 0)
        {{ Session::flush() }}
        <script>
            window.alert("Your account is yet to be approved. Please wait for approval email from Admin");
            window.location = "/";
        </script>
    @else
        {{-- @if(Auth::user()->userRole != "Admin")
            <script>
                window.location ='{{ url("userProfile/".Auth::user()->id) }}';
            </script>
        @endif --}}
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="background-color: #a10c25; color:white; font-size:20; padding-bottom:0; height:50px; vertical-align:middle">
                            <form name="deleteClassForm" method="POST" action="{{ url('/deleteClass') }}">
                                <input type="hidden" name="classID" value="{{$class_['id']}}">
                                @csrf
                                <table style="border:none; margin-bottom:0; padding:0;">
                                    <tr>
                                        <th class="tdNoStyle" style="font-size:20; vertical-align:middle">
                                            <b>Class</b>
                                        </th>
                                        @if((Auth::user()->userRole == "Admin") || (Auth::user()->id == $class_->user->id))
                                            <th class="tdNoStyle" style="text-align: right; padding:0; vertical-align:middle">
                                                <button type="button" style="background-color:transparent; box-shadow: 0px 0px 0px transparent; border: none; text-shadow: 0px 0px 0px transparent; height:30; padding:0 " onclick="editModalShow()" >
                                                    <img class="deletebutton1" src="/images/icons/editblue.png" style="width: 25" title="Edit user"/>
                                                </button>
                                                @if(Auth::user()->userRole == "Admin")
                                                    <button style="background-color:transparent; box-shadow: 0px 0px 0px transparent; border: none; text-shadow: 0px 0px 0px transparent; height:30; padding:0; margin-right: 2" onclick="return confirm('Confirm delete?')" >
                                                        <img class="deletebutton2" src="/images/icons/deletered.png" style="width: 25" title="Delete user" />
                                                    </button>
                                                @endif
                                            </th>
                                        @endif
                                    </tr>
                                </table>
                            </form>
                        </div>
                        <div class="card-body" style="text-align: right">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <table id="" style="border-bottom: 1px solid black;width:100%">
                                @if(Auth::user()->userRole == "Student")
                                    <tr style="border-top:1px solid none">
                                        <td class="tdNoStyle2" style="border-top:none; padding-top:0" colspan="5">
                                            <form id="EnrollForm" method="POST" action="{{ url('/enrollClass') }}">
                                                @csrf
                                                <input type="hidden" name="classID" value="{{$class_['id']}}">
                                                <input type="hidden" name="studentID" value="{{Auth::user()->id}}">
                                            </form>
                                            <form id="LeaveForm" method="POST" action="{{ url('/leaveClass') }}">
                                                @csrf
                                                <input type="hidden" name="classID" value="{{$class_['id']}}">
                                                <input type="hidden" name="studentID" value="{{Auth::user()->id}}">
                                            </form>
                                            @if(Auth::user()->enrolledIn()->where('class__id', $class_['id'])->count() < 1)
                                                <button form="EnrollForm" class="button1" style="width: auto; margin-bottom:2; margin-top:0;" onclick="return confirm('Confirm join class?')">Join Class</button>
                                            @else
                                                <button form="LeaveForm" class="buttonReject" style="width: auto; margin-bottom:2; margin-top:0;" onclick="return confirm('Confirm leave class?\nAll sessions you\'ve booked in this class will be cancelled.')">Leave Class</button>
                                            @endif
                                        </td>
                                    </tr>
                                @endif
                                <tr style="font-weight: bold" class="trBorderLeftRight">
                                    <th style="width: 10%; font-weight: bold">
                                        ID
                                    </th>
                                    <th colspan="2" style="font-weight: bold">
                                        Name
                                    </th>
                                    <th colspan="" style="font-weight: bold">
                                    </th>
                                    <th colspan="" style="font-weight: bold">
                                        Teacher
                                    </th>
                                </tr>
                                <tr class="trBorderLeftRight">
                                    <td>
                                        {{ __($class_['id'])}}
                                    </td>
                                    <td colspan="2">
                                        {{ __($class_['className'])}}
                                    </td>
                                    <td colspan="">
                                    </td>
                                    <td colspan="">
                                        <a style="text-decoration: none; color:black" href="{{url('userProfile/'.$class_->user->id)}}">{{ __($class_->user->name)}}</a>
                                    </td>
                                </tr>
                                <tr class="trBorderLeftRight">
                                    <th colspan="5" style="font-weight: bold">
                                        Description
                                    </th>
                                    {{-- <th style="font-weight: bold">
                                        Cost
                                    </th> --}}
                                </tr>
                                <tr class="trBorderLeftRight">
                                    <td colspan="5" style="max-width: 1px;">
                                        {{ __($class_['classDesc'])}}
                                    </td>
                                    {{-- <td>
                                        {{ __($class_['classCost'])}}
                                    </td> --}}
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="background-color: #a10c25; color:white; font-size:20; padding-bottom:0; height:50px; vertical-align:middle">
                            <table style="border:none; margin-bottom:0; padding:0;">
                                <tr>
                                    <th class="tdNoStyle" style="font-size:20; vertical-align:middle;">
                                        <b>Upcoming Sessions</b>
                                    </th>
                                    @if((Auth::User()->userRole == "Admin" || Auth::User()->userRole == "Student" || Auth::User()->userRole == "Teacher"))
                                        <th class="tdNoStyle" style="text-align: right; padding:0; vertical-align:middle;">
                                            <button form="sessionHistoryForm" style="background-color:transparent; box-shadow: 0px 0px 0px transparent; border: none; text-shadow: 0px 0px 0px transparent; height:30; padding:0;">
                                                <img class="deletebutton2" src="/images/icons/historypurple.png" style="width: 25" title="Session History" />
                                            </button>
                                            <form name="sessionHistoryForm" id="sessionHistoryForm" method="POST" action="{{$class_['id']}}/sessionHistory">
                                            {{-- <form name="sessionHistoryForm" id="sessionHistoryForm" method="POST" action="{{ url('/sessionHistory') }}"> --}}
                                                @csrf
                                                <input type="hidden" name="studentID" value="{{Auth::User()->id}}">
                                            </form>
                                        </th>
                                    @endif
                                </tr>
                            </table>
                        </div>
                        <div class="card-body" style="text-align: right">
                            <form name="ApprovalForm" method="POST" action="{{ url('/approvePayment') }}">
                                @csrf
                                <table id="tableSessionDetails" style="border-bottom: 1px solid black;width:100%">
                                    @if((Auth::user()->userRole == "Admin") || (Auth::user()->id == $class_->user['id']))
                                        <tr style="border-top:none">
                                            <td class="tdNoStyle2" style="border-top:none" colspan="5">
                                                <button type="button" id="addUserButton" value="Create Class" class="button1" style="width: auto; margin-bottom:2" onclick="editModalShow2()">Create Session</button>
                                            </td>
                                        </tr>
                                    @endif
                                    <tr class="trBorderLeftRight">
                                        <th style="font-weight: bold">
                                            No
                                        </th>
                                        <th style="font-weight: bold">
                                            Date
                                        </th>
                                        <th style="font-weight: bold">
                                            Day
                                        </th>
                                        <th style="font-weight: bold">
                                            Time
                                        </th>
                                        <th style="font-weight: bold">
                                            Duration
                                        </th>
                                        <th style="font-weight: bold; text-align:center">
                                            Cost
                                        </th>
                                        <th style="font-weight: bold;text-align: center;">
                                            Capacity
                                        </th>
                                    </tr>
                                    @if($class_->session->count() == 0)
                                        <tr class="trBorderLeftRight">
                                            <td colspan="6" style="text-align: center;">
                                                No session added yet!
                                            </td>
                                        </tr>
                                    @else
                                        @php
                                            $counter = 0;
                                            $currentTimeDate = Date('d-m-Y H:i');
                                            $sessions = $class_->session->sortBy(function($sessions){
                                                return sprintf('%-12s%s', $sessions->sessionDate, $sessions->sessionTime);
                                            });   
                                        @endphp
                                        @foreach($sessions as $session)
                                            @php
                                                $sessionTimeDate = Date($session['sessionDate'] . $session['sessionTime']);
                                                $sessionTimeDate = Date('d-m-Y H:i', strtotime($sessionTimeDate));
                                            @endphp
                                            @if(Date(strtotime($currentTimeDate)) < Date(strtotime($sessionTimeDate)))
                                                <tr class="trBorderLeftRight trSession tdSessionDetails" onclick="window.location='{{$class_['id']}}/sessionDetails/{{$session['id']}}';">
                                                    <td>
                                                        {{++$counter}}
                                                    </td>
                                                    <td class="tdSessionDate tdSessionDetails">
                                                        {{ date('d/m/Y', strtotime($session['sessionDate']))}}
                                                    </td>
                                                    <td class="">
                                                        {{ date('l', strtotime($session['sessionDate']))}}
                                                    </td>
                                                    <td class="tdSessionTime tdSessionDetails">
                                                        {{ date('H:i', strtotime($session['sessionTime']))}}
                                                    </td>
                                                    <td class="tdSessionDetails" style="text-align:;">
                                                        @if($session['sessionDuration'] < 60)
                                                            {{ __($session['sessionDuration'])}} Minutes
                                                        @else
                                                            1 Hour
                                                        @endif
                                                    </td>
                                                    <td class="tdSessionDetails" style="text-align: center;">
                                                        {{$session->sessionCost}} Credit
                                                    </td>
                                                    <td class="tdSessionDetails" style="text-align: center;">
                                                        {{$session->bookedBy->count()}} / {{ __($session['sessionCapacity'])}}
                                                    </td>
                                                </tr>
                                            @endif    
                                        @endforeach
                                        @if($counter == 0)
                                            <tr class="trBorderLeftRight">
                                                <td colspan="6" style="text-align: center;">
                                                    No upcoming session!
                                                </td>
                                            </tr>
                                        @endif
                                    @endif
                                </table>
                                @if(Auth::user()->is_admin == 1)
                                    <input type="hidden" name="classID" value="{{ __($class_['id'])}}">
                                    <span><input type="submit" value="Approve" class="button1" onclick="return confirm('Confirm approve?')" id="mno" disabled>&nbsp<input type="submit" value="Remove" class="buttonReject" formaction="{{ url('/rejectPayment') }}" onclick="return confirm('Confirm reject?')" id="pqr" disabled></span>
                                @endif
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="background-color: #a10c25; color:white; font-size:20"><b>Students</b></div>
                        <div class="card-body" style="text-align: right">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <table style="width:100%">
                                <tr class="nostyle" style="background-color: white; border:none">
                                    <td style="border-right: 1px none black;border-top: none; width:10%">
                                        Name :
                                    </td>
                                    <td style="border-right: 1px none black;border-top: none; width:35%">
                                        <input class="form-control" style="border-radius: 6px; padding-left:10px" type="text" id="studentSearchName" onkeyup="filterStudent()" placeholder="Filter by name.." title="Type in a name">
                                    </td>
                                    <td style="border-right: 1px none black;border-top: none; width:%; text-align:right">
                                        Date :
                                    </td>
                                    <td style="border-right: 1px none black;border-top: none; width:35%">
                                        <input id="studentSearchDate" type="date" class="datepicker1 form-control @error('sessionDate') is-invalid @enderror" name="studentSearchDate" value="{{ old('sessionDate')}}" onchange="filterStudent()" autocomplete="sessionDate" required>
                                    </td>
                                </tr>
                            </table>
                            <table style="border: 1px solid black; width:100%" id="studentListTable">
                                <tr>
                                    <th style="font-weight: bold; width: 5%">
                                        No
                                    </th>
                                    <th style="font-weight: bold">
                                        Name
                                    </th>
                                    <th style="font-weight: bold; width:15%; text-align: center">
                                        Enrolled on
                                    </th>
                                </tr>
                                @if($class_->enrolledBy->count() == 0)
                                    <tr>
                                        <td colspan="3" style="text-align: center;">
                                            No student enrolled yet!
                                        </td>
                                    </tr>
                                @else
                                    @php
                                        $counter = 1;
                                    @endphp
                                    @foreach($class_->enrolledBy as $student)
                                    <tr class="trStudentList">
                                        <td class="tdNo" >
                                        </td>
                                        <td class="tdStudentName">
                                            {{ __($student['name'])}}
                                        </td>
                                        <td class="tdEnrollDate">
                                            {{date('d/m/Y', strtotime($student->pivot->created_at))}}
                                        </td>
                                    @endforeach
                                    <tr id="trNoResult" class="trBorderLeftRight" style="display: none; ">
                                        <td colspan="3" style="text-align:center">
                                            No student matched you filter...
                                        </td>
                                    </tr>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        @if((Auth::user()->userRole == "Admin") || (Auth::user()->id == $class_->user['id']))
            <div id="editModal" class="modal" style="border: none;">
                <div class="row justify-content-center" id="editModal2" >
                    <div class="col-md-8 row justify-content-center" id="editModal3" style="border: none; text-align:center" >
                        <div class="card" id="editCard" style="border: none; width: 60%">
                            <div class="card-header" style="background-color:#a10c25; color:white; width:100%">
                                <b>{{ __('Edit Class Details') }}</b>
                            </div>
                            <div class="card-body">
                                <form method="POST" action="{{ route('editClass') }}">
                                    @csrf
                                    <div class="form-group row">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                        <div class="col-md-6">
                                            <input id="className" type="text" class="form-control @error('className') is-invalid @enderror" name="className" value="{{ old('className') == "" ? $class_['className'] : old('className') }}" maxlength="50" required autocomplete="className" autofocus>
                                            @error('className')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="classDesc" class="col-md-4 col-form-label text-md-right">
                                            {{ __('Description') }}
                                        </label>
                                        <div class="col-md-6">
                                            <textarea name="classDesc" class="form-control" value="{{ old('classDesc') == "" ? $class_['classDesc'] : old('classDesc') }}" rows=5>{{ old('classDesc') == "" ? $class_['classDesc'] : old('classDesc') }}</textarea>
                                            @error('classDesc')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    {{-- <div class="form-group row">
                                        <label for="classCost" class="col-md-4 col-form-label text-md-right">{{ __('Cost') }}</label>
                                        <div class="col-md-6">
                                            <input id="classCost" type="number" class="form-control @error('classCost') is-invalid @enderror" name="classCost" value="{{ old('classCost') == "" ? $class_['classCost'] : old('classCost') }}" required autocomplete="classCost" autofocus>
                                            @error('classCost')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div> --}}
                                    <div class="form-group row">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Teacher') }}</label>
                                        
                                        <div class="col-md-6" style="border:blue none; padding-bottom:0">
                                            @if(Auth::user()->userRole == "Admin")
                                                <select class="form-controla" name="teacherID" style="width: 100%; max-height:100%; height:100%; padding-top:0;">
                                                    @if(Request::old('teacherID') != NULL)
                                                    @foreach($teachers as $teacher)
                                                        @if($teacher['id'] == Request::old('teacherID'))
                                                            {{$oldTeacherName = $teacher['name']}}
                                                            {{$oldTeacherPhone = $teacher['userPhone']}}
                                                            <option value="{{Request::old('teacherID')}}">{{$oldTeacherName}} {{$oldTeacherPhone}}</option>
                                                        @endif
                                                    @endforeach
                                                @else
                                                    <option value="{{ $class_->user->id }}" selected>{{ $class_->user->name }} ({{$class_->user->userPhone}})</option>
                                                @endif
                                            </select>
                                            @else
                                                {{-- <select class="form-controla" name="teacherID" style="width: 100%; max-height:100%; height:100%; padding-top:0;" disabled> --}}
                                                <input type="hidden" name="teacherID" value="{{$class_->user->id}}">
                                                <select name="" class="form-control" disabled required>
                                                    {{-- <option class="form-control" selected hidden value="{{ old('userRole') == "" ? $user['userRole'] : old('userRole') }}">{{ old('userRole') == "" ? $user['userRole'] : old('userRole') }}</option> --}}
                                                    <option value="{{ $class_->user->id }}" selected>{{ $class_->user->name }} ({{$class_->user->userPhone}})</option>
                                                </select>
                                            @endif
                                                
                                        </div>
                                    </div>
                                    <div class="form-group row mb-0 justify-content-center" style="text-align: center;">
                                        <input type="hidden" name="classID" value="{{$class_['id']}}">
                                        <button type="submit" class="button1" style="width: 100">
                                            {{ __('Submit') }}
                                        </button>
                                        &nbsp &nbsp
                                        <button type="button" class="buttonReject" style="width: 100" onclick="eventCloseEditFunction()">
                                            {{ __('Cancel') }}
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="editModal12" class="modal" style="border: none;">
                <div class="row justify-content-center" id="editModal22" >
                    <div class="col-md-8 row justify-content-center" id="editModal32" style="border: none; text-align:center" >
                        <div class="card" id="editCard2" style="border: none; width: 600">
                            <div class="card-header" style="background-color:#a10c25; color:white; width:100%"><b>{{ __('Create new session') }}</b></div>

                            <div class="card-body">
                                <form method="POST" action="{{ route('createSession') }}">
                                    @csrf
                                    <div class="form-group row">
                                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Class') }}</label>
                                        <div class="col-md-6">
                                            <input type="hidden" name="classID" value="{{$class_['id']}}">
                                            {{-- <input id="inputSessionID" type="hidden" name="sessionID" value="{{$session['id']}}"> --}}
                                            <input id="inputSessionTeacherID" type="hidden" name="teacherID" value="{{$class_->user['id']}}">
                                            <input id="className2" type="text" class="form-control @error('className') is-invalid @enderror" name="className" value="{{$class_['className']}}" disabled autocomplete="className" autofocus>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="sessionDate" class="col-md-4 col-form-label text-md-right">{{ __('Date') }}</label>
                                        <div class="col-md-6">
                                            <input id="inputSessionDate" type="date" class="datepicker1 form-control @error('sessionDate') is-invalid @enderror" name="sessionDate" value="{{ old('sessionDate')}}" onchange="datetimePickerValidate()" autocomplete="sessionDate" required>
                                            @error('sessionDate')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            <span id="invalidDate" class="invalid-feedback-man" role="alert" style="">
                                                <strong>Date cannot before today's</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="sessionTime" class="col-md-4 col-form-label text-md-right">{{ __('Time') }}</label>
                                        <div class="col-md-6" style="border:blue none; padding-bottom:0">
                                            <input id="inputSessionTime" type="time" class="timePicker1 form-control @error('sessionTime') is-invalid @enderror" name="sessionTime" value="{{ old('sessionTime')}}" onchange="datetimePickerValidate()" step="" autocomplete="sessionTime" min="" max="" required autofocus>
                                            {{-- <input id="inputSessionTime" type="time" class="timePicker1 form-control @error('sessionTime') is-invalid @enderror" name="sessionTime" value="{{ old('sessionTime')}}" onchange="datetimePickerValidate()" step="900" autocomplete="sessionTime" min="08:00" max="22:00" required autofocus> --}}
                                            @error('sessionDate')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                            <span id="invalidTime" class="invalid-feedback-man" role="alert" style="">
                                                <strong>Time and date cannot before current time</strong>
                                            </span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="sessionTime" class="col-md-4 col-form-label text-md-right">{{ __('Duration') }}</label>
                                        <div class="col-md-6" style="border:blue none; padding-bottom:0">
                                            <select id="inputSessionDuration" name="sessionDuration" class="form-control" onchange="datetimePickerValidate()">
                                                <option value="15">15 Minutes</option>
                                                <option value="30">30 Minutes</option>
                                                <option value="45">45 Minutes</option>
                                                <option value="60">1 Hour</option>
                                            </select>
                                            @error('sessionDuration')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="sessionCost" class="col-md-4 col-form-label text-md-right">{{ __('Cost') }}</label>
                                        <div class="col-md-6" style="border:blue none; padding-bottom:0">
                                            <input id="sessionCost" type="number" class="form-control @error('sessionCost') is-invalid @enderror" name="sessionCost" value="{{ old('sessionCost')}}" step="1" min="0" autocomplete="sessionCost" required autofocus>
                                            @error('sessionCost')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="sessionCapacity" class="col-md-4 col-form-label text-md-right">{{ __('Capacity') }}</label>
                                        <div class="col-md-6" style="border:blue none; padding-bottom:0">
                                            <input id="sessionCapacity" type="number" class="form-control @error('sessionCapacity') is-invalid @enderror" name="sessionCapacity" value="{{ old('sessionCapacity')}}" step="1" min="1" autocomplete="sessionCapacity" required autofocus>
                                            @error('sessionCapacity')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row justify-content-center">
                                        <span id="invalidDateTime" class="invalid-feedback-man" role="alert" style="">
                                            <strong>Date and Time selected already has a session</strong>
                                        </span>
                                        <span id="invalidCurrentTimeDate" class="invalid-feedback-man" role="alert" style="">
                                            <strong>Date and time cannot before current date and time</strong>
                                        </span>
                                        <span id="invalidCurrentTimeDateTeacher" class="invalid-feedback-man" role="alert" style="">
                                            <strong>Teacher already has a session on the selected date and time</strong>
                                        </span>
                                    </div>
                                    <div class="form-group row mb-0 justify-content-center" style="text-align: center;">
                                        <input type="hidden" name="adminReg" value="1">
                                        <button id="submitButton1" type="submit" class="button1" style="width: 100" onclick="datetimePickerValidate()">
                                            {{ __('Submit') }}
                                        </button>
                                        &nbsp &nbsp
                                        <button type="button" class="buttonReject" style="width: 100" onclick="eventCloseEditFunction2()">
                                            {{ __('Cancel') }}
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    @endif
    {{-- @if(isset($sessionAdded)) --}}
    @if(session()->get('sessionAdded') != null)
        <span id="sessionAdded" style="display: none"></span>
    @endif
    @if(session()->get('deletedSession') != null)
        {{ date('d/m/Y', strtotime($session['sessionDate']))}} ({{ date('l', strtotime($session['sessionDate']))}})
        <span id="deletedSession" style="display: none">{{ date('d/m/Y', strtotime(session()->get('deletedSession')['sessionDate']))}} ({{ date('l', strtotime(session()->get('deletedSession')['sessionDate']))}}), {{ date('H:i', strtotime(session()->get('deletedSession')['sessionTime']))}}</span>
    @endif
    @if(session()->get('classEnrollLeave') != null)
        <span id="classEnrollLeave" style="display: none">{{session()->get('classEnrollLeave')}}</span>
    @endif
    
    <script>
        
        window.onload = function(){ //run these after page loaded

            filterStudent();

            if(document.getElementsByClassName('invalid-feedback')[0]!= null)
            {
                var editModal = document.getElementById("editModal");
                editModalShow();                    
            }
            if(document.getElementById('sessionAdded') != null){
                setTimeout(function sessionAdded(){
                    alert("Successfully add a session");
                },100);
            }
            if(document.getElementById('deletedSession') != null){
                setTimeout(function deletedSession(){
                    alert("Successfully deleted " + document.getElementById('deletedSession').innerHTML + " session.");
                },100);
            }
            if(document.getElementById('classEnrollLeave') != null){
                setTimeout(function classEnrollLeave(){
                    alert(document.getElementById('classEnrollLeave').innerHTML);
                },100);
            }
        };

        function datetimePickerValidate(){
            var inputSessionTeacherID = document.getElementById('inputSessionTeacherID').value;
            var inputSessionDate = document.getElementById('inputSessionDate').value;
            var inputSessionTime = document.getElementById('inputSessionTime').value;
            var hours = inputSessionTime.split(":")[0];
            var minutes = inputSessionTime.split(":")[1];
            var inputSessionDateFull = new Date(document.getElementById('inputSessionDate').value);
            var inputSessionDuration = document.getElementById('inputSessionDuration').value;
            var date = new Date();

            var checkDate = true;
            var checkTime = true;
            var checkTeacher = true;

            if(new Date(inputSessionDateFull.getFullYear(), inputSessionDateFull.getMonth(), inputSessionDateFull.getDate(), 0, 0, 0, 0) < new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0)){
                document.getElementById('inputSessionDate').classList.add('is-invalid');
                checkDate = false;
            }else{
                document.getElementById('inputSessionDate').classList.remove('is-invalid');
                checkDate = true;
            }

            if(new Date(inputSessionDateFull.getFullYear(), inputSessionDateFull.getMonth(), inputSessionDateFull.getDate(), hours, minutes, 0, 0) <= new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), 0, 0)){
                document.getElementById('inputSessionTime').classList.add('is-invalid');
                checkTime = false;
            }else{
                document.getElementById('inputSessionTime').classList.remove('is-invalid');
                checkTime = true;
            }

            if(!checkDate || !checkTime){
                document.getElementById('invalidCurrentTimeDate').style.display = 'block';
                document.getElementById('invalidCurrentTimeDateTeacher').style.display = 'none';
            }else{
                document.getElementById('invalidCurrentTimeDate').style.display = 'none';
                if(inputSessionDate != "" && inputSessionTime != ""){
                    checkTeacher = validateDateTimeTeacher(inputSessionDate, inputSessionTime, inputSessionDuration, inputSessionTeacherID);
                    if(!checkTeacher){
                        document.getElementById('inputSessionDate').classList.add('is-invalid');
                        document.getElementById('inputSessionTime').classList.add('is-invalid');
                        document.getElementById('invalidCurrentTimeDateTeacher').style.display = 'Block';
                    }else{
                        document.getElementById('inputSessionDate').classList.remove('is-invalid');
                        document.getElementById('inputSessionTime').classList.remove('is-invalid');
                        document.getElementById('invalidCurrentTimeDateTeacher').style.display = 'none';
                    }
                }
            }

            if(checkDate && checkTime && checkTeacher){
                document.getElementById('submitButton1').disabled = false;
            }else{
                document.getElementById('submitButton1').disabled = true;
            }
        }

        function validateDateTimeTeacher(inputSessionDate, inputSessionTime, inputSessionDuration, inputSessionTeacherID){
            var check;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'post',
                url: '/ajaxcheckValidDatetime2',
                dataType: 'JSON',
                async:false, //ensure process finish then exit the function
                // data: {
                //     _token: CSRF_TOKEN,
                //     date1: inputSessionDate,
                //     time1: inputSessionTime
                data: {
                    sessionDate : inputSessionDate,
                    sessionTime : inputSessionTime,
                    sessionDuration : inputSessionDuration,
                    teacherID : inputSessionTeacherID,
                },
                success: function (data) {
                    if(data.check){
                        check = false;
                    }else{
                        check = true;
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
            });
            return check;
        }

        var editModal = document.getElementById("editModal");
        var editModal2 = document.getElementById("editModal2");
        var editModal3 = document.getElementById("editModal3");
        var editCard = document.getElementById("editCard");
    
        function eventCloseEditFunction() {
            const body = document.body;
            body.style.overflowY = '';
            editModal.style.display = "none";
        };
    
        function editModalShow() {
            editModal.style.display = "block";
            editModal.style.overflowY = "";
            const body = document.body;
            body.style.overflowY = 'hidden';
        };
    
        document.addEventListener("click", function(e)
        {
            if ((e.target==editModal || e.target==editModal2 || e.target==editModal3)) 
            {
                eventCloseEditFunction();
            }
        });

        var editModal12 = document.getElementById("editModal12");
        var editModal22 = document.getElementById("editModal22");
        var editModal32 = document.getElementById("editModal32");
        var editCard2 = document.getElementById("editCard2");
    
        function eventCloseEditFunction2() {
            const body = document.body;
            body.style.overflowY = '';
            editModal12.style.display = "none";
        };
    
        function editModalShow2() {
            editModal12.style.display = "block";
            editModal12.style.overflowY = "";
            const body = document.body;
            body.style.overflowY = 'hidden';
        };
    
        document.addEventListener("click", function(e)
        {
            if ((e.target==editModal12 || e.target==editModal22 || e.target==editModal32)) 
            {
                eventCloseEditFunction2();
            }
        });
    
        $('.form-controla').select2({
            language: {
                noResults: function (params) {
                    if(document.getElementsByClassName('select2-search__field')[0].value == ""){
                        return "Enter a name";
                    }else{
                        return "That's a miss.";
                    }
                }   
            },
            minimumInputLength: 3,
            placeholder: 'Select a teacher',
            ajax: {
                url: '/ajax-autocomplete-search',
                dataType: 'json',
                delay: 250,
                processResults: function (data) {
                    if(document.getElementsByClassName('select2-search__field')[0].value != ""){
                        return {
                            results: $.map(data, function (item) {
                                return {
                                    text: item.name + " (" + item.userPhone +")",
                                    id: item.id,
                                    value:item.id
                                }
                            })
                        };
                    }else{
                        return {
                            results: $.map(data, function (item) {
                                return null
                            })
                        };
                    }
                },
                cache: true,
            }
        });

        function filterStudent() {
            var studentSearchName, studentListTable, trStudentList, tdStudentName, tdEnrollDate, i, txtTdStudentName, txtTdEnrollDate, studentSearchDate, trCounter = 0;
            studentSearchName = document.getElementById("studentSearchName");
            studentSearchDate = document.getElementById("studentSearchDate");
            if(studentSearchDate.value != ""){
                studentSearchDate = new Date(studentSearchDate.value);
                studentSearchDate = studentSearchDate.getDate() + "/" + (studentSearchDate.getMonth()+1) + "/" + studentSearchDate.getFullYear();
            }else{
                studentSearchDate = "";
            }
            studentSearchName = studentSearchName.value.toUpperCase();
            studentListTable = document.getElementById("studentListTable");
            trStudentList = studentListTable.getElementsByTagName("tr");
            for (i = 0; i < trStudentList.length; i++) {
                tdStudentName = trStudentList[i].getElementsByClassName("tdStudentName")[0];
                tdEnrollDate = trStudentList[i].getElementsByClassName("tdEnrollDate")[0];
                tdNo = trStudentList[i].getElementsByClassName("tdNo")[0];
                if (tdStudentName && tdEnrollDate) {
                    txtTdStudentName = tdStudentName.textContent || tdStudentName.innerText;
                    txtTdEnrollDate = tdEnrollDate.textContent || tdEnrollDate.innerText;
                    if(studentSearchDate.value != ""){
                        var day = txtTdEnrollDate.split("/")[0];
                        var month = txtTdEnrollDate.split("/")[1];
                        var year = txtTdEnrollDate.split("/")[2];
                        txtTdEnrollDate = new Date(year, month, day);
                        txtTdEnrollDate = txtTdEnrollDate.getDate() + "/" + (txtTdEnrollDate.getMonth()) + "/" + txtTdEnrollDate.getFullYear();
                    }
                    if (txtTdStudentName.toUpperCase().indexOf(studentSearchName) > -1 && txtTdEnrollDate.toUpperCase().indexOf(studentSearchDate) > -1) {
                        trStudentList[i].style.display = "";
                        tdNo.innerHTML= ++trCounter;
                    } else {
                        trStudentList[i].style.display = "none";
                    }
                }                 
            }

            var trNoResult = document.getElementById('trNoResult');
            if(trNoResult != null){
                if(trCounter == 0){
                    trNoResult.style.display = "";
                }else{
                    trNoResult.style.display = "none";
                }
            }
        }
    </script>
    @endsection
@endguest



