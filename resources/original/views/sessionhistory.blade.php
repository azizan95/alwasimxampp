<style>
    .tdSessionDetails{ 
        cursor: pointer;
    }
    
    table {
        border-collapse: collapse;
        width: 100%;
    }
    
    th, td {
        padding: 8px;
        text-align: left;
        border-top: 1px solid black;
    }

    tr:nth-child(odd) {
        background-color: #f2f2f2;
    }

    th {
        text-align: center;
        font-weight: normal;
        font-size: 15;
        background-color: #242d5f;
        color: white;
    }

    .buttonCleared:disabled {
        text-align: center;
        background-color: #4CAF50; 
        color: white; 
        border: 2px solid #409143;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .button1:enabled {
        background-color: white; 
        color: black; 
        border: 2px solid #4CAF50;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .button1:hover {
        background-color: #4CAF50;
        color: white;
        }
    
    .button1:disabled{
        background-color: white;
        color: black;
        border: 2px solid #4CAF50;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px;
        /* background-color: #ffffff;
        color: grey; 
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px */
    }

    .buttonNoInfo:disabled {
        background-color: #a10c25; 
        color: white; 
        border: 2px solid #8a0b20;
        border-radius: 4px;
        /* width: 75px; */
        margin-top: 5px;
        margin-right: 0px;
        text-align: center;
    }

    .buttonReject:enabled {
        background-color: white; 
        color: black; 
        border: 2px solid #a10c25;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonReject:hover {
        background-color: #a10c25;
        color: white;
        }
    
    .buttonReject:disabled{
        background-color: #ffffff;
        color: grey; 
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .nostyle{
        -webkit-appearance: none; 
        border:1px solid;
       }

    .modal {
        display: block; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    .modal-content {
        /* background-color: #fefefe; */
        background-color: #3D3D3D;
        text-align: center;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        /* width: 10%; */
    } 

    .tdNoStyle {
    /* margin: 0; */
        padding: 0;
        border-top: none;
        border-left: none;
        border-right: none;
        outline: 0;
        font-size: 100%;
        vertical-align: baseline;
        background-color: #a10c25;
    }
    .tdNoStyle2 {
    /* margin: 0; */
        padding: 0;
        border-top: none;
        border-left: none;
        border-right: none;
        outline: 0;
        font-size: 100%;
        vertical-align: baseline;
        background-color: #ffffff;
    }

    .trBorderLeftRight{
        border-left: 1px solid black;
        border-right: 1px solid black;
        border-bottom: 1px solid black;
    }
    
    .livesearchA{
        all: unset;
    }   
</style>

@guest
please login

@else
    @extends('layouts.app')
    @section('content')

    @if(Auth::user()->userStatus == 0)
        {{ Session::flush() }}
        <script>
            window.alert("Your account is yet to be approved. Please wait for approval email from Admin");
            window.location = "/";
        </script>
    @else
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="background-color: #a10c25; color:white; font-size:20; padding-bottom:0; height:50px; vertical-align:middle">
                            <form name="deleteClassForm" method="POST" action="{{ url('/deleteClass') }}">
                                <input type="hidden" name="classID" value="{{$class_['id']}}">
                                @csrf
                                <table style="border:none; margin-bottom:0; padding:0;">
                                    <tr>
                                        <th class="tdNoStyle" style="font-size:20; vertical-align:middle">
                                            <b>Class</b>
                                        </th>
                                    </tr>
                                </table>
                            </form>
                        </div>
                        <div class="card-body" style="text-align: right">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <table id="" style="border-bottom: 1px solid black;width:100%">
                                <tr style="font-weight: bold" class="trBorderLeftRight">
                                    <th style="width: 10%; font-weight: bold">
                                        ID
                                    </th>
                                    <th colspan="2" style="font-weight: bold">
                                        Name
                                    </th>
                                    <th colspan="" style="font-weight: bold">
                                    </th>
                                    <th colspan="" style="font-weight: bold">
                                        Teacher
                                    </th>
                                </tr>
                                <tr class="trBorderLeftRight">
                                    <td>
                                        {{ __($class_['id'])}}
                                    </td>
                                    <td colspan="2">
                                        {{ __($class_['className'])}}
                                    </td>
                                    <td colspan="">
                                    </td>
                                    <td colspan="">
                                        <a style="text-decoration: none; color:black" href="{{url('userProfile/'.$class_->user->id)}}">{{ __($class_->user->name)}}</a>
                                    </td>
                                </tr>
                                <tr class="trBorderLeftRight">
                                    <th colspan="4" style="font-weight: bold">
                                        Description
                                    </th>
                                    <th style="font-weight: bold">
                                        Cost
                                    </th>
                                </tr>
                                <tr class="trBorderLeftRight">
                                    <td colspan="4" style="max-width: 1px;">
                                        {{ __($class_['classDesc'])}}
                                    </td>
                                    <td>
                                        {{ __($class_['classCost'])}}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="background-color: #a10c25; color:white; font-size:20; padding-bottom:0; height:50px; vertical-align:middle">
                            <table style="border:none; margin-bottom:0; padding:0;">
                                <tr>
                                    <th class="tdNoStyle" style="font-size:20; vertical-align:middle;">
                                        @if(Auth::user()->userRole == "Student")
                                            <b>Booking History</b>
                                        @else
                                            <b>Session History</b>
                                        @endif
                                    </th>
                                </tr>
                            </table>
                        </div>
                        <div class="card-body" style="text-align: right">
                            <form name="ApprovalForm" method="POST" action="{{ url('/approvePayment') }}">
                                @csrf
                                <table style="width:100%">
                                    <tr class="" style="background-color: white; border:none; ">
                                        <td style="border-top: none; width:5%">
                                            From
                                        </td>
                                        <td style="border-top: none; width:15%">
                                            <input class="nostyle form-control" id="inputDateFrom" type="date" value="{{ date('Y-m-d', strtotime(date('Y-m-d').'-7 day'))}}" max="{{ date('Y-m-d')}}"  onchange="filterByDate()" title="Select a from date">
                                        </td>
                                        <td style="border-top: none; text-align:right">
                                            Status
                                        </td>
                                        <td style="border-top: none; width:30%">
                                            <select id="inputSessionStatus" name="inputSessionStatus" class="form-control" onchange="filterSessionDetails()">
                                                <option selected value="ALL">All</option>
                                                <option value="confirmed">Confirmed</option>
                                                <option value="cancelled">Cancelled</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-top: none; width:5%; text-align:right">
                                            To 
                                        </td>
                                        <td style="border-top: none; width:15%">
                                            <input class="nostyle form-control" type="date" id="inputDateTo" value="{{ date('Y-m-d')}}" max="{{ date('Y-m-d')}}" onchange="filterByDate()" title="Select a to date">
                                        </td>
                                        @if (Auth::User()->userRole == 'Student')
                                            <td style="border-top: none; text-align:right">
                                                Attendance
                                            </td>
                                            <td style="border-top: none; width:30%">
                                                <select id="inputSessionAttendance" name="inputSessionAttendance" class="form-control" onchange="filterSessionDetails()">
                                                    <option selected value="ALL">All</option>
                                                    <option value="attended">Attended</option>
                                                    <option value="not Attend">Not Attend</option>
                                                </select>
                                            </td>
                                        @else
                                            <td style="border-top: none;">
                                            </td>
                                            <td style="border-top: none; width:30%">
                                            </td>
                                        @endif
                                    </tr>
                                </table>
                                <span id="inputClassID" style="display: none">{{$class_['id']}}</span>
                                <table id="tableSessionDetails" style="border-bottom: 1px solid black;width:100%">
                                    <tr  class="trBorderLeftRight">
                                        <th style="font-weight: bold; width:5%">
                                            No
                                        </th>
                                        <th style="font-weight: bold">
                                            Date
                                        </th>
                                        <th style="font-weight: bold; text-align: center">
                                            Time
                                        </th>
                                        <th style="font-weight: bold; text-align: center">
                                            Duration
                                        </th>
                                        <th style="font-weight: bold; text-align: center">
                                            Status
                                        </th>
                                        <th style="font-weight: bold;text-align: center; width:15%">
                                            Attendance
                                        </th>
                                    </tr>
                                    <tr id="trNoResult" class="trBorderLeftRight" style="display: none;">
                                        <td colspan="6" style="text-align: center">
                                            No session matched the filter!
                                        </td>
                                    </tr>
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <script>

        window.onload = function(){ //run these after page loaded
            filterByDate();
        };

        function filterByDate(){
            var inputDateFrom = document.getElementById("inputDateFrom").value;

            var inputDateTo = document.getElementById("inputDateTo").value;
            var inputClassID = document.getElementById("inputClassID").innerHTML;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'post',
                url: '/getSessionHistory',
                dataType: 'JSON',
                async:false, 
                data: {
                    dateFrom : inputDateFrom,
                    dateTo : inputDateTo,
                    classID : inputClassID,
                },
                success: function (data) {
                    if(data.sessions.length == 0){
                        $('#tableSessionDetails .trSessionList').remove();
                        $('#tableSessionDetails')
                        .append($('<tr>', {class : "trSessionList"})
                            .append($('<td>', { colspan:6, style : "color:black; text-align:center", text : 'No session history in the time range!'} ))
                        );
                    }else{
                        $('#tableSessionDetails .trSessionList').remove();
                        for (var d in data.sessions) {
                            var session = data.sessions[d];
                            var fullDate = new Date(session.sessionDate);
                            var year = fullDate.getFullYear();
                            var month = fullDate.getMonth()+1;
                            var date = fullDate.getDate();
                            var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                            var day = days[fullDate.getDay()];
                            var hours = session.sessionTime.split(":")[0];
                            var minutes = session.sessionTime.split(":")[1];
                            $('#tableSessionDetails')
                                .append($('<tr>', { class   : "trSessionList tdSessionDetails trBorderLeftRight", 
                                                    onclick :"window.location='sessionHistory/sessionDetails/"+session.id+"';"})
                                    .append($('<td>', { class : "tdNo", 
                                                        style : "color:black;", 
                                                        text  : ++d}))
                                    .append($('<td>', { text  : date +"/"+ month +"/"+ year +" ("+ day +")"}))
                                    .append($('<td>', { style : "text-align:center;", 
                                                        text  : hours+":"+minutes}))
                                    .append($('<td>', { style : "text-align:center;", 
                                                        text  : session.sessionDuration+" minutes"}))
                                    .append($('<td>', { class : "tdSessionStatus", 
                                                        style : function(){
                                                                    if(session.sessionStatus == 'Confirmed'){
                                                                        return "color:black; text-align:center; color:white; background-color:#4CAF50E7";
                                                                    }else{
                                                                        return "color:black; text-align:center; color:white; background-color:#A10C25E7";
                                                                    }
                                                                },
                                                        text  : session.sessionStatus}))
                                    .append($('<td>', { class : "tdSessionAttendance", 
                                                        style : function(){
                                                                    if(session.sessionAttendance == 'Attended'){
                                                                        return "text-align:center; color:white; background-color:#4CAF50E7";
                                                                    }else if(session.sessionAttendance == 'Not Attend'){
                                                                        return "text-align:center; color:white; background-color:#A10C25E7";
                                                                    }else{
                                                                        return "text-align:center; color:black;";
                                                                    }
                                                                }, 
                                                        text : session.sessionAttendance}))
                                );
                        }
                    }
                    
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
            });
        }

        var inputSessionStatus,inputSessionAttendance, tableSessionDetails, trSessionDetails, trCounter, tdSessionStatus, tdSessionAttendance, tdNo, trNoResult; 
        function filterSessionDetails() {
            inputSessionStatus = document.getElementById("inputSessionStatus").value.toUpperCase();
            if(document.getElementById("inputSessionAttendance")!=null){
                 inputSessionAttendance = document.getElementById("inputSessionAttendance").value.toUpperCase();
                if(inputSessionAttendance == "ALL"){
                    inputSessionAttendance = "";
                }
            }else{
                inputSessionAttendance = "";
            }
            if(inputSessionStatus == "ALL"){
                inputSessionStatus = "";
            }
            tableSessionDetails = document.getElementById("tableSessionDetails");
            trSessionDetails = tableSessionDetails.getElementsByTagName("tr");
            trCounter = 0;
            for (i = 0; i < trSessionDetails.length; i++) {
                tdSessionStatus = trSessionDetails[i].getElementsByClassName("tdSessionStatus")[0];
                tdSessionAttendance = trSessionDetails[i].getElementsByClassName("tdSessionAttendance")[0];
                tdNo = trSessionDetails[i].getElementsByClassName("tdNo")[0];
                if (tdSessionStatus && tdSessionAttendance) {
                    txttdSessionStatus = tdSessionStatus.textContent || tdSessionStatus.innerText;
                    txttdSessionAttendance = tdSessionAttendance.textContent || tdSessionAttendance.innerText;
                    if (txttdSessionStatus.toUpperCase().indexOf(inputSessionStatus) > -1 && txttdSessionAttendance.toUpperCase().indexOf(inputSessionAttendance) > -1) {
                        trSessionDetails[i].style.display = "";
                        tdNo.innerHTML= ++trCounter;
                    } else {
                        trSessionDetails[i].style.display = "none";
                    }
                }                 
            }
            trNoResult = document.getElementById('trNoResult');
            if(trNoResult != null){
                if(trCounter == 0){
                    trNoResult.style.display = "";
                }else{
                    trNoResult.style.display = "none";
                }
            }
        }
    </script>
    @endsection
@endguest



