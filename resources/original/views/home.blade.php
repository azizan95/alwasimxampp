@extends('layouts.app')
@section('content')

<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }

    .tdSessionDetails{
        cursor: pointer;
    }

    .trBorderLeftRight{
        border-left: 1px solid black;
        border-right: 1px solid black;
        border-bottom: 1px solid black;
    }
    
    th, td {
        padding: 8px;
        text-align: left;
        border-top: 1px solid black;
    }

    tr:nth-child(odd) {
        background-color: #f2f2f2;
    }

    th {
        text-align:;
        font-weight: normal;
        font-size: 15;
        background-color: #242d5f;
        color: white;
    }

    .button1:enabled {
        background-color: white; 
        color: black; 
        border: 2px solid #4CAF50;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .button1:hover {
        background-color: #4CAF50;
        color: white;
        }
    
    .button1:disabled{
        background-color: #ffffff;
        color: grey; 
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonMenu:enabled {
        background-color: white; 
        color: black; 
        border:2px solid var(--myYellow);
        border-radius: 4px;
        width: 200px;
        height: 50px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonMenu:hover {
        background-color: var(--myYellowHover);
        border: 2px solid var(--myYellow);
        color: white;
        -webkit-box-shadow: inset 1px 1px 10px rgba(51, 51, 51, 0.200);
        -moz-box-shadow:    inset 1px 1px 10px rgba(51, 51, 51, 0.200);
        box-shadow:         inset 1px 1px 10px rgba(51, 51, 51, 0.200);
        }

    .buttonReject:enabled {
        background-color: white; 
        color: black; 
        border: 2px solid #a10c25;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonReject:hover {
        background-color: #a10c25;
        color: white;
        }
    
    .buttonReject:disabled{
        background-color: #ffffff;
        color: grey; 
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonChangeView:enabled{
        background-color: white; 
        color: black; 
        border:2px solid var(--myYellow);
        border-radius: 4px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonChangeView:hover{
        background-color: var(--myYellowHover);
        border: 2px solid var(--myYellow);
        color: white;
        -webkit-box-shadow: inset 1px 1px 10px rgba(51, 51, 51, 0.200);
        -moz-box-shadow:    inset 1px 1px 10px rgba(51, 51, 51, 0.200);
        box-shadow:         inset 1px 1px 10px rgba(51, 51, 51, 0.200);
    }

    /* .buttonChangeView:focus {
        outline: 0 !important;
    } */

    /* .buttonChangeView:focus{
        background-color: white; 
        color: black; 
        border:2px solid var(--myYellow);
        border-radius: 4px;
        margin-top: 5px;
        margin-right: 0px
    } */

    /* .nostyle{
        -webkit-appearance: none; 
        border:1px solid;
       } */
    
    
</style>

@guest
    please login
@else
    @if(Auth::user()->userStatus == 0)
        <script>
            window.location = "/";
        </script>
    @elseif(Auth::user()->userRole == "Admin")
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="font-size: 20px"><b>{{ __('Admin Menus') }}</b></div>

                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <div style="text-align: center">
                                <button value="" class="buttonMenu" onclick="window.location='{{ url('userManagement') }}'" id="" style="width: 200; font-size:large">User Management</button>&nbsp&nbsp&nbsp&nbsp
                                <button value="" class="buttonMenu" onclick="window.location='{{ url('classManagement') }}'" id="" style="width: 200; font-size:large">Class Management</button>&nbsp&nbsp&nbsp&nbsp
                                <button value="" class="buttonMenu" onclick="window.location='{{ url('creditManagement') }}'" id="" style="width: 200; font-size:large">Credit Management</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @elseif(Auth::user()->userRole == "Student")
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="font-size: 20px"><b>{{ __('Student Menus') }}</b></div>
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <div style="text-align: center">
                                <button value="" class="buttonMenu" onclick="window.location='{{ url('classManagement') }}'" id="stu" style="width: 200; font-size:large">Manage Class</button>&nbsp&nbsp&nbsp&nbsp
                                <button value="" class="buttonMenu" onclick="window.location='{{url('userProfile/'.Auth::user()->id)}}'" id="stu" style="width: 200; font-size:large">Manage Profile</button>&nbsp&nbsp&nbsp&nbsp
                                <button value="" class="buttonMenu" onclick="window.location='{{url('creditDetails/'.Auth::user()->id)}}'" id="stu" style="width: 200; font-size:large">Credit Management</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="background-color: #a10c25; color:white; font-size:20px">
                            <b>Upcoming Sessions</b>
                        </div>
                        <div class="card-body" style="text-align: right">
                            <form name="ApprovalForm" method="POST" action="{{ url('/approvePayment') }}">
                                @csrf
                                <table id="tableSessionDetails" style="border-bottom: 1px solid black;width:100%">
                                    <tr class="trBorderLeftRight">
                                        <th style="font-weight: bold;">
                                            No
                                        </th>
                                        <th style="font-weight: bold">
                                            Class
                                        </th>
                                        <th style="font-weight: bold">
                                            Date
                                        </th>
                                        <th style="font-weight: bold">
                                            Time
                                        </th>
                                        <th style="font-weight: bold;text-align: center;">
                                            Capacity
                                        </th>
                                        <th style="font-weight: bold;text-align: center;">
                                            Status
                                        </th>
                                    </tr>
                                    @if(Auth::User()->booked->count() == 0)
                                        <tr class="trBorderLeftRight">
                                            <td colspan="6" style="text-align: center;">
                                                No session booked yet!
                                            </td>
                                        </tr>
                                    @else
                                        @php
                                            $counter = 0;
                                            $currentTimeDate = Date('d-m-Y H:i');
                                            $sessions = Auth::User()->booked->sortBy(function($sessions){
                                                return sprintf('%-12s%s', $sessions->sessionDate, $sessions->sessionTime);
                                            });   
                                        @endphp
                                        @foreach($sessions as $session)
                                            @php
                                                $currentTimeDate = Date('d-m-Y H:i');
                                                $sessionTimeDate = Date($session['sessionDate'] . $session['sessionTime']);
                                                $sessionTimeDate = Date('d-m-Y H:i', strtotime($sessionTimeDate));
                                                $sessionTimeDateEnd = Date($session['sessionDate'] . $session['sessionTimeEnd']);
                                                $sessionTimeDateEnd = Date('d-m-Y H:i', strtotime($sessionTimeDateEnd));
                                                $sessiondate = new DateTime(Date('d-m-Y H:i', strtotime($sessionTimeDate)));
                                                $sessiondateend = new DateTime(Date('d-m-Y H:i', strtotime($sessionTimeDateEnd)));
                                                $currentdate = new DateTime(Date('d-m-Y H:i', strtotime($currentTimeDate)));
                                                $timeDiff = $currentdate->diff($sessiondate);
                                                $minutes = $timeDiff->days * 24 * 60;
                                                $minutes += $timeDiff->h * 60;
                                                $minutes += $timeDiff->i;
                                                $timeDiff = $timeDiff->format('%r'); //get + or - time diff // + means before time session, - means after time session
                                                $timeDiff = $timeDiff.$minutes; //put + or - in front timediff in minutes

                                                //$boolTimeDiff = $minutes <= 15; //time diff +-15 // timenow +-15 = timesession // 15minit before and after time session
                                                //$boolTimeDiffPos = ($timeDiff >= 15); //time diff +15 // timenow +15 = timesession // 15minit before time session
                                                //$boolTimeDiffNeg = ($timeDiff >= -15); //time diff -15 // timenow -15 = timesession // 15minit after time session

                                                //$boolSessionTimeDate = (Date(strtotime($currentTimeDate)) < Date(strtotime($sessionTimeDate)));
                                                $boolSessionTimeDateEnd = (new DateTime(Date('d-m-Y H:i', strtotime($currentTimeDate)))) < (new DateTime(Date('d-m-Y H:i', strtotime($sessionTimeDateEnd))));
                                            @endphp
                                            @if($boolSessionTimeDateEnd)
                                                @if($timeDiff <= 10 && $boolSessionTimeDateEnd)
                                                    <tr class="trBorderLeftRight trSession tdSessionDetails" style="background-color:{{ $session['sessionStatus'] == 'Cancelled' ? '#a10c25e7; color:white' : '#4CAF50E7; color:white' }}" onclick="window.location='classDetails/{{$session->class_['id']}}/sessionDetails/{{$session['id']}}';">
                                                @else
                                                    <tr class="trBorderLeftRight trSession tdSessionDetails" onclick="window.location='classDetails/{{$session->class_['id']}}/sessionDetails/{{$session['id']}}';">
                                                @endif
                                                    <td>
                                                        {{++$counter}}
                                                    </td>
                                                    <td class="">
                                                        {{$session->class_['className']}}
                                                    </td>
                                                    <td class="tdSessionDate tdSessionDetails">
                                                        {{ date('d/m/Y', strtotime($session['sessionDate']))}} ({{ date('l', strtotime($session['sessionDate']))}})
                                                    </td>
                                                    <td class="tdSessionTime tdSessionDetails">
                                                        {{ date('H:i', strtotime($session['sessionTime']))}}
                                                    </td>
                                                    <td class="tdSessionDetails" style="text-align: center;">
                                                        {{$session->bookedBy->count()}} / {{ __($session['sessionCapacity'])}}
                                                    </td>
                                                    @if($session['sessionStatus'] == "Cancelled")
                                                        <td class="tdSessionDetails" style="text-align: center; background-color:#a10c25; color:white">
                                                    @elseif($session['sessionStatus'] == "Confirmed")
                                                        <td class="tdSessionDetails" style="text-align: center; background-color:#4CAF50;">
                                                    @else
                                                        <td class="tdSessionDetails" style="text-align: center; background-color:var(--myYellow);">
                                                    @endif
                                                        {{$session['sessionStatus']}}
                                                    </td>
                                                </tr>
                                            @endif    
                                        @endforeach
                                        @if($counter == 0)
                                            <tr class="trBorderLeftRight">
                                                <td colspan="6" style="text-align: center;">
                                                    No upcoming session!
                                                </td>
                                            </tr>
                                        @endif
                                    @endif
                                </table>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @elseif(Auth::user()->userRole == "Teacher")
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="font-size: 20px"><b>{{ __('Teacher Menus') }}</b></div>
                        <div class="card-body">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <div style="text-align: center">
                                <button class="buttonMenu" onclick="window.location='{{ url('classManagement') }}'" id="stu" style="width: 200; font-size:large">Manage Class</button>&nbsp&nbsp&nbsp&nbsp
                                <button class="buttonMenu" onclick="window.location='{{url('userProfile/'.Auth::user()->id)}}'" id="stu" style="width: 200; font-size:large">Manage Profile</button>&nbsp&nbsp&nbsp&nbsp
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="background-color: #a10c25; color:white; font-size:20px">
                            <b>Upcoming Sessions</b>
                        </div>
                        <div class="card-body" style="text-align: right">
                            <div style="text-align: right; margin-bottom:5px">
                                <button type="button" class="buttonChangeView" id="buttonChangeView" onclick="changeTableView()" style="">View by class</button>
                            </div>
                            <div id="tableUpcomingSessionAllDiv" style="text-align: right">
                                <table id="tableUpcomingSessionAll" style="border-bottom: 1px solid black; border-top: 1px solid #ffcd04; width:100%;">
                                    <tr class="trBorderLeftRight">
                                        <th style="font-weight: bold;">
                                            No
                                        </th>
                                        <th style="font-weight: bold">
                                            Class
                                        </th>
                                        <th style="font-weight: bold">
                                            Date
                                        </th>
                                        <th style="font-weight: bold">
                                            Time
                                        </th>
                                        <th style="font-weight: bold;text-align: center;">
                                            Capacity
                                        </th>
                                        <th style="font-weight: bold;text-align: center;">
                                            Status
                                        </th>
                                    </tr>
                                    @if(Auth::User()->sessions->count() == 0)
                                        <tr class="trBorderLeftRight">
                                            <td colspan="5" style="text-align: center;">
                                                No session created yet!
                                            </td>
                                        </tr>
                                    @endif
                                    @php   
                                        $counter = 0;
                                        $sessions = Auth::User()->sessions;
                                        $currentTimeDate = Date('d-m-Y H:i');
                                        $sessions = $sessions->sortBy(function($sessions){
                                            return sprintf('%-12s%s', $sessions->sessionDate, $sessions->sessionTime);
                                        });   
                                    @endphp
                                    @foreach ($sessions as $session)
                                        @php
                                            $currentTimeDate = Date('d-m-Y H:i');
                                            $sessionTimeDate = Date($session['sessionDate'] . $session['sessionTime']);
                                            $sessionTimeDate = Date('d-m-Y H:i', strtotime($sessionTimeDate));
                                            $sessionTimeDateEnd = Date($session['sessionDate'] . $session['sessionTimeEnd']);
                                            $sessionTimeDateEnd = Date('d-m-Y H:i', strtotime($sessionTimeDateEnd));
                                            $sessiondate = new DateTime(Date('d-m-Y H:i', strtotime($sessionTimeDate)));
                                            $sessiondateend = new DateTime(Date('d-m-Y H:i', strtotime($sessionTimeDateEnd)));
                                            $currentdate = new DateTime(Date('d-m-Y H:i', strtotime($currentTimeDate)));
                                            $timeDiff = $currentdate->diff($sessiondate);
                                            $minutes = $timeDiff->days * 24 * 60;
                                            $minutes += $timeDiff->h * 60;
                                            $minutes += $timeDiff->i;
                                            $timeDiff = $timeDiff->format('%r'); //get + or - time diff // + means before time session, - means after time session
                                            $timeDiff = $timeDiff.$minutes; //put + or - in front timediff in minutes

                                            //$boolTimeDiff = $minutes <= 15; //time diff +-15 // timenow +-15 = timesession // 15minit before and after time session
                                            //$boolTimeDiffPos = ($timeDiff >= 15); //time diff +15 // timenow +15 = timesession // 15minit before time session
                                            //$boolTimeDiffNeg = ($timeDiff >= -15); //time diff -15 // timenow -15 = timesession // 15minit after time session

                                            //$boolSessionTimeDate = (Date(strtotime($currentTimeDate)) < Date(strtotime($sessionTimeDate)));
                                            // $boolSessionTimeDateEnd = (Date(strtotime($currentTimeDate)) < Date(strtotime($sessionTimeDateEnd)));
                                            $boolSessionTimeDateEnd = (new DateTime(Date('d-m-Y H:i', strtotime($currentTimeDate)))) < (new DateTime(Date('d-m-Y H:i', strtotime($sessionTimeDateEnd))));
                                        @endphp
                                        @if($boolSessionTimeDateEnd)
                                            @if($timeDiff <= 10 && $boolSessionTimeDateEnd)
                                                <tr class="trBorderLeftRight trSession tdSessionDetails" style="background-color:{{ $session['sessionStatus'] == 'Cancelled' ? '#a10c25e7; color:white' : '#4CAF50E7; color:white' }}" onclick="window.location='classDetails/{{$session->class_['id']}}/sessionDetails/{{$session['id']}}';">
                                            @else
                                                <tr class="trBorderLeftRight trSession tdSessionDetails" onclick="window.location='classDetails/{{$session->class_['id']}}/sessionDetails/{{$session['id']}}';">
                                            @endif
                                                <td>
                                                    {{++$counter}}
                                                </td>
                                                <td class="">
                                                    {{$session->class_['className']}}
                                                </td>
                                                <td class="tdSessionDate tdSessionDetails">
                                                    {{ date('d/m/Y', strtotime($session['sessionDate']))}} ({{ date('l', strtotime($session['sessionDate']))}})
                                                </td>
                                                <td class="tdSessionTime tdSessionDetails">
                                                    {{ date('H:i', strtotime($session['sessionTime']))}}
                                                </td>
                                                <td class="tdSessionDetails" style="text-align: center;">
                                                    {{$session->bookedBy->count()}} / {{ __($session['sessionCapacity'])}}
                                                </td>
                                                @if($session['sessionStatus'] == "Cancelled")
                                                    <td class="tdSessionDetails" style="text-align: center; background-color:#a10c25; color:white">
                                                @elseif($session['sessionStatus'] == "Confirmed")
                                                    <td class="tdSessionDetails" style="text-align: center; background-color:#4CAF50;">
                                                @else
                                                    <td class="tdSessionDetails" style="text-align: center; background-color:var(--myYellow);">
                                                @endif
                                                    {{$session['sessionStatus']}}
                                                </td>
                                            </tr>
                                        @endif 
                                    @endforeach
                                    @if($counter == 0)
                                        <tr class="trBorderLeftRight">
                                            <td colspan="6" style="text-align: center;">
                                                No upcoming session!
                                            </td>
                                        </tr>
                                    @endif
                                </table>
                            </div>
                            <div id="tableUpcomingSessionByClassDiv" style="display: none">
                                @foreach (Auth::User()->classes as $class_)
                                    {{-- @if($class_) --}}
                                    <table id="tableUpcomingSessionByClass" style="border-bottom: 1px solid black; width:100%;">
                                        <tr class="" style="border-top:none;">
                                            <th colspan="5" style="background-color:white; border-top: none; color:black; padding:0px;font-size:">
                                                <a style="text-decoration: none; color:black" href="{{url('classDetails/'.$class_['id'])}}"><b>{{$class_['className']}}</b></a>
                                            </th>
                                        </tr>
                                        <tr class="trBorderLeftRight">
                                            <th style="font-weight: bold;">
                                                No
                                            </th>
                                            
                                            <th style="font-weight: bold">
                                                Date
                                            </th>
                                            <th style="font-weight: bold">
                                                Time
                                            </th>
                                            <th style="font-weight: bold;text-align: center;">
                                                Capacity
                                            </th>
                                            <th style="font-weight: bold;text-align: center; width:15%">
                                                Status
                                            </th>
                                        </tr>
                                        @if($class_->session->count() == 0)
                                            <tr class="trBorderLeftRight">
                                                <td colspan="5" style="text-align: center;">
                                                    No session created yet!
                                                </td>
                                            </tr>
                                        @else
                                            @php
                                                $counter = 0;
                                                $currentTimeDate = Date('d-m-Y H:i');
                                                $sessions = $class_->session->sortBy(function($sessions){
                                                    return sprintf('%-12s%s', $sessions->sessionDate, $sessions->sessionTime);
                                                });   
                                            @endphp
                                            @foreach($sessions as $session)
                                                @php
                                                $currentTimeDate = Date('d-m-Y H:i');
                                                $sessionTimeDate = Date($session['sessionDate'] . $session['sessionTime']);
                                                $sessionTimeDate = Date('d-m-Y H:i', strtotime($sessionTimeDate));
                                                $sessionTimeDateEnd = Date($session['sessionDate'] . $session['sessionTimeEnd']);
                                                $sessionTimeDateEnd = Date('d-m-Y H:i', strtotime($sessionTimeDateEnd));
                                                $sessiondate = new DateTime(Date('d-m-Y H:i', strtotime($sessionTimeDate)));
                                                $sessiondateend = new DateTime(Date('d-m-Y H:i', strtotime($sessionTimeDateEnd)));
                                                $currentdate = new DateTime(Date('d-m-Y H:i', strtotime($currentTimeDate)));
                                                $timeDiff = $currentdate->diff($sessiondate);
                                                $minutes = $timeDiff->days * 24 * 60;
                                                $minutes += $timeDiff->h * 60;
                                                $minutes += $timeDiff->i;
                                                $timeDiff = $timeDiff->format('%r'); //get + or - time diff // + means before time session, - means after time session
                                                $timeDiff = $timeDiff.$minutes; //put + or - in front timediff in minutes

                                                //$boolTimeDiff = $minutes <= 15; //time diff +-15 // timenow +-15 = timesession // 15minit before and after time session
                                                //$boolTimeDiffPos = ($timeDiff >= 15); //time diff +15 // timenow +15 = timesession // 15minit before time session
                                                //$boolTimeDiffNeg = ($timeDiff >= -15); //time diff -15 // timenow -15 = timesession // 15minit after time session

                                                //$boolSessionTimeDate = (Date(strtotime($currentTimeDate)) < Date(strtotime($sessionTimeDate)));
                                                // $boolSessionTimeDateEnd = (Date(strtotime($currentTimeDate)) < Date(strtotime($sessionTimeDateEnd)));
                                                $boolSessionTimeDateEnd = (new DateTime(Date('d-m-Y H:i', strtotime($currentTimeDate)))) < (new DateTime(Date('d-m-Y H:i', strtotime($sessionTimeDateEnd))));
                                            @endphp
                                            @if($boolSessionTimeDateEnd)
                                                @if($timeDiff <= 10 && $boolSessionTimeDateEnd)
                                                    <tr class="trBorderLeftRight trSession tdSessionDetails" style="background-color:{{ $session['sessionStatus'] == 'Cancelled' ? '#a10c25e7; color:white' : '#4CAF50E7; color:white' }}" onclick="window.location='classDetails/{{$session->class_['id']}}/sessionDetails/{{$session['id']}}';">
                                                @else
                                                    <tr class="trBorderLeftRight trSession tdSessionDetails" onclick="window.location='classDetails/{{$session->class_['id']}}/sessionDetails/{{$session['id']}}';">
                                                @endif
                                                    <td>
                                                        {{++$counter}}
                                                    </td>
                                                    
                                                    <td class="tdSessionDate tdSessionDetails">
                                                        {{ date('d/m/Y', strtotime($session['sessionDate']))}} ({{ date('l', strtotime($session['sessionDate']))}})
                                                    </td>
                                                    <td class="tdSessionTime tdSessionDetails">
                                                        {{ date('H:i', strtotime($session['sessionTime']))}}
                                                    </td>
                                                    <td class="tdSessionDetails" style="text-align: center;">
                                                        {{$session->bookedBy->count()}} / {{ __($session['sessionCapacity'])}}
                                                    </td>
                                                    @if($session['sessionStatus'] == "Cancelled")
                                                        <td class="tdSessionDetails" style="text-align: center; background-color:#a10c25; color:white">
                                                    @elseif($session['sessionStatus'] == "Confirmed")
                                                        <td class="tdSessionDetails" style="text-align: center; background-color:#4CAF50;">
                                                    @else
                                                        <td class="tdSessionDetails" style="text-align: center; background-color:var(--myYellow);">
                                                    @endif
                                                        {{$session['sessionStatus']}}
                                                    </td>
                                                </tr>
                                            @endif    
                                        @endforeach
                                        @if($counter == 0)
                                            <tr class="trBorderLeftRight">
                                                <td colspan="5" style="text-align: center;">
                                                    No upcoming session!
                                                </td>
                                            </tr>
                                        @endif
                                    @endif
                                </table>
                                    <br>
                            @endforeach
                            </div>
                            @if(Auth::user()->is_admin == 1)
                                <input type="hidden" name="classID" value="{{ __($class_['id'])}}">
                                <span><input type="submit" value="Approve" class="button1" onclick="return confirm('Confirm approve?')" id="mno" disabled>&nbsp<input type="submit" value="Remove" class="buttonReject" formaction="{{ url('/rejectPayment') }}" onclick="return confirm('Confirm reject?')" id="pqr" disabled></span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endif
<script>

    var tableUpcomingSessionAllDiv = document.getElementById("tableUpcomingSessionAllDiv");
    var tableUpcomingSessionByClassDiv = document.getElementById("tableUpcomingSessionByClassDiv");
    var currentTable = tableUpcomingSessionAllDiv;
    var buttonChangeView = document.getElementById("buttonChangeView");
    function changeTableView(){
        if(currentTable == tableUpcomingSessionAllDiv){
            tableUpcomingSessionByClassDiv.style.display="block";
            tableUpcomingSessionAllDiv.style.display="none";
            currentTable = tableUpcomingSessionByClassDiv;
            buttonChangeView.innerHTML = "View all";
        }else{
            tableUpcomingSessionByClassDiv.style.display="none";
            tableUpcomingSessionAllDiv.style.display="block";
            currentTable = tableUpcomingSessionAllDiv;
            buttonChangeView.innerHTML = "View by class";
        }
    }
    // const uploadButton = document.querySelector('.browse-btn');
    // const fileInfo = document.querySelector('.file-info');
    // const realInput = document.getElementById('real-input');
    // var newPpImg = document.getElementById('newPpImg');

    // uploadButton.addEventListener('click', (e) => {
    //     realInput.click();
    //     }
    // );

    // realInput.addEventListener('change', () => {
    //     const name = realInput.value.split(/\\|\//).pop();
    //     const truncated = name.length > 20 ? name.substr(name.length - 20) : name;
        
    //     fileInfo.innerHTML = truncated;

    //     newPpImg.src = URL.createObjectURL(event.target.files[0]); // change img src preview in the form
    //     newPpImg.style.visibility = "visible"; // change img visibility
    //     newPpImg.height = "100"; // change img height

    // });
</script>
@endsection
