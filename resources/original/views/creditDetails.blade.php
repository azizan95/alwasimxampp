<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }
    
    th, td {
        padding: 8px;
        text-align: left;
        border-top: 1px solid black;
    }

    tr:nth-child(odd) {
        background-color: #f2f2f2;
    }

    th {
        text-align: center;
        font-weight: normal;
        font-size: 15;
        background-color: #242d5f;
        color: white;
    }

    .buttonCleared:disabled {
        text-align: center;
        background-color: #4CAF50; 
        color: white; 
        border: 2px solid #409143;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .button1:enabled {
        background-color: white; 
        color: black; 
        border: 2px solid #4CAF50;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .button1:hover {
        background-color: #4CAF50;
        color: white;
        }
    
    .button1:disabled{
        background-color: #ffffff;
        color: grey; 
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonNoInfo:disabled {
        background-color: #a10c25; 
        color: white; 
        border: 2px solid #8a0b20;
        border-radius: 4px;
        /* width: 75px; */
        margin-top: 5px;
        margin-right: 0px;
        text-align: center;
    }

    .buttonReject:enabled {
        background-color: white; 
        color: black; 
        border: 2px solid #a10c25;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonReject:hover {
        background-color: #a10c25;
        color: white;
        }
    
    .buttonReject:disabled{
        background-color: #ffffff;
        color: grey; 
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .nostyle{
        -webkit-appearance: none; 
        border:1px solid;
       }

    .modal {
        display: block; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }

    .modal-content {
        /* background-color: #fefefe; */
        background-color: #3D3D3D;
        text-align: center;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        /* width: 10%; */
    } 

    .tdNoStyle {
    /* margin: 0; */
        padding: 0;
        border-top: none;
        border-left: none;
        border-right: none;
        outline: 0;
        font-size: 100%;
        vertical-align: baseline;
        background-color: #a10c25;
    }
</style>

@guest
please login

@else
    @extends('layouts.app')
    @section('content')
    
    @if(Auth::user()->userStatus == 0)
        {{ Session::flush() }}
        <script>
            window.alert("Your account is yet to be approved. Please wait for approval email from Admin");
            window.location = "/";
        </script>
    @else
        @if((Auth::user()->id != $user['id'] && Auth::user()->userRole != "Admin") || $user['userRole'] == "Teacher")
            @if($user['userRole'] == "Teacher")
                <script>
                    window.location ='{{ url("userProfile/".$user->id)}}';
                </script>
            @endif
            @if(Auth::user()->userRole == "Teacher")
                <script>
                    window.location ='{{ url("userProfile/".Auth::user()->id)}}';
                </script>
            @endif
            @if(Route::current()->getName() == 'creditDetails')
                <script>
                    window.location ='{{ url("creditDetails/".Auth::user()->id)}}';
                </script>
            @else
                <script>
                    window.location ='{{ url("userProfile/".Auth::user()->id)."/creditDetails" }}';
                </script>
            @endif
        @endif
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="background-color: #a10c25; color:white; font-size:20; padding-bottom:0; height:50px; vertical-align:middle">
                            <table style="border:none; margin-bottom:0; padding:0;">
                                <tr>
                                    <th class="tdNoStyle" style="font-size:20; vertical-align:middle">
                                        <b>Student</b>
                                    </th>
                                </tr>
                            </table>
                        </div>
                        <div class="card-body" style="text-align: right">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <table style="border: 1px solid black; width:100%;">
                                <tr style="font-weight: bold">
                                    <th style="width: 5%; font-weight: bold; border:1px solid none">
                                        ID
                                    </th>
                                    <th colspan="" style="font-weight: bold; border:1px solid none">
                                        Name
                                    </th>
                                    <th colspan="" style="font-weight: bold; border:1px solid none">
                                        Email
                                    </th>
                                    <th style="font-weight: bold; border:1px solid none"> 
                                        Phone Number
                                    </th>
                                </tr>
                                <tr>
                                    <td style="font-weight: ; border:1px solid none">
                                        <span id='userID'>{{ __($user['id'])}}</span>
                                    </td>
                                    <td colspan="" style="width:30%; font-weight: ; border:1px solid none">
                                        {{ __($user['name'])}}
                                    </td>
                                    <td colspan="" style="font-weight: ; border:1px solid none">
                                        {{ __($user['email'])}}
                                    </td>
                                    <td style="border:1px solid none">
                                        {{ __($user['userPhone'])}}
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header" style="background-color: #a10c25; color:white; font-size:20; padding-bottom:0; height:50px; vertical-align:middle">
                            <table style="border:none; margin-bottom:0; padding:0;">
                                <tr>
                                    <th class="tdNoStyle" style="font-size:20; vertical-align:middle">
                                        <b>Credit Details</b>
                                    </th>
                                    <th class="" style="text-align: right; padding:0; vertical-align:middle; background-color:#a10c25; border:none">
                                        @if ((Auth::User()->userRole == "Admin" || Auth::User()->id == $user['id']) && $user->userRole != "Teacher")
                                            <button form="" style="background-color:transparent; box-shadow: 0px 0px 0px transparent; border: none; text-shadow: 0px 0px 0px transparent; height:30; padding:0; margin-right: 2" onclick="topupModalShow()" >
                                                <img class="deletebutton2" src="/images/icons/topuporange.png" style="width: 25" title="Topup Credit" />
                                            </button>
                                        @endif
                                    </th>
                                </tr>
                            </table>
                        </div>
                        <div class="card-body" style="text-align: right">
                            @if (session('status'))
                                <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                </div>
                            @endif
                            <table id="tableCreditDetails" style="border: 1px solid black; width:100%;">
                                <tr style="font-weight: bold">
                                    <th style="width: 5%; font-weight: bold; border:1px solid none; width:5%">
                                        No
                                    </th>
                                    <th colspan="" style="font-weight: bold; border:1px solid none; width:20%">
                                        Date
                                    </th>
                                    <th colspan="" style="font-weight: bold; border:1px solid none">
                                        Transaction
                                    </th>
                                    <th style="font-weight: bold; border:1px solid none; width:10%; text-align:right"> 
                                        Credit
                                    </th>
                                    <th style="font-weight: bold; border:1px solid none; width:10%; text-align:right"> 
                                        Debit
                                    </th>
                                    <th style="font-weight: bold; border:1px solid none; width:10%; text-align:right"> 
                                        Balance
                                    </th>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" id="topupModal" style="border: solid">
            <div class="row justify-content-center" id="topupModal2" >
                <div class="col-md-8 row justify-content-center" id="topupModal3" style="border: none; text-align:center" >
                    <div class="card" id="editCard" style="border: none; width: 40%">
                        <div class="card-header" style="background-color:#a10c25; color:white"><b>{{ __('Topup Credit') }}</b></div>
                        <div class="card-body">
                            <form method="POST" action="{{ url('/topupCredit') }}">
                                @csrf
                                <input type="hidden" name="userID" value="{{ __($user['id'])}}">
                                <div class="form-group row">
                                    <label for="Name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" value="{{$user['name']}}" disabled autofocus>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="Phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone Number') }}</label>
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" value="{{$user['userPhone']}}" disabled autofocus>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="Current_Credit" class="col-md-4 col-form-label text-md-right">{{ __('Current Credit') }}</label>
                                    <div class="col-md-6">
                                        <input id="inputCreditBalance" type="number" class="form-control" value="0" disabled autofocus>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="topupAmount" class="col-md-4 col-form-label text-md-right">{{ __('Topup Amount') }}</label>
                                    <div class="col-md-6">
                                        <input id="topupAmount" type="number" class="form-control @error('topupAmount') is-invalid @enderror" name="topupAmount" min="1" step="1" required autofocus>
                                        @error('topupAmount')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row mb-0 justify-content-center" style="text-align: center;">
                                    <button type="submit" class="button1" style="width: 100">
                                        {{ __('Submit') }}
                                    </button>&nbsp &nbsp
                                    <button type="reset" class="buttonReject" style="width: 100" onclick="eventCloseEditFunction()">
                                        {{ __('Cancel') }}
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(session()->get('topupSuccessful') != null)
            <span id="topupSuccessful" style="display: none">{{session()->get('topupSuccessful')}}</span>
        @endif
    @endif
    <script>
        window.onload = function(){ //run these after page loaded
            getCreditDetails();

            if(document.getElementById('topupSuccessful') != null){
                setTimeout(function topupSuccessful(){
                    alert(document.getElementById('topupSuccessful').innerHTML);
                },50);
            }         
        }

        function topupModalShow() {
            topupModal.style.display = "block";
            topupModal.style.overflowY = "";
            const body = document.body;
            body.style.overflowY = 'hidden';
        };

        document.addEventListener('keydown', function(event){
            if(event.key === "Escape"){
                eventCloseEditFunction();
            }
        });

        document.addEventListener("click", function(e)
        {
            if ((e.target==topupModal || e.target==topupModal2 || e.target==topupModal3)) 
            {
                eventCloseEditFunction();
            }
        });

        function eventCloseEditFunction() {
            const body = document.body;
            body.style.overflowY = '';
            topupModal.style.display = "none";
        };

        function getCreditDetails(){
            var userID = document.getElementById("userID").innerText;
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: 'post',
                url: '/getCreditDetails',
                dataType: 'JSON',
                async:false, 
                data: {
                    userID : userID,
                },
                success: function (data) {
                    if(data.credits.length == 0){
                        $('#tableCreditDetails .trCreditDetails').remove();
                        $('#tableCreditDetails')
                        .append($('<tr>', {class : "trCreditDetails"})
                            .append($('<td>', { colspan:6, 
                                                style : "color:black; text-align:center", 
                                                text : 'No credit history yet!'} ))
                        );
                    }else{
                        $('#tableCreditDetails .trCreditDetails').remove();
                        for (var d in data.credits) {
                            var credit = data.credits[d];
                            var fullDate = new Date(credit.creditDate);
                            var year = fullDate.getFullYear();
                            var month = fullDate.getMonth()+1;
                            var date = fullDate.getDate();
                            var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
                            var day = days[fullDate.getDay()];
                            $('#tableCreditDetails')
                                .append($('<tr>', { class   : "trCreditDetails", 
                                                    onclick : ""})
                                    .append($('<td>', { class : "tdNo", 
                                                        style : "color : black; text-align: center;", 
                                                        text  : ++d}))
                                    .append($('<td>', { text  : date +"/"+ month +"/"+ year}))
                                    .append($('<td>', { style : "text-align:;", 
                                                        text  : credit.creditTransaction}))
                                    .append($('<td>', { style : "text-align:right;", 
                                                        text  : credit.creditCredit}))
                                    .append($('<td>', { style : "text-align:right;", 
                                                        text  : credit.creditDebit}))
                                    .append($('<td>', { style : "text-align:right;", 
                                                        text  : credit.creditBalance}))
                                );
                        }
                        document.getElementById("inputCreditBalance").value = data.credits[0].creditBalance;
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                },
            });
        }
    </script>
    @endsection
@endguest



