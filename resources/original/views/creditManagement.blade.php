<style>
    table {
        border-collapse: collapse;
        width: 100%;
    }
    
    th, td {
        padding: 8px;
        text-align: left;
        border-top: 1px solid black;
        font-size: 14;
        
    }

    tr:nth-child(odd) {
        background-color: #f2f2f2;
        
    }

    th {
        text-align: center;
        font-weight: normal;
        font-size: 15;
        background-color: #242d5f;
        color: white;
    }

    .trBorderLeftRight{
        border-left: 1px solid black;
        border-right: 1px solid black;
        border-bottom: 1px solid black;
    }

    .tdNoStyle {
    /* margin: 0; */
        padding: 0;
        border-top: none;
        border-left: none;
        border-right: none;
        outline: 0;
        font-size: 100%;
        vertical-align: baseline;
        background-color: white;
    }

    .btnReg:enabled {
        background-color: white; 
        color: black; 
        border: 2px solid #4CAF50;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .btnReg:hover {
        background-color: #4CAF50;
        color: white;
        }
    
    .btnReg:disabled{
        background-color: #ffffff;
        color: grey; 
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .button1:enabled {
        background-color: white; 
        color: black; 
        border: 2px solid #ffcd04;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonPaging {
        background-color: white; 
        color: black; 
        border: 1px solid #4CAF50;
        border-radius: 4px;
        width: 30px;
        margin-top: 2px;
        margin-right: 1px;
        font-size: 12;
    }

    .buttonPagingCurrent{
        background-color: #4CAF50;
        border: 1px solid #4CAF50;
        border-radius: 4px;
        margin-right: 1px;
        margin-top: 2px;
        width: 30px;
        color: white;
        font-size: 12;
    }

    .button1:hover {
        background-color: #ffd52b;
        color: white;
    }
    .buttonPaging:hover {
        background-color: #4CAF50;
        color: white;
    }
    
    .button1:disabled{
        background-color: #ffffff;
        color: grey; 
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonReject:enabled {
        background-color: white; 
        color: black; 
        border: 2px solid #a10c25;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonReject:hover {
        background-color: #a10c25;
        color: white;
        }
    
    .buttonReject:disabled{
        background-color: #ffffff;
        color: grey; 
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .nostyle{
        -webkit-appearance: none; 
        border:1px solid;
    }

    .nostyle-text{
        text-decoration: none; 
    }
    a:link {
        text-decoration: none;
    }
    a:visited {
        text-decoration: none;
    }

    a:hover {
        text-decoration: none;
    }

    a:active {
        text-decoration: none;
    }

    .modal {
        display: block; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 0; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }
    .modal-content {
        /* background-color: #fefefe; */
        background-color: #3D3D3D;
        text-align: center;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        /* width: 10%; */
    }   

    
</style>

@guest
please login

@else
    @extends('layouts.app')
    @section('content')
        @php
            unset($_POST);   
        @endphp
        @if(Auth::user()->userStatus == 0)
            {{ Session::flush() }}
            <script>
                window.alert("Your account is yet to be approved. Please wait for approval email from Admin");
                window.location = "/";
            </script>
        @elseif(Auth::user()->userRole == "Admin")
            <div id="containerUserList" class="container container75" style="border: black none 1px; min-height:80%">
                <div class="row justify-content-center" style="border:none red; width:; ">
                    <div class="col-md-8" style="border:none cyan;height:80%;">
                        <div class="card" style="border:none yellow;height:80%;">
                            <div class="card-header" name="userList" style="background-color: #a10c25; color:white; font-size:20"><b>Student List</b></div>

                            <div class="card-body" style="text-align: right; overflow-x:auto; border:none purple; height:80%">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <table style="width:100%">
                                    <tr class="" style="background-color: white; border:none; ">
                                        <td style="border-top: none">
                                            Name
                                        </td>
                                        <td style="border-top: none">
                                            <input class="nostyle form-control" style="border-radius: 6px; padding-left:10px" type="text" id="myInput" onkeyup="myFunctionReset()" placeholder="Filter by name.." title="Type in a name">
                                        </td>
                                        <td style="border-top: none">
                                            &nbsp
                                        </td>
                                        <td style="border-top: none">
                                            &nbsp
                                        </td>
                                        <td style="border-top: none">
                                            &nbsp
                                        </td>
                                        <td style="border-top: none">
                                            Email
                                        </td>
                                        <td style="border-top: none">
                                            <input class="nostyle form-control" style="border-radius: 6px; padding-left:10px" type="text" id="myInputEmail" onkeyup="myFunctionReset()" placeholder="Filter by email.." title="Type in an email">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-top: none;">
                                            Phone no
                                        </td>
                                        <td style="border-top: none">
                                            <input class="nostyle form-control" style="border-radius: 6px; padding-left:10px" type="text" id="myInputPhoneNo" onkeyup="myFunctionReset()" placeholder="Filter by phone no.." title="Type in a phone no">
                                        </td>
                                        <td style="border-top: none">
                                            &nbsp
                                        </td>
                                        <td style="border-top: none">
                                            &nbsp
                                        </td>
                                        <td style="border-top: none">
                                            &nbsp
                                        </td>
                                    </tr>
                                </table>
                                <table style="border: none; width:100%" id="myTable">
                                    <tr>
                                        <td colspan="5" class="tdNoStyle" style="font-size:12; text-align:right; vertical-align:bottom">
                                            <span>
                                                Show entries : 
                                                <select class="" id="inputShowEntries" onchange="myFunctionReset()" style="margin-bottom: 2; margin-right:">
                                                    <option selected>10</option>
                                                    <option>20</option>
                                                    <option>30</option>
                                                    <option>100</option>
                                                </select>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr class="trBorderLeftRight">
                                        <th>
                                            No.
                                        </th>
                                        <th>
                                            Name
                                        </th>
                                        <th>
                                            Email
                                        </th>
                                        <th>
                                            Phone No.
                                        </th>
                                        <th>
                                            Credit
                                        </th>
                                    </tr>
                                    @if($users == null)
                                        <tr>
                                            <td colspan="5" style="text-align: center;">
                                                No users registered yet!
                                            </td>
                                        </tr>
                                    @else
                                        @foreach($users as $user)
                                            <tr class="trBorderLeftRight">
                                                <td class="tdNo">
                                                    {{ __($user['id'])}}
                                                </td>
                                                <td class="tdName">
                                                    @if($user['userRole']=="Admin")
                                                        {{ __($user['name'])}}
                                                    @else
                                                        <a style="text-decoration: none; color:black" href="{{url('creditDetails/'.$user['id'])}}">{{ __($user['name'])}}</a>
                                                    @endif
                                                </td>
                                                <td class="tdEmail">
                                                    {{ __($user['email'])}}
                                                </td>
                                                <td class="tdPhoneNo" style="text-align: left;">
                                                    {{ __($user['userPhone'])}}
                                                </td>
                                                <td class="tdCreditBalance">
                                                    {{ __($user['creditBalance'])}}
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr id="trNoResult" class="trBorderLeftRight" style="display: none; ">
                                            <td colspan="5" style="text-align:center">
                                                No user matched you filter...
                                            </td>
                                        </tr>  
                                    @endif
                                </table>
                                <div id="pagingDiv" style="border:none;">
                                    <span id="pagingSpan" style="font-size:"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br>
        @endif

        <script>
            
            window.onload = function(){ //run these after page loaded
                myFunction();
            };
            
            var currentPage = 1; //default current page
            var showEntries = 10; //default show entries
            var checkRegSuccess = 0;

            function myFunctionReset(){
                currentPage = 1; //reset current page position
                showEntries = document.getElementById("inputShowEntries").value;
                
                myFunction();
                var pagingSpan = document.getElementById('pagingSpan');
                if(showEntries <= 20){
                    pagingSpan.scrollIntoView(); // scroll the page make sure user can see full table witouth scrolling
                }
            };
            
            function myFunction() {
                var input, filter, table, tr, td, tdState, tdNo, tdPhoneNo, i, txtValue, txtValueState, inputState, filterState, inputPhoneNo, filterPhoneNo, txtValuePhoneNo, pagingSpan, counterTr = 0, counterPageButton = 0, oldPagingButton = 0;
                input = document.getElementById("myInput");
                inputState = document.getElementById("myInputEmail");
                inputPhoneNo = document.getElementById("myInputPhoneNo");
                filter = input.value.toUpperCase();
                filterState = inputState.value.toUpperCase();
                filterPhoneNo = inputPhoneNo.value;
                table = document.getElementById("myTable");
                tr = table.getElementsByTagName("tr");
                for (i = 0; i < tr.length; i++) {
                    tdNo = tr[i].getElementsByClassName("tdNo")[0];
                    td = tr[i].getElementsByClassName("tdName")[0];
                    tdState = tr[i].getElementsByClassName("tdEmail")[0];
                    tdPhoneNo = tr[i].getElementsByClassName("tdPhoneNo")[0];
                    if (td && tdState && tdPhoneNo) {
                        txtValue = td.textContent || td.innerText;
                        txtValueState = tdState.textContent || tdState.innerText;
                        txtValuePhoneNo = tdPhoneNo.textContent || tdPhoneNo.innerText;
                        if (txtValue.toUpperCase().indexOf(filter) > -1 && txtValueState.toUpperCase().indexOf(filterState) > -1 && txtValuePhoneNo.toUpperCase().indexOf(filterPhoneNo) > -1) {
                            if((counterTr >= (currentPage-1)*showEntries) && ((counterTr+1) <= (currentPage*showEntries))){
                                tr[i].style.display = "";
                                tdNo.innerHTML=counterTr+1;
                            }else{
                                tr[i].style.display = "none";
                            }
                            counterTr++;
                        }else{
                            tr[i].style.display = "none";
                        }
                    }
                }

                pagingSpan = document.getElementById("pagingSpan");
                pagingSpan.innerHTML = "";

                var trNoResult = document.getElementById('trNoResult');

                if((document.getElementById('regSuccess') != null) && (checkRegSuccess == 0)){
                        currentPage = Math.ceil(counterTr/showEntries);
                        checkRegSuccess++;
                        myFunction();
                        pagingSpan.scrollIntoView();
                }

                if(counterTr == 0){
                    trNoResult.style.display = "";
                }else{
                    trNoResult.style.display = "none";
                }

                if(counterTr/showEntries > 1){
                    pagingSpan.innerHTML = "Page : ";
                    for(i = 1; i <= Math.ceil(counterTr/showEntries); i++){
                        var pagingButton = document.createElement('button');
                        if(i == currentPage){
                            pagingButton.className = "buttonPagingCurrent";
                            pagingButton.disabled = true;
                        }
                        else{
                            pagingButton.className = "buttonPaging";
                        }
                        pagingButton.type = "button";
                        pagingButton.addEventListener('click', pageNumberClicked);
                        pagingButton.value = i;
                        pagingButton.innerHTML = i;
                        pagingSpan.appendChild(pagingButton); 
                        counterPageButton++;
                    }
                }
            };

            function pageNumberClicked(e){
                currentPage = e.target.value;
                myFunction();
                var pagingSpan = document.getElementById('pagingSpan');
                var addUserButton = document.getElementById('addUserButton');
                if(showEntries <= 20){
                    pagingSpan.scrollIntoView(); // scroll the page make sure user can see full table witouth scrolling
                }else{
                    addUserButton.scrollIntoView();
                }
            };

        </script>

    @endsection
@endguest

