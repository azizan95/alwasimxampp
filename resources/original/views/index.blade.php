<!DOCTYPE html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Al - Wasim</title>

  <!-- Bootstrap core CSS -->
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <script src="{{ asset('js/app.js') }}" defer></script>
  {{-- <!-- <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"> --> --}}
  <!-- Custom styles for this template -->
  <!-- <link href="css/small-business.css" rel="stylesheet"> -->
  <!-- <link href="css/small-business.css" rel="stylesheet"> -->
  <style>
    table {
        border-collapse: collapse;
        width: 100%;
    }

    th, td {
        padding: 8px;
        text-align: left;
        border-top: 1px solid black;
    }

    tr:nth-child(odd) {
        background-color: #f2f2f2;
    }

    th {
        text-align: center;
        font-weight: normal;
        font-size: 15;
        background-color: #242d5f;
        color: white;
    }

    .buttonLogin:enabled {
        background-color: #ffffff00; 
        color: rgb(0, 0, 0); 
        border: none;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonLogin:hover, .buttonLogin:focus {
        background-color: #a10c25;
        color: rgb(255, 255, 255);
        border:2px, solid, #a10c25; 
    }

    /* .buttonLogin:hover {
        background-color: #a10c25;
        color: rgb(255, 255, 255); 
    } */

    .buttonSave:enabled {
        background-color: white; 
        color: black; 
        border: 2px solid #4CAF50;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonSave:hover {
        background-color: #4CAF50;
        color: white;
        }

    .buttonDisable{
        background-color: #ffffff;
        color: grey;
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }
    
    .button1:enabled {
        background-color: white;
        color: black;
        border: 2px solid #ffdb3c;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .button1:hover {
        background-color: #ffdb3c;
        color: white;
        }

    .button1:disabled{
        background-color: #ffffff;
        color: grey;
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonReject:enabled {
        background-color: white;
        color: black;
        border: 2px solid #a10c25;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }

    .buttonReject:hover {
        background-color: #a10c25;
        color: white;
        }

    .buttonReject:disabled{
        background-color: #ffffff;
        color: grey;
        border: 2px solid grey;
        border-radius: 4px;
        width: 75px;
        margin-top: 5px;
        margin-right: 0px
    }
    .deletebutton2{
        display: none;
    }
    .deletebutton2:hover{
        display: block;
    }
    .deletebutton1:hover{
        display: none;
    }

    button img:last-child {
        display: none;  
    }
    button:hover img:last-child {
        display: block;  
    }
    
    button:hover img:first-child {
        display: none;  
    }

    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 100px; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
    }
    .modal-content {
        /* background-color: #fefefe; */
        background-color: #3D3D3D;
        text-align: center;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        /* width: 10%; */
    }   

    .btn-primary {
      color: rgb(0, 0, 0);
      background-color: #ffd52b;
      border-color: #ffcd04;
    }

    .btn-primary:hover {
      color: rgb(255, 255, 255);
      background-color: #ffb004;
      border-color: #ffd52b;
    }

    .nav-link2 {
      display: block;
      padding: 0.5rem 1rem;
    }

</style>


</head>

<body>
  @if(Auth::user()!=null)
    @if(Auth::user()->userStatus == 0)
      {{ Session::flush() }}
      <script>
        window.onload = function(){
          window.alert("You have been registered. Please wait an email from Admin indicating you account has been approved.");
          window.location = "/";
        }
      </script>
    @endif
  @endif
    <!-- Navigation -->
    <!-- <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top"> -->
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top" style="background-color:#ffcd04;">
    <div class="container">
      <a class="navbar-brand" href="#">Al - Wasim</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
        <!-- <li class="nav-item active"> -->
          <li class="nav-item">
            @if(Auth::user()==null)
              <button class="nav-link2 buttonLogin" onclick="eventEditFunction()">Login</button>
            @else
            {{-- <button class="nav-link2 buttonLogin" onclick="return confirm('Sure logout?'); {{ url('/index/logout') }}">Logout</button> --}}

              <a class="dropdown-item nav-link2 buttonLogin" style="border-radius:4px" href="{{ url('/index/logout') }}"
                  onclick="return confirm('Sure logout?')">
                  {{ __('Logout') }}
              </a>
            @endif
          </li>
          <li class="nav-item">
            {{-- <a class="nav-link" href="#">Our Politicians</a> --}}
          </li>
          <li class="nav-item">
            {{-- <a class="nav-link" href="#">About us</a> --}}
          </li>
          <li class="nav-item">
            {{-- <a class="nav-link" href="#">Contact us</a> --}}
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="modal" id="editModal" style="border: none">
    <div class="row justify-content-center" id="editModal2" style="border:none">
      <div class="col-md-8 row justify-content-center" id="editModal3" style="border:none; text-align:center" >
        <div class="card" id="editCard" style="border: none; width: 50%">
          <div class="card-header" style="background-color:#a10c25; color:white"><b>Login</b></div>
            <div class="card-body">
              <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="form-group row">
                  <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                  <div class="col-md-6">
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                      </span>
                    @enderror
                  </div>
                </div>

                <div class="form-group row">
                  <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                  <div class="col-md-6">
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                  </div>
                </div>
                <div class="form-group row" >
                  <div class="col-md-6 offset-md-4" >
                    <div class="form-check text-md-left">
                      <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                      <label class="form-check-label" for="remember">
                          {{ __('Remember Me') }}
                      </label>
                    </div>
                  </div>
                </div>

                <div class="form-group row mb-0 justify-content-center" style="text-align: center;">
                  <button type="submit" class="buttonSave" style="width: 100">{{ __('Login') }}</button>&nbsp &nbsp;
                  <button type="reset" class="buttonReject" style="width: 100" onclick="eventCloseEditFunction()">{{ __('Cancel') }}</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <!-- Page Content -->
  <div class="container">

    <!-- Heading Row -->
    <div class="row align-items-center my-5" style="position: relative; top:25px;">
      <div class="col-lg-7" style="text-align: center">
      <!-- <img class="img-fluid rounded mb-4 mb-lg-0" src="http://placehold.it/900x400" alt=""> -->
        {{-- <img class="img-fluid rounded mb-4 mb-lg-0" src="/images/norwaylabourpartylogo.png" width="300" alt=""> --}}
      </div>
      <!-- /.col-lg-8 -->
      <div class="col-lg-5">
        <h1 class="font-weight-light">Mari Belajar Mengaji!</h1>
        <p>Al - Wasim adalah sebuah laman sesawang untung belajar mengaji secara atas talian.</p>
        @if(Auth::user()==null)
          <a class="btn btn-primary" href="{{ url('/register') }}">Klik sini untuk daftar sekarang!</a>
        @endif
      </div>
      <!-- /.col-md-4 -->
    </div>
    <!-- /.row -->

    <!-- Call to Action Well -->
    <!-- <div class="card text-white bg-secondary my-5 py-4 text-center" style="position: relative; top:25px; background-color:red"> -->
    <div class="card text-white my-5 py-4 text-center" style="position: relative; top:25px; background-color:#a10c25">

      <div class="card-body" style="background-color:#a10c25">
        <p class="text-white m-0">All people are unique, irreplaceable and equally valuable. </p>
      </div>
    </div>

    <!-- Content Row -->
    <div class="row">
      <div class="col-md-4 mb-5">
        <div class="card h-100">
          <div class="card-body">
            <h2 class="card-title">Brief history</h2>
            <p class="card-text">The Norwegian Labour Party was founded in 1887. In gained its first parliamentary seats in 1903 and has been Norway`s largest political party since 1927. The party experienced a split in 1921 caused by a decision made two years earlier to join the Communist International. In 1923 the party left the Communist International and in 1927 Labour was once again united.</p>
          </div>
          <div class="card-footer">
            {{-- <a href="#" class="btn btn-primary btn-sm">More Info</a> --}}
          </div>
        </div>
      </div>
      <!-- /.col-md-4 -->
      <div class="col-md-4 mb-5">
        <div class="card h-100">
          <div class="card-body">
            <h2 class="card-title">Organisational structure</h2>
            <p class="card-text">A system whereby local trade unions could be affiliated to the party was abolished from 1997, and today The Labour Party has approximately 49 500 individual members. The party is organised in 19 counties, 434 municipalities and about 2 500 localassociations.</p>
          </div>
          <div class="card-footer">
            {{-- <a href="#" class="btn btn-primary btn-sm">More Info</a> --}}
          </div>
        </div>
      </div>
      <!-- /.col-md-4 -->
      <div class="col-md-4 mb-5">
        <div class="card h-100">
          <div class="card-body">
            <h2 class="card-title">Relations with the trade union movement</h2>
            <p class="card-text">The Labour Party and the trade union movement have common historical roots. There has always been a close cooperation between these two branches of the labour movement. However, the organisational and decision-making structures of the party and the Confederation of Trade Unions (LO) are separate and fully independent. </p>
          </div>
          <div class="card-footer">
            {{-- <a href="#" class="btn btn-primary btn-sm">More Info</a> --}}
          </div>
        </div>
      </div>
      <!-- /.col-md-4 -->

    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Your Website 2020</p>
    </div>
    <!-- /.container -->
  </footer>

</body>

<script type="text/javascript">
  var editModal = document.getElementById("editModal");
  var editModal2 = document.getElementById("editModal2");
  var editModal3 = document.getElementById("editModal3");

  var disabledButton = document.getElementById("buttonDisabled");

  var editCard = document.getElementById("editCard");


    function eventCloseEditFunction() {
        editModal.style.display = "none";
    };

    function eventEditFunction() {

    editModal.style.display = "block";
    };
    
    document.addEventListener("click", function(e)
    {
        if ((e.target==editModal || e.target==editModal2 || e.target==editModal3)) 
        {
            editModal.style.display = "none";
        }
    });
  </script>

</html>
