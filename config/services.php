<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],

    // 'twilio' => [
    //     'sid' => env('AC6c706137228816bc95e0098884b0fbf3'),
    //     'token' => env('e56c751a13fcb4cfdf0c247afbbe9225'),
    //     'whatsapp_from' => env('+14155238886')
    // ],

    'twilio' => [
        'sid' => 'AC6c706137228816bc95e0098884b0fbf3',
        'password' => 'e56c751a13fcb4cfdf0c247afbbe9225',
        'token' => 'e56c751a13fcb4cfdf0c247afbbe9225',
        'username' => 'AC6c706137228816bc95e0098884b0fbf3',
        'whatsapp_from' => '+16822435503',
        'from' => '+16822435503'
    ],

    'telegram-bot-api' => [
        'token' => env('TELEGRAM_BOT_TOKEN', '1718620351:AAGS5xOXv8xhxZMTBrX0ug1dpA5H8Qqv4Ss')
    ],

    // 'stream' => [
    //     'ssl' => [
    //        'allow_self_signed' => true,
    //        'verify_peer' => false,
    //        'verify_peer_name' => false,
    //     ],
    //  ],
    
];
