<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAttributesToCreditsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('credits', function (Blueprint $table) {
            $table->foreignId('user_id');
            $table->foreignId('session_id')->nullable();
            $table->date('creditDate');
            $table->string('creditTransaction')->nullable();
            $table->integer('creditCredit')->nullable();
            $table->integer('creditDebit')->nullable();
            $table->string('creditStatus')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('credits', function (Blueprint $table) {
            $table->dropColumn('user_id');
            $table->dropColumn('session_id');
            $table->dropColumn('creditDate');
            $table->dropColumn('creditTransaction');
            $table->dropColumn('creditCredit');
            $table->dropColumn('creditDebit');
            $table->dropColumn('creditStatus');
        });
    }
}
