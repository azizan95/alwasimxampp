<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddClassTypeClassDateStartClassDateEnd extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('classes', function (Blueprint $table) {
            $table->string('classType')->nullable();
            $table->date('classDateStart')->nullable();
            $table->date('classDateEnd')->nullable();
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('class', function (Blueprint $table) {
            $table->dropColumn('classType');
            $table->dropColumn('classDateStart');
            $table->dropColumn('classDateEnd');
            //
        });
    }
}
